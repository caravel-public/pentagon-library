Pentagon Integrals
==================

This repository contains the source code for the numerical evaluation of the
massless pentagon integrals published in
[arXiv:1807.09812](https://arxiv.org/abs/1807.09812 "Visit on arXiv."). <br>
Furthermore, it includes a small build facility that allows to compile
the code to a shared library and generates the *pentagon.pc* files that is
needed to integrate the shared library into other code bases.

***

Installation
-------------

__Prerequisites__

The build system used to generate the shared library is called [Meson][2], which
can be installed via (Python3 is prerequisite):

```bash
$ pip install meson
```


As a backend, meson uses [Ninja][1] which also needs to be installed. <br>

The integral library itself depends on both GSL (which should be available)
on any linux distribution and [Ginac][3].

On Mac, GSL can be installed by running:

```bash
$ brew install gsl
```


__Install__

When all these prerequisites are met, the library can be installed by navigating
to the top directory (the one containing this README.md file) and running:

```bash
$ mkdir build
$ cd build
$ meson .. -Dprefix=/path/to/install/location -Dlibdir=lib
$ ninja 
$ ninja install
```

Here, the option *-j4* just indicates that 4 cores should be used in parallel
to speed up compilation time. This can, however, be omitted.

***

Linking against other libraries
-------------------------------

In order to link against other libraries, just add the location of the file
*/path/to/install/location/lib/x86_64-linux-gnu/pkgconfig/pentagon.pc* to
the __PKG_CONFIG_PATH__ environment variable such that it can be found the
package manager.

***

Examples
--------

To also compile the example that comes with the publication of the library,
just run

```bash
$ meson configure -Dexamples=true
$ ninja install
$ examples/example-code
```

[1]: https://ninja-build.org/ "ninja-build.org"
[2]: http://mesonbuild.com/ "mesonbuild.com"
[3]: https://www.ginac.de/ "ginac"
