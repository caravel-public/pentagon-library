#include <vector>
#include "src/header.h"
#include <iomanip>
#include <string>
#include <time.h>
#include <fstream>
#include <sstream>
#include <assert.h>
#include <complex>

#include "function_names.h"


std::vector<std::complex<double> > evaluate_pentagonfunctions(double v[5], double error);
std::vector<std::vector<std::vector<std::complex<double> > > > evaluate_pentagonintegrals(std::vector<std::complex<double> > pentagonfunctions);

template <typename C>
C cyclic_permutation(const C& collection, int offset) {
    
    assert(offset <= (int)collection.size());
    if (offset == 0) {
        return collection;
    }
    auto shuffle = (offset) % collection.size();
    auto tmp = C(collection.begin() + shuffle, collection.end());
    tmp.insert(tmp.end(), collection.begin(), collection.begin() + shuffle);
    return tmp;
}

void write(const std::string& filename, const std::vector<std::complex<double>>& vals, const std::vector<double>& psp) {
    std::ofstream stream(filename);
    
    // Print the phase space point.
    stream << "(* psp: ";
        for(std::size_t i = 0; i < psp.size(); ++i){
            stream << "s" << i+1 << i+2 << " = " << std::setprecision(18)  << psp[i] << "   ";
        }
    stream << " *)"  << std::endl;

    // Print the values.
    assert(vals.size() == func_names.size());
    for(std::size_t i = 0; i < vals.size(); ++i) {
        stream << func_names.at(i) << " = " << vals.at(i).real() << std::endl; 
    }
    
    stream.close();
}

int main() {

    // User input
    //const std::string id = "badger_euclidean";
    const std::string id = "five_parton";
    //std::vector<double> psp{-1., -37. / 78., -2023381. / 3194997., - 83. / 102., -193672. / 606645.}; // badger point euclidean
    std::vector<double> psp{-1., -8. / 13., -1094. / 2431., - 7. / 17., -749. / 7293.}; // five parton point


    std::vector<double> psp_reflected(psp.rbegin(), psp.rend());

    std::cout << std::endl << "psp          : ";
    for (const auto& p: psp) {
        std::cout << p << " ";
    }

    std::cout << std::endl << std::endl;

    constexpr double error = 1e-6;

    // Compute the special functions for each offset
    for(std::size_t p = 0; p <= psp.size(); ++p) {
        std::string permutation(std::string("functions/permutation_") + std::to_string(p) + "_" + id + ".txt");
        std::string reflection(std::string("functions/reflected_permutation_") + std::to_string(p) + "_" + id + ".txt");

        auto psp_perm = cyclic_permutation(psp, p);
        auto psp_refl = cyclic_permutation(psp_reflected, p);

        std::vector<std::complex<double> > funcs_perm = evaluate_pentagonfunctions(psp_perm.data(), error);
        std::vector<std::complex<double> > funcs_refl = evaluate_pentagonfunctions(psp_refl.data(), error);

        write(permutation, funcs_perm, psp_perm);
        write(reflection,  funcs_refl, psp_refl);
    }
    

    return 0;
}
