#include <vector>
#include "src/header.h"
#include <iomanip>
#include <string>
#include <time.h>
#include <fstream>
#include <sstream>


std::vector<std::complex<double> > evaluate_pentagonfunctions(double v[5], double error);
std::vector<std::vector<std::vector<std::complex<double> > > > evaluate_pentagonintegrals(std::vector<std::complex<double> > pentagonfunctions);

int main (void)
{

    using namespace std;
  
   // input 

    int ptno;
    cout<<endl<<endl<<" ID number of kinematical test point [1-16]: ";
    cin>>ptno;
    cout<<endl;
    double v[5];
    switch(ptno){
       case 1 : v[0] = 0.9876543209876;  v[1] = 0.790123456790; v[2] = -1.80267859636; v[3] = 3.16049382716; v[4] = -2.211585663744; break; 
       case 2 : v[0] = 16.1428571428571; v[1] = -1.57508763903766; v[2] = 7.36670543927394; v[3] = 2.61908827694276; v[4] = -2.56621144698481e-1; break; 
       case 3 : v[0] = 16.1445783132530;   v[1] = -1.59898477157360; v[2] = 7.62385321100917; v[3] = 2.47169811320755;v[4] = -2.50000000000000e-1;break;
       case 4 : v[0] = 2.47169811320755; v[1] = -2.50000000000000e-1; v[2] = 16.1445783132530; v[3] = -1.59898477157360; v[4] = 7.62385321100917; break; 
       case 5 : v[0] = 8.88349514563107; v[1] = 11.1298701298701; v[2] = -9.16326530612245; v[3] = 22.0983606557377; v[4] = -1.20103092783505; break; 
       case 6 : v[0] = 2.37681159420290; v[1] = -7.48245614035088; v[2] = 15.9272727272727; v[3] = -8.01666666666667; v[4] = 4.65925925925926; break; 
       case 7 : v[0] = -2.78723404255319; v[1] = 4.26027397260274; v[2] = -2.14563106796117; v[3] = 1.35238095238095; v[4] = 1.26415094339623; break; 
       case 8 : v[0] = 8.88349514563107; v[1] = -3.16774193548387; v[2] = -9.16326530612245; v[3] = -4.05172413793103; v[4] = -1.20103092783505;break; 
       case 9 : v[0] = -7.85964912280702e-1; v[1] = -1.67833333333333e1; v[2] = -8.84955752212389e-2; v[3] = 2.26168224299065; v[4] = -3.10465116279070; break; 
       case 10 : v[0] = 1.64367816091954; v[1] = -2.14563106796117; v[2] = -1.99382716049383; v[3] = -6.22641509433962e-1; v[4] = -2.78723404255319; break;
       case 11 : v[0] = -1.; v[1] = -3.; v[2] = -11.; v[3] = -19.; v[4] = -31.; break; 
       case 12 : v[0] = -1.07718120805369; v[1] = -3.39320388349515; v[2] = -2.33380480905233; v[3] = -5.17628205128205; v[4] = -2.84709480122324; break;
       case 13 : v[0] = -6.76394849785408; v[1] = -4.01621621621622; v[2] = -5.71052631578947; v[3] = -7.58457711442786; v[4] = -1.81286549707602; break;
       case 14 : v[0] = -8.71016166281755; v[1] = -1.74778761061947; v[2] = -4.09117647058824; v[3] = -3.13824884792627; v[4] = -5.24157303370787; break;
       case 15 : v[0] = -0.0382775119617225; v[1] = -7.79320987654321; v[2] = -0.770897832817337; v[3] = -1.31178707224335; v[4] = -3.72935779816514; break;
       case 16 : v[0] = -5.55447941888620; v[1] = -8.10691823899371; v[2] = -0.408304498269896; v[3] = -0.431803896920176; v[4] = -6.80459770114943; break;
    
       default :  cout<<endl<<" A number outside the [1-16] range was entered. ";
                  cout<<endl<<endl<<" Insert phase space point (s12 s23 s34 s45 s51) : ";     cin>>v[0];   cin>>v[1];  cin>>v[2];  cin>>v[3];  cin>>v[4];    
    }
  
    if((v[0]<0)&&(v[1]<0)&&(v[2]<0)&&(v[3]<0)&&(v[4]<0)){cout<<endl<<" Euclidean kinematics"<<endl;}
    if((v[0]>0)&&(v[1]<0)&&(v[2]>0)&&(v[3]>0)&&(v[4]<0)){cout<<endl<<" Minkowskian kinematics, s12 channel"<<endl;}
    if((v[0]<0)&&(v[1]>0)&&(v[2]<0)&&(v[3]>0)&&(v[4]>0)){cout<<endl<<" Minkowskian kinematics, s23 channel"<<endl;}
    if((v[0]>0)&&(v[1]<0)&&(v[2]>0)&&(v[3]<0)&&(v[4]>0)){cout<<endl<<" Minkowskian kinematics, s34 channel"<<endl;}
    if((v[0]>0)&&(v[1]>0)&&(v[2]<0)&&(v[3]>0)&&(v[4]<0)){cout<<endl<<" Minkowskian kinematics, s45 channel"<<endl;}
    if((v[0]<0)&&(v[1]>0)&&(v[2]>0)&&(v[3]<0)&&(v[4]>0)){cout<<endl<<" Minkowskian kinematics, s15 channel"<<endl;}
    if((v[0]>0)&&(v[1]<0)&&(v[2]<0)&&(v[3]<0)&&(v[4]<0)){cout<<endl<<" Minkowskian kinematics, s35 channel"<<endl;}
    if((v[0]<0)&&(v[1]>0)&&(v[2]<0)&&(v[3]<0)&&(v[4]<0)){cout<<endl<<" Minkowskian kinematics, s14 channel"<<endl;}
    if((v[0]<0)&&(v[1]<0)&&(v[2]>0)&&(v[3]<0)&&(v[4]<0)){cout<<endl<<" Minkowskian kinematics, s25 channel"<<endl;}
    if((v[0]<0)&&(v[1]<0)&&(v[2]<0)&&(v[3]>0)&&(v[4]<0)){cout<<endl<<" Minkowskian kinematics, s13 channel"<<endl;}
    if((v[0]<0)&&(v[1]<0)&&(v[2]<0)&&(v[3]<0)&&(v[4]>0)){cout<<endl<<" Minkowskian kinematics, s24 channel"<<endl;}
    cout<<endl<<" point = { v1f -> "<<setprecision(15)<<v[0]<<" ,";
    cout<<" v2f -> "<<setprecision(15)<<v[1]<<" ,";
    cout<<" v3f -> "<<setprecision(15)<<v[2]<<" ,";
    cout<<" v4f -> "<<setprecision(15)<<v[3]<<" ,";
    cout<<" v5f -> "<<setprecision(15)<<v[4]<<"};"<<endl;
    cout<<endl;

   // set the integration error of the pentagon functions' evaluation

    double error = 1e-6;
  
    clock_t tick;
    clock_t tock;

   //  compute pentagon functions (and time it)
    tick = clock();

    vector<complex<double> > pentagonfunctions = evaluate_pentagonfunctions(v, error);

    tock = clock();

    cout << "Time to evaluate all functions: "<<float( tock-tick )/CLOCKS_PER_SEC <<"s."<<endl;


   //  assemble pentagon integrals (and time it)
    tick = clock();

    vector<vector<vector<complex<double> > > > pentagonintegrals = evaluate_pentagonintegrals(pentagonfunctions);
      
    tock = clock();
    cout << "Time to assemble them : "<<float( tock-tick )/CLOCKS_PER_SEC <<"s."<<endl<<endl;

   // output 
   ofstream tofile;
   tofile.open("output.txt"); 
   for(int i1=0; i1<5; i1++){
      for(int i2=0; i2<61; i2++){
         for(int i3=0; i3<5; i3++){
            if(i3==0){
               tofile<<" f["<<i2+1<<","<<i1+1<<"] = { "<<setw(4)<<real(pentagonintegrals[i2][i3][i1])<<" , ";
            } else if(i3==4){
                  tofile<<setw(16)<<setprecision(13)<<real(pentagonintegrals[i2][i3][i1])
                <<" + I*("<<setw(16)<<setprecision(13)<<imag(pentagonintegrals[i2][i3][i1])<<") }; "<<endl;
            } else{
                  tofile<<setw(16)<<setprecision(13)<<real(pentagonintegrals[i2][i3][i1])
                <<" + I*("<<setw(16)<<setprecision(13)<<imag(pentagonintegrals[i2][i3][i1])<<") , ";
            }           
         };
      }     tofile<<endl;
   }     tofile<<endl;
   tofile.close();

 
   return 0;
}



