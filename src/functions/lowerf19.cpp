
#include "../header.h"  
double FAw3pt5Re(double v1,double v2,double v3,double v4, double v5);
double FAw3pt5Im(double v1,double v2,double v3,double v4, double v5);

double lowerweight19 (int imag, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;  
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double v1=-v1f; double v2=-v2f; double v3=-v3f; double v4=-v4f; double v5=-v5f;
   double zeta3=1.2020569031595942;

   double result19;
    if(imag==0){
         result19 =  -(hvs(v1/v4)*rLi(3,1 - v1/v4)) - hvs(-(v1/v4))*rLi(3,v4/(-v1 + v4)) - (Power(M_PI,2)*hvs(-(v1/v4))*rlog(Abs(v4)))/6. + (Power(M_PI,2)*Power(hvs(v1 - v4),2)*hvs(-(v1/v4))*rlog(Abs(v4)))/2. - Power(M_PI,2)*hvs(v1 - v4)*hvs(-(v1/v4))*hvs(-v4)*rlog(Abs(v4)) + (Power(M_PI,2)*hvs(-(v1/v4))*Power(hvs(-v4),2)*rlog(Abs(v4)))/2. - (hvs(-(v1/v4))*Power(rlog(Abs(v4)),3))/6. + (Power(M_PI,2)*hvs(-(v1/v4))*rlog(Abs(-v1 + v4)))/6. - (Power(M_PI,2)*Power(hvs(v1 - v4),2)*hvs(-(v1/v4))*rlog(Abs(-v1 + v4)))/2. + Power(M_PI,2)*hvs(v1 - v4)*hvs(-(v1/v4))*hvs(-v4)*rlog(Abs(-v1 + v4)) - (Power(M_PI,2)*hvs(-(v1/v4))*Power(hvs(-v4),2)*rlog(Abs(-v1 + v4)))/2. + (hvs(-(v1/v4))*Power(rlog(Abs(v4)),2)*rlog(Abs(-v1 + v4)))/2. - (hvs(-(v1/v4))*rlog(Abs(v4))*Power(rlog(Abs(-v1 + v4)),2))/2. + (hvs(-(v1/v4))*Power(rlog(Abs(-v1 + v4)),3))/6. + Power(M_PI,2)*hvs(v1 - v4)*hvs(-(v1/v4))*rlog(Abs(v4))*sign(v4) - Power(M_PI,2)*hvs(-(v1/v4))*hvs(-v4)*rlog(Abs(v4))*sign(v4) - Power(M_PI,2)*hvs(v1 - v4)*hvs(-(v1/v4))*rlog(Abs(-v1 + v4))*sign(v4) + Power(M_PI,2)*hvs(-(v1/v4))*hvs(-v4)*rlog(Abs(-v1 + v4))*sign(v4) + (Power(M_PI,2)*hvs(-(v1/v4))*rlog(Abs(v4))*Power(sign(v4),2))/2. - (Power(M_PI,2)*hvs(-(v1/v4))*rlog(Abs(-v1 + v4))*Power(sign(v4),2))/2. ;
    }else{
         result19 =  -(Power(M_PI,3)*hvs(v1 - v4)*hvs(-(v1/v4)))/6. + (Power(M_PI,3)*Power(hvs(v1 - v4),3)*hvs(-(v1/v4)))/6. + (Power(M_PI,3)*hvs(-(v1/v4))*hvs(-v4))/6. - (Power(M_PI,3)*Power(hvs(v1 - v4),2)*hvs(-(v1/v4))*hvs(-v4))/2. + (Power(M_PI,3)*hvs(v1 - v4)*hvs(-(v1/v4))*Power(hvs(-v4),2))/2. - (Power(M_PI,3)*hvs(-(v1/v4))*Power(hvs(-v4),3))/6. - (M_PI*hvs(v1 - v4)*hvs(-(v1/v4))*Power(rlog(Abs(v4)),2))/2. + (M_PI*hvs(-(v1/v4))*hvs(-v4)*Power(rlog(Abs(v4)),2))/2. + M_PI*hvs(v1 - v4)*hvs(-(v1/v4))*rlog(Abs(v4))*rlog(Abs(-v1 + v4)) - M_PI*hvs(-(v1/v4))*hvs(-v4)*rlog(Abs(v4))*rlog(Abs(-v1 + v4)) - (M_PI*hvs(v1 - v4)*hvs(-(v1/v4))*Power(rlog(Abs(-v1 + v4)),2))/2. + (M_PI*hvs(-(v1/v4))*hvs(-v4)*Power(rlog(Abs(-v1 + v4)),2))/2. - (Power(M_PI,3)*hvs(-(v1/v4))*sign(v4))/6. + (Power(M_PI,3)*Power(hvs(v1 - v4),2)*hvs(-(v1/v4))*sign(v4))/2. - Power(M_PI,3)*hvs(v1 - v4)*hvs(-(v1/v4))*hvs(-v4)*sign(v4) + (Power(M_PI,3)*hvs(-(v1/v4))*Power(hvs(-v4),2)*sign(v4))/2. - (M_PI*hvs(-(v1/v4))*Power(rlog(Abs(v4)),2)*sign(v4))/2. + M_PI*hvs(-(v1/v4))*rlog(Abs(v4))*rlog(Abs(-v1 + v4))*sign(v4) - (M_PI*hvs(-(v1/v4))*Power(rlog(Abs(-v1 + v4)),2)*sign(v4))/2. + (Power(M_PI,3)*hvs(v1 - v4)*hvs(-(v1/v4))*Power(sign(v4),2))/2. - (Power(M_PI,3)*hvs(-(v1/v4))*hvs(-v4)*Power(sign(v4),2))/2. + (Power(M_PI,3)*hvs(-(v1/v4))*Power(sign(v4),3))/6. ;
    }
 return result19;
}  
