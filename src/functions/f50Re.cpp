
#include "../header.h"


double integrandRe50 (double t, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double bcphi5Re(0.);
   double bcphi5Im(0.);
   double v1i, v2i, v3i, v4i, v5i, val;

   double d37s3 =9.03301789873836469269;
   double d38s3 = 22.0377098912592295221202221000567;
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=0;}
 if((v1f>0)&&(v2f<0)&&(v3f>0)&&(v4f>0)&&(v5f<0)){val=1;}
 if((v1f<0)&&(v2f>0)&&(v3f<0)&&(v4f>0)&&(v5f>0)){val=2;}
 if((v1f>0)&&(v2f<0)&&(v3f>0)&&(v4f<0)&&(v5f>0)){val=3;}
 if((v1f>0)&&(v2f>0)&&(v3f<0)&&(v4f>0)&&(v5f<0)){val=4;}
 if((v1f<0)&&(v2f>0)&&(v3f>0)&&(v4f<0)&&(v5f>0)){val=5;}
 if((v1f>0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=6;}
 if((v1f<0)&&(v2f>0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=7;}
 if((v1f<0)&&(v2f<0)&&(v3f>0)&&(v4f<0)&&(v5f<0)){val=8;}
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f>0)&&(v5f<0)){val=9;}
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f>0)){val=10;}

if(val==0){ v1i = -1.; v2i = -1.; v3i = -1.; v4i = -1.; v5i = -1.;
      bcphi5Re = 9.0330178987383646926900837570428; bcphi5Im = 0;};
if(val==1){ v1i = 1.; v2i = -1./3.; v3i = 1./3.; v4i = 1./3.; v5i = -1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==2){ v1i = -1./3.; v2i = 1.; v3i = -1./3.; v4i = 1./3.; v5i = 1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==3){ v1i = 1./3.; v2i = -1./3.; v3i = 1.; v4i = -1./3.; v5i = 1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==4){ v1i = 1./3.; v2i = 1./3.; v3i = -1./3.; v4i = 1.; v5i = -1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==5){ v1i = -1./3.; v2i = 1./3.; v3i = 1./3.; v4i = -1./3.; v5i = 1.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==6){ v1i = 1./3.; v2i = -1./3.; v3i = -1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==7){ v1i = -1./3.; v2i = 1./3.; v3i = -1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==8){ v1i = -1./3.; v2i = -1./3.; v3i = 1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==9){ v1i = -1./3.; v2i = -1./3.; v3i = -1./3.; v4i = 1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==10){ v1i = -1./3.; v2i = -1./3.; v3i = -1./3.; v4i = -1./3.; v5i = 1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};

   double v1=-v1i*(1.-t)-v1f*t;
   double v2=-v2i*(1.-t)-v2f*t;
   double v3=-v3i*(1.-t)-v3f*t;
   double v4=-v4i*(1.-t)-v4f*t;
   double v5=-v5i*(1.-t)-v5f*t;

   double zeta3=1.2020569031595942;
   
   double integrandRe50=  (bcphi5Re*(-v1f + v1i - v2f + v2i + v4f - v4i))/(v1 + v2 - v4) + 
  (bcphi5Re*(v1f - v1i - v3f + v3i - v4f + v4i))/(-v1 + v3 + v4) + 
  (4*bcphi5Re*(Power(t*(v1f - v1i) + v1i,2)*(v2f - v2i - v5f + v5i)*
        (v2i - v5i + t*(v2f - v2i - v5f + v5i)) + 
       (v1f - v1i)*(t*(v1f - v1i) + v1i)*
        Power(v2i - v5i + t*(v2f - v2i - v5f + v5i),2) + 
       ((t*(v2f - v2i) + v2i)*(t*(v3f - v3i) + v3i) - 
          (t*(v4f - v4i) + v4i)*(v3i - v5i + t*(v3f - v3i - v5f + v5i)))*
        (v2i*(v3f - 2*v3i) + v2f*v3i - v3i*v4f - v3f*v4i + 2*v3i*v4i + 
          v4i*v5f + v4f*v5i - 2*v4i*v5i + 
          2*t*(v2f*(v3f - v3i) + v2i*(-v3f + v3i) - 
             (v4f - v4i)*(v3f - v3i - v5f + v5i))) + 
       (-v1f + v1i)*(Power(t*(v2f - v2i) + v2i,2)*(t*(v3f - v3i) + v3i) + 
          (t*(v4f - v4i) + v4i)*(t*(v5f - v5i) + v5i)*
           (-v3i + t*(-v3f + v3i + v5f - v5i) + v5i) + 
          (-v2i + t*(-v2f + v2i))*
           ((t*(v4f - v4i) + v4i)*(t*(v5f - v5i) + v5i) + 
             (t*(v3f - v3i) + v3i)*(v4i + t*(v4f - v4i + v5f - v5i) + v5i))\
) + (-v1i + t*(-v1f + v1i))*(Power(t*(v2f - v2i) + v2i,2)*(v3f - v3i) + 
          2*(v2f - v2i)*(t*(v2f - v2i) + v2i)*(t*(v3f - v3i) + v3i) - 
          (t*(v4f - v4i) + v4i)*(v3f - v3i - v5f + v5i)*
           (t*(v5f - v5i) + v5i) + 
          (t*(v4f - v4i) + v4i)*(v5f - v5i)*
           (-v3i + t*(-v3f + v3i + v5f - v5i) + v5i) + 
          (v4f - v4i)*(t*(v5f - v5i) + v5i)*
           (-v3i + t*(-v3f + v3i + v5f - v5i) + v5i) + 
          (-v2f + v2i)*((t*(v4f - v4i) + v4i)*(t*(v5f - v5i) + v5i) + 
             (t*(v3f - v3i) + v3i)*(v4i + t*(v4f - v4i + v5f - v5i) + v5i)) \
- (t*(v2f - v2i) + v2i)*(v3f*v4i + v4i*v5f + 
             v3i*(v4f - 2*v4i + v5f - 2*v5i) + v3f*v5i + v4f*v5i - 
             2*v4i*v5i + 2*t*((v4f - v4i)*(v5f - v5i) + 
                v3f*(v4f - v4i + v5f - v5i) + v3i*(-v4f + v4i - v5f + v5i)))\
)))/(Power(v1,2)*Power(v2 - v5,2) + Power(v2*v3 + v4*(-v3 + v5),2) + 
     2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + v2*(v4*v5 + v3*(v4 + v5)))) \
- (3*rlog((v1f - v3f - v4f)/(-v1 + v3 + v4))*
     (((-2*(Power(v2,2)*Power(-v1 + v3,2) + 
               Power(v3*v4 + (v1 - v4)*v5,2) + 
               2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
                  v3*(v1*v5 + v4*(v1 + v5))))*
             (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (-(v1*v2) + v2*v3 - v3*v4 - v1*v5 + v4*v5)*
             (2*Power(-(t*v2f) - (1 - t)*v2i,2)*(v1f - v1i - v3f + v3i)*
                (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
               2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
                Power(t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i,2) + 
               2*((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
                  (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
                   (-v5f + v5i) + 
                  (-v1f + v1i + v4f - v4i)*(-(t*v5f) - (1 - t)*v5i))*
                ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
                   (-(t*v5f) - (1 - t)*v5i)) + 
               2*(-(t*v2f) - (1 - t)*v2i)*
                (-(Power(-(t*v3f) - (1 - t)*v3i,2)*(-v4f + v4i)) - 
                  2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
                   (-(t*v4f) - (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                   (-v5f + v5i) + 
                  (-(t*v1f) - (1 - t)*v1i)*(v1f - v1i - v4f + v4i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-v1f + v1i)*
                   (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v3f) - (1 - t)*v3i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
                     (-(t*v4f) - (1 - t)*v4i)*
                      (-v1f + v1i - v5f + v5i) + 
                     (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-v4f + v4i)*
                      (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i)) + 
                  (-v3f + v3i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-(t*v4f) - (1 - t)*v4i)*
                      (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i))) + 
               2*(-v2f + v2i)*
                (-(Power(-(t*v3f) - (1 - t)*v3i,2)*
                     (-(t*v4f) - (1 - t)*v4i)) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v3f) - (1 - t)*v3i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-(t*v4f) - (1 - t)*v4i)*
                      (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i)))))*
          (-(hvs(-(v1/v3))*(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
               rlog(1 - v1/v3)) + 
            (M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*rlog(1 - v4/v1) + 
            M_PI*hvs(-v1)*rlog(Abs(v1)) - M_PI*hvs(-v4)*rlog(Abs(v1)) - 
            M_PI*hvs(-v3)*rlog(Abs(v3)) + M_PI*hvs(-v4)*rlog(Abs(v3)) - 
            M_PI*hvs(-v1)*rlog(Abs(v4)) + M_PI*hvs(-v3)*rlog(Abs(v4))))/
        (4.*v1*v2*(-v1 + v3 + v4)*v5*
          sqrt(-(Power(v2,2)*Power(-v1 + v3,2)) - 
            Power(v3*v4 + (v1 - v4)*v5,2) - 
            2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
               v3*(v1*v5 + v4*(v1 + v5))))) + 
       ((-2*(Power(v1*v2 + v3*(-v2 + v4),2) + 
               2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + 
                  v1*(v3*v4 + v2*(v3 + v4)))*v5 + 
               Power(v1 - v4,2)*Power(v5,2))*
             (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (-(v1*v2) + v2*v3 - v3*v4 + v1*v5 - v4*v5)*
             (2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
                  (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                  (-(t*v3f) - (1 - t)*v3i)*(v2f - v2i - v4f + v4i) + 
                  (-v3f + v3i)*
                   (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i))*
                ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                  (-(t*v3f) - (1 - t)*v3i)*
                   (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i)) + 
               2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*
                     (-(t*v2f) - (1 - t)*v2i)) + 
                  (-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   ((-(t*v3f) - (1 - t)*v3i)*
                      (-(t*v4f) - (1 - t)*v4i) + 
                     (-(t*v2f) - (1 - t)*v2i)*
                      (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
                (-v5f + v5i) + 
               2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*(-v2f + v2i)) - 
                  2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
                   (-(t*v2f) - (1 - t)*v2i) + 
                  (-(t*v3f) - (1 - t)*v3i)*(-v2f + v2i + v4f - v4i)*
                   (-(t*v4f) - (1 - t)*v4i) + 
                  (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                  (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   ((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
                     (-(t*v2f) - (1 - t)*v2i)*
                      (-v3f + v3i - v4f + v4i) + 
                     (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                     (-v2f + v2i)*
                      (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)) \
+ (-v1f + v1i)*((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                     (-(t*v2f) - (1 - t)*v2i)*
                      (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
                (-(t*v5f) - (1 - t)*v5i) + 
               2*Power(-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i,2)*
                (-v5f + v5i)*(-(t*v5f) - (1 - t)*v5i) + 
               2*(-v1f + v1i + v4f - v4i)*
                (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
                Power(-(t*v5f) - (1 - t)*v5i,2)))*
          (hvs(-(v2/v4))*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(1 - v2/v4) - 
            (M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*rlog(1 - v4/v1) - 
            M_PI*hvs(-v1)*rlog(Abs(v1)) + M_PI*hvs(-v2)*rlog(Abs(v1)) + 
            M_PI*hvs(-v1)*rlog(Abs(v2)) - M_PI*hvs(-v4)*rlog(Abs(v2)) - 
            M_PI*hvs(-v2)*rlog(Abs(v4)) + M_PI*hvs(-v4)*rlog(Abs(v4))))/
        (4.*v3*(v1 + v2 - v4)*v4*v5*
          sqrt(-Power(v1*v2 + v3*(-v2 + v4),2) - 
            2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + 
               v1*(v3*v4 + v2*(v3 + v4)))*v5 - Power(v1 - v4,2)*Power(v5,2)\
)) + ((-2*(Power(v3,2)*Power(-v2 + v4,2) + 
               Power(v1*(v2 - v5) + v4*v5,2) + 
               2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
                  v4*(v1*v2 + (v1 + v2)*v5)))*
             (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (-(v1*v2) - v2*v3 + v3*v4 + v1*v5 - v4*v5)*
             (2*Power(-(t*v3f) - (1 - t)*v3i,2)*(v2f - v2i - v4f + v4i)*
                (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i) + 
               2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
                Power(t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i,2) + 
               2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i + v5f - v5i) + 
                  (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
                  (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                  (-v1f + v1i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i))*
                ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i)) + 
               2*(-(t*v3f) - (1 - t)*v3i)*
                (-(Power(-(t*v4f) - (1 - t)*v4i,2)*(-v5f + v5i)) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
                   (v2f - v2i - v5f + v5i) - 
                  2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)*
                   (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                  (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i)*
                   (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                  (-(t*v4f) - (1 - t)*v4i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
                     (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                     (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                      (-v5f + v5i) + 
                     (-v1f + v1i - v2f + v2i)*(-(t*v5f) - (1 - t)*v5i)) \
+ (-v4f + v4i)*((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                     (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                      (-(t*v5f) - (1 - t)*v5i))) + 
               2*(-v3f + v3i)*
                (-(Power(-(t*v4f) - (1 - t)*v4i,2)*
                     (-(t*v5f) - (1 - t)*v5i)) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
                   (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                  (-(t*v4f) - (1 - t)*v4i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                     (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                      (-(t*v5f) - (1 - t)*v5i)))))*
          (-(hvs(-(v2/v4))*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
               rlog(1 - v2/v4)) + 
            (M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*rlog(1 - v5/v2) + 
            M_PI*hvs(-v2)*rlog(Abs(v2)) - M_PI*hvs(-v5)*rlog(Abs(v2)) - 
            M_PI*hvs(-v4)*rlog(Abs(v4)) + M_PI*hvs(-v5)*rlog(Abs(v4)) - 
            M_PI*hvs(-v2)*rlog(Abs(v5)) + M_PI*hvs(-v4)*rlog(Abs(v5))))/
        (4.*v1*v2*v3*(-v2 + v4 + v5)*
          sqrt(-(Power(v3,2)*Power(-v2 + v4,2)) - 
            Power(v1*(v2 - v5) + v4*v5,2) - 
            2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
               v4*(v1*v2 + (v1 + v2)*v5)))) + 
       ((-2*(Power(v4,2)*Power(-v3 + v5,2) + 
               Power(v2*(-v1 + v3) + v1*v5,2) + 
               2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - 
                  v1*Power(v5,2)))*
             ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (v1*v2 - v2*v3 - v3*v4 - v1*v5 + v4*v5)*
             (2*Power(-(t*v4f) - (1 - t)*v4i,2)*(v3f - v3i - v5f + v5i)*
                (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i) + 
               2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
                Power(t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i,2) + 
               2*((-(t*v2f) - (1 - t)*v2i)*(v1f - v1i - v3f + v3i) + 
                  (-v2f + v2i)*
                   (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
                  (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i))*
                ((-(t*v2f) - (1 - t)*v2i)*
                   (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i)) + 
               2*(-(t*v4f) - (1 - t)*v4i)*
                ((-(t*v2f) - (1 - t)*v2i)*(-v1f + v1i + v3f - v3i)*
                   (-(t*v3f) - (1 - t)*v3i) + 
                  (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i)*
                   (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                  (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i)*
                   (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                  ((-(t*v2f) - (1 - t)*v2i)*
                      (-(t*v3f) - (1 - t)*v3i) + 
                     (-(t*v1f) - (1 - t)*v1i)*
                      (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                   (-v5f + v5i) + 
                  ((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
                     (-(t*v1f) - (1 - t)*v1i)*
                      (-v2f + v2i - v3f + v3i) + 
                     (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                     (-v1f + v1i)*
                      (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                   (-(t*v5f) - (1 - t)*v5i) - 
                  2*(-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i)*
                   (-(t*v5f) - (1 - t)*v5i) - 
                  (-v1f + v1i)*Power(-(t*v5f) - (1 - t)*v5i,2)) + 
               2*(-v4f + v4i)*
                ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i)*
                   (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                  ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                     (-(t*v1f) - (1 - t)*v1i)*
                      (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                   (-(t*v5f) - (1 - t)*v5i) - 
                  (-(t*v1f) - (1 - t)*v1i)*
                   Power(-(t*v5f) - (1 - t)*v5i,2))))*
          (hvs(-(v1/v3))*(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(1 - v1/v3) - 
            hvs(-(v3/v5))*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
             rlog(1 - v3/v5) - M_PI*hvs(-v3)*rlog(Abs(v1)) + 
            M_PI*hvs(-v5)*rlog(Abs(v1)) - M_PI*hvs(-v1)*rlog(Abs(v3)) + 
            M_PI*hvs(-v3)*rlog(Abs(v3)) + M_PI*hvs(-v1)*rlog(Abs(v5)) - 
            M_PI*hvs(-v5)*rlog(Abs(v5))))/
        (4.*v2*v3*v4*(v1 - v3 + v5)*
          sqrt(-(Power(v4,2)*Power(-v3 + v5,2)) - 
            Power(v2*(-v1 + v3) + v1*v5,2) - 
            2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - 
               v1*Power(v5,2)))) + 
       ((-2*(Power(v1,2)*Power(v2 - v5,2) + 
               Power(v2*v3 + v4*(-v3 + v5),2) + 
               2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
                  v2*(v4*v5 + v3*(v4 + v5))))*
             ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (v1*v2 - v2*v3 + v3*v4 - v1*v5 - v4*v5)*
             (2*Power(-(t*v1f) - (1 - t)*v1i,2)*(-v2f + v2i + v5f - v5i)*
                (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i) + 
               2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
                Power(-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i,2) + 
               2*((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
                  (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                  (-(t*v4f) - (1 - t)*v4i)*(v3f - v3i - v5f + v5i) + 
                  (-v4f + v4i)*
                   (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i))*
                ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                  (-(t*v4f) - (1 - t)*v4i)*
                   (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i)) + 
               2*(-(t*v1f) - (1 - t)*v1i)*
                (-(Power(-(t*v2f) - (1 - t)*v2i,2)*(-v3f + v3i)) - 
                  2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
                   (-(t*v3f) - (1 - t)*v3i) + 
                  (-(t*v4f) - (1 - t)*v4i)*(-v3f + v3i + v5f - v5i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i)*
                   (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                  (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)*
                   (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                  (-(t*v2f) - (1 - t)*v2i)*
                   ((-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
                     (-(t*v3f) - (1 - t)*v3i)*
                      (-v4f + v4i - v5f + v5i) + 
                     (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-v3f + v3i)*
                      (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i)) + 
                  (-v2f + v2i)*
                   ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-(t*v3f) - (1 - t)*v3i)*
                      (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i))) + 
               2*(-v1f + v1i)*
                (-(Power(-(t*v2f) - (1 - t)*v2i,2)*
                     (-(t*v3f) - (1 - t)*v3i)) + 
                  (-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i)*
                   (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                  (-(t*v2f) - (1 - t)*v2i)*
                   ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-(t*v3f) - (1 - t)*v3i)*
                      (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i)))))*
          (hvs(-(v3/v5))*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(1 - v3/v5) - 
            (M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*rlog(1 - v5/v2) - 
            M_PI*hvs(-v2)*rlog(Abs(v2)) + M_PI*hvs(-v3)*rlog(Abs(v2)) + 
            M_PI*hvs(-v2)*rlog(Abs(v3)) - M_PI*hvs(-v5)*rlog(Abs(v3)) - 
            M_PI*hvs(-v3)*rlog(Abs(v5)) + M_PI*hvs(-v5)*rlog(Abs(v5))))/
        (4.*v1*v4*(v2 + v3 - v5)*v5*
          sqrt(-(Power(v1,2)*Power(v2 - v5,2)) - 
            Power(v2*v3 + v4*(-v3 + v5),2) - 
            2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
               v2*(v4*v5 + v3*(v4 + v5)))))))/2. - 
  (3*rlog((-v1f - v2f + v4f)/(v1 + v2 - v4))*
     (((-2*(Power(v2,2)*Power(-v1 + v3,2) + 
               Power(v3*v4 + (v1 - v4)*v5,2) + 
               2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
                  v3*(v1*v5 + v4*(v1 + v5))))*
             (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (-(v1*v2) + v2*v3 - v3*v4 - v1*v5 + v4*v5)*
             (2*Power(-(t*v2f) - (1 - t)*v2i,2)*(v1f - v1i - v3f + v3i)*
                (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
               2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
                Power(t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i,2) + 
               2*((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
                  (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
                   (-v5f + v5i) + 
                  (-v1f + v1i + v4f - v4i)*(-(t*v5f) - (1 - t)*v5i))*
                ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
                   (-(t*v5f) - (1 - t)*v5i)) + 
               2*(-(t*v2f) - (1 - t)*v2i)*
                (-(Power(-(t*v3f) - (1 - t)*v3i,2)*(-v4f + v4i)) - 
                  2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
                   (-(t*v4f) - (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                   (-v5f + v5i) + 
                  (-(t*v1f) - (1 - t)*v1i)*(v1f - v1i - v4f + v4i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-v1f + v1i)*
                   (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v3f) - (1 - t)*v3i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
                     (-(t*v4f) - (1 - t)*v4i)*
                      (-v1f + v1i - v5f + v5i) + 
                     (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-v4f + v4i)*
                      (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i)) + 
                  (-v3f + v3i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-(t*v4f) - (1 - t)*v4i)*
                      (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i))) + 
               2*(-v2f + v2i)*
                (-(Power(-(t*v3f) - (1 - t)*v3i,2)*
                     (-(t*v4f) - (1 - t)*v4i)) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v3f) - (1 - t)*v3i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-(t*v4f) - (1 - t)*v4i)*
                      (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i)))))*
          (-(hvs(-(v1/v3))*(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
               rlog(1 - v1/v3)) + 
            (M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*rlog(1 - v4/v1) + 
            M_PI*hvs(-v1)*rlog(Abs(v1)) - M_PI*hvs(-v4)*rlog(Abs(v1)) - 
            M_PI*hvs(-v3)*rlog(Abs(v3)) + M_PI*hvs(-v4)*rlog(Abs(v3)) - 
            M_PI*hvs(-v1)*rlog(Abs(v4)) + M_PI*hvs(-v3)*rlog(Abs(v4))))/
        (4.*v1*v2*(-v1 + v3 + v4)*v5*
          sqrt(-(Power(v2,2)*Power(-v1 + v3,2)) - 
            Power(v3*v4 + (v1 - v4)*v5,2) - 
            2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
               v3*(v1*v5 + v4*(v1 + v5))))) + 
       ((-2*(Power(v1*v2 + v3*(-v2 + v4),2) + 
               2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + 
                  v1*(v3*v4 + v2*(v3 + v4)))*v5 + 
               Power(v1 - v4,2)*Power(v5,2))*
             (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (-(v1*v2) + v2*v3 - v3*v4 + v1*v5 - v4*v5)*
             (2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
                  (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                  (-(t*v3f) - (1 - t)*v3i)*(v2f - v2i - v4f + v4i) + 
                  (-v3f + v3i)*
                   (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i))*
                ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                  (-(t*v3f) - (1 - t)*v3i)*
                   (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i)) + 
               2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*
                     (-(t*v2f) - (1 - t)*v2i)) + 
                  (-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   ((-(t*v3f) - (1 - t)*v3i)*
                      (-(t*v4f) - (1 - t)*v4i) + 
                     (-(t*v2f) - (1 - t)*v2i)*
                      (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
                (-v5f + v5i) + 
               2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*(-v2f + v2i)) - 
                  2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
                   (-(t*v2f) - (1 - t)*v2i) + 
                  (-(t*v3f) - (1 - t)*v3i)*(-v2f + v2i + v4f - v4i)*
                   (-(t*v4f) - (1 - t)*v4i) + 
                  (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                  (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   ((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
                     (-(t*v2f) - (1 - t)*v2i)*
                      (-v3f + v3i - v4f + v4i) + 
                     (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                     (-v2f + v2i)*
                      (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)) \
+ (-v1f + v1i)*((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                     (-(t*v2f) - (1 - t)*v2i)*
                      (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
                (-(t*v5f) - (1 - t)*v5i) + 
               2*Power(-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i,2)*
                (-v5f + v5i)*(-(t*v5f) - (1 - t)*v5i) + 
               2*(-v1f + v1i + v4f - v4i)*
                (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
                Power(-(t*v5f) - (1 - t)*v5i,2)))*
          (hvs(-(v2/v4))*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(1 - v2/v4) - 
            (M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*rlog(1 - v4/v1) - 
            M_PI*hvs(-v1)*rlog(Abs(v1)) + M_PI*hvs(-v2)*rlog(Abs(v1)) + 
            M_PI*hvs(-v1)*rlog(Abs(v2)) - M_PI*hvs(-v4)*rlog(Abs(v2)) - 
            M_PI*hvs(-v2)*rlog(Abs(v4)) + M_PI*hvs(-v4)*rlog(Abs(v4))))/
        (4.*v3*(v1 + v2 - v4)*v4*v5*
          sqrt(-Power(v1*v2 + v3*(-v2 + v4),2) - 
            2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + 
               v1*(v3*v4 + v2*(v3 + v4)))*v5 - Power(v1 - v4,2)*Power(v5,2)\
)) + ((-2*(Power(v3,2)*Power(-v2 + v4,2) + 
               Power(v1*(v2 - v5) + v4*v5,2) + 
               2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
                  v4*(v1*v2 + (v1 + v2)*v5)))*
             (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (-(v1*v2) - v2*v3 + v3*v4 + v1*v5 - v4*v5)*
             (2*Power(-(t*v3f) - (1 - t)*v3i,2)*(v2f - v2i - v4f + v4i)*
                (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i) + 
               2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
                Power(t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i,2) + 
               2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i + v5f - v5i) + 
                  (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
                  (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                  (-v1f + v1i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i))*
                ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v1f) - (1 - t)*v1i)*
                   (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i)) + 
               2*(-(t*v3f) - (1 - t)*v3i)*
                (-(Power(-(t*v4f) - (1 - t)*v4i,2)*(-v5f + v5i)) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
                   (v2f - v2i - v5f + v5i) - 
                  2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)*
                   (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                  (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i)*
                   (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                  (-(t*v4f) - (1 - t)*v4i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
                     (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                     (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                      (-v5f + v5i) + 
                     (-v1f + v1i - v2f + v2i)*(-(t*v5f) - (1 - t)*v5i)) \
+ (-v4f + v4i)*((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                     (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                      (-(t*v5f) - (1 - t)*v5i))) + 
               2*(-v3f + v3i)*
                (-(Power(-(t*v4f) - (1 - t)*v4i,2)*
                     (-(t*v5f) - (1 - t)*v5i)) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
                   (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                  (-(t*v4f) - (1 - t)*v4i)*
                   ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                     (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                      (-(t*v5f) - (1 - t)*v5i)))))*
          (-(hvs(-(v2/v4))*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
               rlog(1 - v2/v4)) + 
            (M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*rlog(1 - v5/v2) + 
            M_PI*hvs(-v2)*rlog(Abs(v2)) - M_PI*hvs(-v5)*rlog(Abs(v2)) - 
            M_PI*hvs(-v4)*rlog(Abs(v4)) + M_PI*hvs(-v5)*rlog(Abs(v4)) - 
            M_PI*hvs(-v2)*rlog(Abs(v5)) + M_PI*hvs(-v4)*rlog(Abs(v5))))/
        (4.*v1*v2*v3*(-v2 + v4 + v5)*
          sqrt(-(Power(v3,2)*Power(-v2 + v4,2)) - 
            Power(v1*(v2 - v5) + v4*v5,2) - 
            2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
               v4*(v1*v2 + (v1 + v2)*v5)))) + 
       ((-2*(Power(v4,2)*Power(-v3 + v5,2) + 
               Power(v2*(-v1 + v3) + v1*v5,2) + 
               2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - 
                  v1*Power(v5,2)))*
             ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (v1*v2 - v2*v3 - v3*v4 - v1*v5 + v4*v5)*
             (2*Power(-(t*v4f) - (1 - t)*v4i,2)*(v3f - v3i - v5f + v5i)*
                (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i) + 
               2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
                Power(t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i,2) + 
               2*((-(t*v2f) - (1 - t)*v2i)*(v1f - v1i - v3f + v3i) + 
                  (-v2f + v2i)*
                   (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
                  (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i))*
                ((-(t*v2f) - (1 - t)*v2i)*
                   (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
                  (-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i)) + 
               2*(-(t*v4f) - (1 - t)*v4i)*
                ((-(t*v2f) - (1 - t)*v2i)*(-v1f + v1i + v3f - v3i)*
                   (-(t*v3f) - (1 - t)*v3i) + 
                  (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i)*
                   (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                  (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i)*
                   (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                  ((-(t*v2f) - (1 - t)*v2i)*
                      (-(t*v3f) - (1 - t)*v3i) + 
                     (-(t*v1f) - (1 - t)*v1i)*
                      (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                   (-v5f + v5i) + 
                  ((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
                     (-(t*v1f) - (1 - t)*v1i)*
                      (-v2f + v2i - v3f + v3i) + 
                     (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                     (-v1f + v1i)*
                      (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                   (-(t*v5f) - (1 - t)*v5i) - 
                  2*(-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i)*
                   (-(t*v5f) - (1 - t)*v5i) - 
                  (-v1f + v1i)*Power(-(t*v5f) - (1 - t)*v5i,2)) + 
               2*(-v4f + v4i)*
                ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i)*
                   (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                  ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                     (-(t*v1f) - (1 - t)*v1i)*
                      (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                   (-(t*v5f) - (1 - t)*v5i) - 
                  (-(t*v1f) - (1 - t)*v1i)*
                   Power(-(t*v5f) - (1 - t)*v5i,2))))*
          (hvs(-(v1/v3))*(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(1 - v1/v3) - 
            hvs(-(v3/v5))*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
             rlog(1 - v3/v5) - M_PI*hvs(-v3)*rlog(Abs(v1)) + 
            M_PI*hvs(-v5)*rlog(Abs(v1)) - M_PI*hvs(-v1)*rlog(Abs(v3)) + 
            M_PI*hvs(-v3)*rlog(Abs(v3)) + M_PI*hvs(-v1)*rlog(Abs(v5)) - 
            M_PI*hvs(-v5)*rlog(Abs(v5))))/
        (4.*v2*v3*v4*(v1 - v3 + v5)*
          sqrt(-(Power(v4,2)*Power(-v3 + v5,2)) - 
            Power(v2*(-v1 + v3) + v1*v5,2) - 
            2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - 
               v1*Power(v5,2)))) + 
       ((-2*(Power(v1,2)*Power(v2 - v5,2) + 
               Power(v2*v3 + v4*(-v3 + v5),2) + 
               2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
                  v2*(v4*v5 + v3*(v4 + v5))))*
             ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
               (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
               (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
               (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
               (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
               (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
               (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
               (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
               (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
               (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
            (v1*v2 - v2*v3 + v3*v4 - v1*v5 - v4*v5)*
             (2*Power(-(t*v1f) - (1 - t)*v1i,2)*(-v2f + v2i + v5f - v5i)*
                (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i) + 
               2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
                Power(-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i,2) + 
               2*((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
                  (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                  (-(t*v4f) - (1 - t)*v4i)*(v3f - v3i - v5f + v5i) + 
                  (-v4f + v4i)*
                   (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i))*
                ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                  (-(t*v4f) - (1 - t)*v4i)*
                   (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i)) + 
               2*(-(t*v1f) - (1 - t)*v1i)*
                (-(Power(-(t*v2f) - (1 - t)*v2i,2)*(-v3f + v3i)) - 
                  2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
                   (-(t*v3f) - (1 - t)*v3i) + 
                  (-(t*v4f) - (1 - t)*v4i)*(-v3f + v3i + v5f - v5i)*
                   (-(t*v5f) - (1 - t)*v5i) + 
                  (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i)*
                   (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                  (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)*
                   (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                  (-(t*v2f) - (1 - t)*v2i)*
                   ((-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
                     (-(t*v3f) - (1 - t)*v3i)*
                      (-v4f + v4i - v5f + v5i) + 
                     (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-v3f + v3i)*
                      (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i)) + 
                  (-v2f + v2i)*
                   ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-(t*v3f) - (1 - t)*v3i)*
                      (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i))) + 
               2*(-v1f + v1i)*
                (-(Power(-(t*v2f) - (1 - t)*v2i,2)*
                     (-(t*v3f) - (1 - t)*v3i)) + 
                  (-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i)*
                   (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                  (-(t*v2f) - (1 - t)*v2i)*
                   ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                     (-(t*v3f) - (1 - t)*v3i)*
                      (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i)))))*
          (hvs(-(v3/v5))*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(1 - v3/v5) - 
            (M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*rlog(1 - v5/v2) - 
            M_PI*hvs(-v2)*rlog(Abs(v2)) + M_PI*hvs(-v3)*rlog(Abs(v2)) + 
            M_PI*hvs(-v2)*rlog(Abs(v3)) - M_PI*hvs(-v5)*rlog(Abs(v3)) - 
            M_PI*hvs(-v3)*rlog(Abs(v5)) + M_PI*hvs(-v5)*rlog(Abs(v5))))/
        (4.*v1*v4*(v2 + v3 - v5)*v5*
          sqrt(-(Power(v1,2)*Power(v2 - v5,2)) - 
            Power(v2*v3 + v4*(-v3 + v5),2) - 
            2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
               v2*(v4*v5 + v3*(v4 + v5)))))))/2. - 
  3*rlog((Power(v2f*v3f - v4f*(v3f - v5f),2) + 
       Power(v1f,2)*Power(-v2f + v5f,2) - 
       2*v1f*(Power(v2f,2)*v3f + v4f*v5f*(-v3f + v5f) - 
          v2f*(-(v3f*(-v4f - v5f)) + v4f*v5f)))/
     (Power(v1,2)*Power(v2 - v5,2) + Power(v2*v3 + v4*(-v3 + v5),2) + 
       2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
          v2*(v4*v5 + v3*(v4 + v5)))))*
   (((-2*(Power(v2,2)*Power(-v1 + v3,2) + 
             Power(v3*v4 + (v1 - v4)*v5,2) + 
             2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
                v3*(v1*v5 + v4*(v1 + v5))))*
           (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
             (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
             (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
             (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
             (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
          (-(v1*v2) + v2*v3 - v3*v4 - v1*v5 + v4*v5)*
           (2*Power(-(t*v2f) - (1 - t)*v2i,2)*(v1f - v1i - v3f + v3i)*
              (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
             2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
              Power(t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i,2) + 
             2*((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
                (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
                 (-v5f + v5i) + 
                (-v1f + v1i + v4f - v4i)*(-(t*v5f) - (1 - t)*v5i))*
              ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
                 (-(t*v5f) - (1 - t)*v5i)) + 
             2*(-(t*v2f) - (1 - t)*v2i)*
              (-(Power(-(t*v3f) - (1 - t)*v3i,2)*(-v4f + v4i)) - 
                2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v4f) - (1 - t)*v4i) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                 (-v5f + v5i) + 
                (-(t*v1f) - (1 - t)*v1i)*(v1f - v1i - v4f + v4i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-v1f + v1i)*
                 (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v3f) - (1 - t)*v3i)*
                 ((-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
                   (-(t*v4f) - (1 - t)*v4i)*(-v1f + v1i - v5f + v5i) + 
                   (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                   (-v4f + v4i)*
                    (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i)) + 
                (-v3f + v3i)*((-(t*v1f) - (1 - t)*v1i)*
                    (-(t*v5f) - (1 - t)*v5i) + 
                   (-(t*v4f) - (1 - t)*v4i)*
                    (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i))) + 
             2*(-v2f + v2i)*(-(Power(-(t*v3f) - (1 - t)*v3i,2)*
                   (-(t*v4f) - (1 - t)*v4i)) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v3f) - (1 - t)*v3i)*
                 ((-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                   (-(t*v4f) - (1 - t)*v4i)*
                    (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i)))))*
        (-(hvs(-(v1/v3))*(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(1 - v1/v3)) + 
          (M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*rlog(1 - v4/v1) + 
          M_PI*hvs(-v1)*rlog(Abs(v1)) - M_PI*hvs(-v4)*rlog(Abs(v1)) - 
          M_PI*hvs(-v3)*rlog(Abs(v3)) + M_PI*hvs(-v4)*rlog(Abs(v3)) - 
          M_PI*hvs(-v1)*rlog(Abs(v4)) + M_PI*hvs(-v3)*rlog(Abs(v4))))/
      (4.*v1*v2*(-v1 + v3 + v4)*v5*
        sqrt(-(Power(v2,2)*Power(-v1 + v3,2)) - 
          Power(v3*v4 + (v1 - v4)*v5,2) - 
          2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
             v3*(v1*v5 + v4*(v1 + v5))))) + 
     ((-2*(Power(v1*v2 + v3*(-v2 + v4),2) + 
             2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + 
                v1*(v3*v4 + v2*(v3 + v4)))*v5 + 
             Power(v1 - v4,2)*Power(v5,2))*
           (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
             (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
             (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
             (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
          (-(v1*v2) + v2*v3 - v3*v4 + v1*v5 - v4*v5)*
           (2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
                (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v3f) - (1 - t)*v3i)*(v2f - v2i - v4f + v4i) + 
                (-v3f + v3i)*(t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i)\
)*((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v3f) - (1 - t)*v3i)*
                 (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i)) + 
             2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*
                   (-(t*v2f) - (1 - t)*v2i)) + 
                (-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                   (-(t*v2f) - (1 - t)*v2i)*
                    (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
              (-v5f + v5i) + 2*
              (-(Power(-(t*v1f) - (1 - t)*v1i,2)*(-v2f + v2i)) - 
                2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v3f) - (1 - t)*v3i)*(-v2f + v2i + v4f - v4i)*
                 (-(t*v4f) - (1 - t)*v4i) + 
                (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i)*
                 (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 ((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
                   (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i - v4f + v4i) + 
                   (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                   (-v2f + v2i)*
                    (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)) + 
                (-v1f + v1i)*
                 ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                   (-(t*v2f) - (1 - t)*v2i)*
                    (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
              (-(t*v5f) - (1 - t)*v5i) + 
             2*Power(-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i,2)*
              (-v5f + v5i)*(-(t*v5f) - (1 - t)*v5i) + 
             2*(-v1f + v1i + v4f - v4i)*
              (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
              Power(-(t*v5f) - (1 - t)*v5i,2)))*
        (hvs(-(v2/v4))*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(1 - v2/v4) - 
          (M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*rlog(1 - v4/v1) - 
          M_PI*hvs(-v1)*rlog(Abs(v1)) + M_PI*hvs(-v2)*rlog(Abs(v1)) + 
          M_PI*hvs(-v1)*rlog(Abs(v2)) - M_PI*hvs(-v4)*rlog(Abs(v2)) - 
          M_PI*hvs(-v2)*rlog(Abs(v4)) + M_PI*hvs(-v4)*rlog(Abs(v4))))/
      (4.*v3*(v1 + v2 - v4)*v4*v5*
        sqrt(-Power(v1*v2 + v3*(-v2 + v4),2) - 
          2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + 
             v1*(v3*v4 + v2*(v3 + v4)))*v5 - Power(v1 - v4,2)*Power(v5,2))) \
+ ((-2*(Power(v3,2)*Power(-v2 + v4,2) + Power(v1*(v2 - v5) + v4*v5,2) + 
             2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
                v4*(v1*v2 + (v1 + v2)*v5)))*
           (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
             (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
             (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
          (-(v1*v2) - v2*v3 + v3*v4 + v1*v5 - v4*v5)*
           (2*Power(-(t*v3f) - (1 - t)*v3i,2)*(v2f - v2i - v4f + v4i)*
              (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i) + 
             2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
              Power(t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i,2) + 
             2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i + v5f - v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
                (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-v1f + v1i)*
                 (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i))*
              ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i)) + 
             2*(-(t*v3f) - (1 - t)*v3i)*
              (-(Power(-(t*v4f) - (1 - t)*v4i,2)*(-v5f + v5i)) + 
                (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
                 (v2f - v2i - v5f + v5i) - 
                2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)*
                 (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i)*
                 (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*
                 ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
                   (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                   (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                    (-v5f + v5i) + 
                   (-v1f + v1i - v2f + v2i)*(-(t*v5f) - (1 - t)*v5i)) + 
                (-v4f + v4i)*((-(t*v1f) - (1 - t)*v1i)*
                    (-(t*v2f) - (1 - t)*v2i) + 
                   (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                    (-(t*v5f) - (1 - t)*v5i))) + 
             2*(-v3f + v3i)*(-(Power(-(t*v4f) - (1 - t)*v4i,2)*
                   (-(t*v5f) - (1 - t)*v5i)) + 
                (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
                 (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*
                 ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                   (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                    (-(t*v5f) - (1 - t)*v5i)))))*
        (-(hvs(-(v2/v4))*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(1 - v2/v4)) + 
          (M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*rlog(1 - v5/v2) + 
          M_PI*hvs(-v2)*rlog(Abs(v2)) - M_PI*hvs(-v5)*rlog(Abs(v2)) - 
          M_PI*hvs(-v4)*rlog(Abs(v4)) + M_PI*hvs(-v5)*rlog(Abs(v4)) - 
          M_PI*hvs(-v2)*rlog(Abs(v5)) + M_PI*hvs(-v4)*rlog(Abs(v5))))/
      (4.*v1*v2*v3*(-v2 + v4 + v5)*
        sqrt(-(Power(v3,2)*Power(-v2 + v4,2)) - 
          Power(v1*(v2 - v5) + v4*v5,2) - 
          2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
             v4*(v1*v2 + (v1 + v2)*v5)))) + 
     ((-2*(Power(v4,2)*Power(-v3 + v5,2) + 
             Power(v2*(-v1 + v3) + v1*v5,2) + 
             2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - 
                v1*Power(v5,2)))*
           ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
             (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
             (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
             (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
             (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
          (v1*v2 - v2*v3 - v3*v4 - v1*v5 + v4*v5)*
           (2*Power(-(t*v4f) - (1 - t)*v4i,2)*(v3f - v3i - v5f + v5i)*
              (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i) + 
             2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
              Power(t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i,2) + 
             2*((-(t*v2f) - (1 - t)*v2i)*(v1f - v1i - v3f + v3i) + 
                (-v2f + v2i)*
                 (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
                (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
                (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i))*
              ((-(t*v2f) - (1 - t)*v2i)*
                 (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
                (-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i)) + 
             2*(-(t*v4f) - (1 - t)*v4i)*
              ((-(t*v2f) - (1 - t)*v2i)*(-v1f + v1i + v3f - v3i)*
                 (-(t*v3f) - (1 - t)*v3i) + 
                (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i)*
                 (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                   (-(t*v1f) - (1 - t)*v1i)*
                    (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                 (-v5f + v5i) + 
                ((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
                   (-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i - v3f + v3i) + 
                   (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                   (-v1f + v1i)*
                    (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                 (-(t*v5f) - (1 - t)*v5i) - 
                2*(-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i)*
                 (-(t*v5f) - (1 - t)*v5i) - 
                (-v1f + v1i)*Power(-(t*v5f) - (1 - t)*v5i,2)) + 
             2*(-v4f + v4i)*((-(t*v2f) - (1 - t)*v2i)*
                 (-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
                ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                   (-(t*v1f) - (1 - t)*v1i)*
                    (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
                 (-(t*v5f) - (1 - t)*v5i) - 
                (-(t*v1f) - (1 - t)*v1i)*Power(-(t*v5f) - (1 - t)*v5i,2)))\
)*(hvs(-(v1/v3))*(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(1 - v1/v3) - 
          hvs(-(v3/v5))*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(1 - v3/v5) - 
          M_PI*hvs(-v3)*rlog(Abs(v1)) + M_PI*hvs(-v5)*rlog(Abs(v1)) - 
          M_PI*hvs(-v1)*rlog(Abs(v3)) + M_PI*hvs(-v3)*rlog(Abs(v3)) + 
          M_PI*hvs(-v1)*rlog(Abs(v5)) - M_PI*hvs(-v5)*rlog(Abs(v5))))/
      (4.*v2*v3*v4*(v1 - v3 + v5)*
        sqrt(-(Power(v4,2)*Power(-v3 + v5,2)) - 
          Power(v2*(-v1 + v3) + v1*v5,2) - 
          2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - 
             v1*Power(v5,2)))) + 
     ((-2*(Power(v1,2)*Power(v2 - v5,2) + 
             Power(v2*v3 + v4*(-v3 + v5),2) + 
             2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
                v2*(v4*v5 + v3*(v4 + v5))))*
           ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
             (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
             (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
             (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
          (v1*v2 - v2*v3 + v3*v4 - v1*v5 - v4*v5)*
           (2*Power(-(t*v1f) - (1 - t)*v1i,2)*(-v2f + v2i + v5f - v5i)*
              (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i) + 
             2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
              Power(-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i,2) + 
             2*((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
                (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                (-(t*v4f) - (1 - t)*v4i)*(v3f - v3i - v5f + v5i) + 
                (-v4f + v4i)*(t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i))*
              ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                (-(t*v4f) - (1 - t)*v4i)*
                 (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i)) + 
             2*(-(t*v1f) - (1 - t)*v1i)*
              (-(Power(-(t*v2f) - (1 - t)*v2i,2)*(-v3f + v3i)) - 
                2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
                 (-(t*v3f) - (1 - t)*v3i) + 
                (-(t*v4f) - (1 - t)*v4i)*(-v3f + v3i + v5f - v5i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i)*
                 (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)*
                 (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                (-(t*v2f) - (1 - t)*v2i)*
                 ((-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
                   (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i - v5f + v5i) + 
                   (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                   (-v3f + v3i)*
                    (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i)) + 
                (-v2f + v2i)*((-(t*v4f) - (1 - t)*v4i)*
                    (-(t*v5f) - (1 - t)*v5i) + 
                   (-(t*v3f) - (1 - t)*v3i)*
                    (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i))) + 
             2*(-v1f + v1i)*(-(Power(-(t*v2f) - (1 - t)*v2i,2)*
                   (-(t*v3f) - (1 - t)*v3i)) + 
                (-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i)*
                 (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
                (-(t*v2f) - (1 - t)*v2i)*
                 ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                   (-(t*v3f) - (1 - t)*v3i)*
                    (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i)))))*
        (hvs(-(v3/v5))*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(1 - v3/v5) - 
          (M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*rlog(1 - v5/v2) - 
          M_PI*hvs(-v2)*rlog(Abs(v2)) + M_PI*hvs(-v3)*rlog(Abs(v2)) + 
          M_PI*hvs(-v2)*rlog(Abs(v3)) - M_PI*hvs(-v5)*rlog(Abs(v3)) - 
          M_PI*hvs(-v3)*rlog(Abs(v5)) + M_PI*hvs(-v5)*rlog(Abs(v5))))/
      (4.*v1*v4*(v2 + v3 - v5)*v5*
        sqrt(-(Power(v1,2)*Power(v2 - v5,2)) - 
          Power(v2*v3 + v4*(-v3 + v5),2) - 
          2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
             v2*(v4*v5 + v3*(v4 + v5)))))) + 
  ((-2*(Power(v4,2)*Power(-v3 + v5,2) + Power(v2*(-v1 + v3) + v1*v5,2) + 
          2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - 
             v1*Power(v5,2)))*
        ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (v1*v2 - v2*v3 - v3*v4 - v1*v5 + v4*v5)*
        (2*Power(-(t*v4f) - (1 - t)*v4i,2)*(v3f - v3i - v5f + v5i)*
           (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i) + 
          2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
           Power(t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i,2) + 
          2*((-(t*v2f) - (1 - t)*v2i)*(v1f - v1i - v3f + v3i) + 
             (-v2f + v2i)*(t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
             (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
             (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i))*
           ((-(t*v2f) - (1 - t)*v2i)*
              (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
             (-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i)) + 
          2*(-(t*v4f) - (1 - t)*v4i)*
           ((-(t*v2f) - (1 - t)*v2i)*(-v1f + v1i + v3f - v3i)*
              (-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i)*
              (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i)*
              (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
             ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
              (-v5f + v5i) + ((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
                (-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i - v3f + v3i) + 
                (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                (-v1f + v1i)*
                 (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
              (-(t*v5f) - (1 - t)*v5i) - 
             2*(-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i)*
              (-(t*v5f) - (1 - t)*v5i) - 
             (-v1f + v1i)*Power(-(t*v5f) - (1 - t)*v5i,2)) + 
          2*(-v4f + v4i)*((-(t*v2f) - (1 - t)*v2i)*
              (-(t*v3f) - (1 - t)*v3i)*
              (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
             ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
              (-(t*v5f) - (1 - t)*v5i) - 
             (-(t*v1f) - (1 - t)*v1i)*Power(-(t*v5f) - (1 - t)*v5i,2))))*
     (Power(M_PI,3)*Power(hvs(-v1),3) - 
       (9*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v3))/2. - 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v3))/2. - 
       (3*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v3),2))/4. + 
       (9*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v3),2))/4. + 
       (9*Power(M_PI,3)*Power(hvs(-v3),3))/4. - 
       (9*Power(M_PI,2)*hvs(-v1)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3)))/2. - 
       (9*Power(M_PI,2)*hvs(-v2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3)))/2. + 
       (3*Power(M_PI,2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*hvs(-v3)*
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3)))/2. - 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v3)*hvs(-v4))/2. + 
       (9*Power(M_PI,3)*Power(hvs(-v3),2)*hvs(-v4))/4. - 
       (9*Power(M_PI,2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs(-v4))/2. + 
       (3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v5))/2. + 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v5))/2. + 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v3)*hvs(-v5))/2. - 
       3*Power(M_PI,3)*Power(hvs(-v3),2)*hvs(-v5) - 
       3*Power(M_PI,2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*
        (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs(-v5) + 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v4)*hvs(-v5))/2. - 
       (3*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v5),2))/4. - 
       (9*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v5),2))/4. + 
       (3*Power(M_PI,3)*hvs(-v3)*Power(hvs(-v5),2))/4. - 
       (9*Power(M_PI,3)*hvs(-v4)*Power(hvs(-v5),2))/4. - 
       Power(M_PI,3)*Power(hvs(-v5),3) + 
       (9*Power(M_PI,2)*hvs(-v1)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. + 
       (9*Power(M_PI,2)*hvs(-v2)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. - 
       (3*Power(M_PI,2)*hvs(-v3)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. + 
       (9*Power(M_PI,2)*hvs(-v4)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. + 
       3*Power(M_PI,2)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*hvs(-v5)*
        (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)) + 
       hvs(-(v3/v1))*Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1),
         3) + hvs(-(v1/v3))*Power(M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + 
          M_PI*sign(v3),3) + hvs(-(v5/v3))*
        Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3),3) + 
       (hvs(-(v3/v5))*Power(M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),
           3))/2. - 3*(-((M_PI*hvs(-v3) - M_PI*hvs(-v5))*hvs((-v3 + v5)/v1)*
              Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                 hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3),2))/2. - 
          ((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs((v1 - v3)/v5)*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3),2))/2. + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           ((hvs(v1)*hvs(-v3)*
                Power(2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))),3)*
                hvs(v5))/6. - (hvs(-1 + v5/v3)*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),3))/6. - 
             (hvs((v1 - v3)/v5)*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 3))/6. - (hvs((-v3 + v5)/v1)*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),3))/6. - 
             (hvs(-1 + v1/v3)*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),
                 3))/6.) + hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (Power(-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                2*M_PI*hvs(-v3)*hvs(-(v1*v5)),3)/6. - 
             (hvs(-1 + v5/v3)*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),3))/6. - 
             (hvs((v1 - v3)/v5)*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 3))/6. - (hvs((-v3 + v5)/v1)*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),3))/6. - 
             (hvs(-1 + v1/v3)*Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                   hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),3))/6.))))/
   (4.*v2*v3*v4*(v1 - v3 + v5)*
     sqrt(-(Power(v4,2)*Power(-v3 + v5,2)) - 
       Power(v2*(-v1 + v3) + v1*v5,2) - 
       2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - v1*Power(v5,2)))) \
+ ((-2*(Power(v3,2)*Power(-v2 + v4,2) + Power(v1*(v2 - v5) + v4*v5,2) + 
          2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
             v4*(v1*v2 + (v1 + v2)*v5)))*
        (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (-(v1*v2) - v2*v3 + v3*v4 + v1*v5 - v4*v5)*
        (2*Power(-(t*v3f) - (1 - t)*v3i,2)*(v2f - v2i - v4f + v4i)*
           (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i) + 
          2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
           Power(t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i,2) + 
          2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i + v5f - v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i))*
           ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v1f) - (1 - t)*v1i)*
              (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i)) + 
          2*(-(t*v3f) - (1 - t)*v3i)*
           (-(Power(-(t*v4f) - (1 - t)*v4i,2)*(-v5f + v5i)) + 
             (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
              (v2f - v2i - v5f + v5i) - 
             2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)*
              (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i)*
              (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*
              ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
                (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                 (-v5f + v5i) + 
                (-v1f + v1i - v2f + v2i)*(-(t*v5f) - (1 - t)*v5i)) + 
             (-v4f + v4i)*((-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                 (-(t*v5f) - (1 - t)*v5i))) + 
          2*(-v3f + v3i)*(-(Power(-(t*v4f) - (1 - t)*v4i,2)*
                (-(t*v5f) - (1 - t)*v5i)) + 
             (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
              (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*
              ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                 (-(t*v5f) - (1 - t)*v5i)))))*
     ((9*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v2),2))/4. + 
       (9*Power(M_PI,3)*Power(hvs(-v2),3))/4. + 
       (9*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v3))/4. - 
       (9*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v4))/4. - 
       (9*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v4),2))/4. + 
       (3*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v4),2))/4. - 
       (9*Power(M_PI,3)*hvs(-v3)*Power(hvs(-v4),2))/4. - 
       (5*Power(M_PI,3)*Power(hvs(-v4),3))/4. + 
       (9*Power(M_PI,2)*hvs(-v1)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)))/2. - 
       (3*Power(M_PI,2)*hvs(-v2)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)))/2. + 
       (9*Power(M_PI,2)*hvs(-v3)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)))/2. + 
       (9*Power(M_PI,2)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*hvs(-v4)*
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)))/2. - 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v5))/2. - 
       (3*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v5))/2. - 
       (9*Power(M_PI,3)*hvs(-v2)*hvs(-v3)*hvs(-v5))/2. + 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v4)*hvs(-v5))/2. + 
       3*Power(M_PI,3)*hvs(-v2)*hvs(-v4)*hvs(-v5) + 
       (9*Power(M_PI,3)*hvs(-v3)*hvs(-v4)*hvs(-v5))/2. + 
       3*Power(M_PI,2)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
        (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*hvs(-v5) - 
       3*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v5),2) + 
       (3*Power(M_PI,3)*hvs(-v4)*Power(hvs(-v5),2))/2. + 
       (Power(M_PI,3)*Power(hvs(-v5),3))/2. - 
       (9*Power(M_PI,2)*hvs(-v1)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. + 
       (3*Power(M_PI,2)*hvs(-v2)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. - 
       (9*Power(M_PI,2)*hvs(-v3)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. - 
       (9*Power(M_PI,2)*hvs(-v4)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. - 
       3*Power(M_PI,2)*hvs(-v5)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
        hvs(-1 + v5/v2) + hvs(-(v4/v2))*
        Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + M_PI*sign(v2),3) + 
       hvs(-(v5/v2))*Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2),
         3) + hvs(-(v2/v4))*Power(M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + 
          M_PI*sign(v4),3) + (hvs(-(v2/v5))*
          Power(M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),3))/2. - 
       3*(-((M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs((-v2 + v5)/v4)*
              Power(2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                 hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v4) - M_PI*sign(v2),2))/2. - 
          ((M_PI*hvs(-v2) - M_PI*hvs(-v4))*hvs((-v2 + v4)/v5)*
             Power(2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
               M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v5) - M_PI*sign(v2),2))/2. + 
          hvs(1 + (v2*(v2 - v4 - v5))/(v4*v5))*
           ((hvs(-v2)*hvs(v4)*
                Power(2*M_PI - M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5))),3)*
                hvs(v5))/6. - (hvs((-v2 + v4)/v5)*
                Power(-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                  2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                   hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                  M_PI*sign(v2),3))/6. - 
             (hvs((-v2 + v5)/v4)*
                Power(-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                  2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                   hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                  M_PI*sign(v2),3))/6. - 
             (hvs(-1 + v5/v2)*
                Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                   hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                  M_PI*sign(v4),3))/6. - 
             (hvs(-1 + v4/v2)*
                Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                  2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                   hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - M_PI*sign(v5),
                 3))/6.) + hvs(-1 - (v2*(v2 - v4 - v5))/(v4*v5))*
           (Power(-M_PI - M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5)) + 
                2*M_PI*hvs(-v2)*hvs(-(v4*v5)),3)/6. - 
             (hvs((-v2 + v4)/v5)*
                Power(-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                  2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                   hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - M_PI*sign(v2),
                 3))/6. - (hvs((-v2 + v5)/v4)*
                Power(-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                  2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                   hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                  M_PI*sign(v2),3))/6. - 
             (hvs(-1 + v5/v2)*
                Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                   hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                  M_PI*sign(v4),3))/6. - 
             (hvs(-1 + v4/v2)*Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                  2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                   hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - M_PI*sign(v5),
                 3))/6.))))/
   (4.*v1*v2*v3*(-v2 + v4 + v5)*
     sqrt(-(Power(v3,2)*Power(-v2 + v4,2)) - Power(v1*(v2 - v5) + v4*v5,2) - 
       2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
          v4*(v1*v2 + (v1 + v2)*v5)))) + 
  ((-2*(Power(v1,2)*Power(v2 - v5,2) + Power(v2*v3 + v4*(-v3 + v5),2) + 
          2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
             v2*(v4*v5 + v3*(v4 + v5))))*
        ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (v1*v2 - v2*v3 + v3*v4 - v1*v5 - v4*v5)*
        (2*Power(-(t*v1f) - (1 - t)*v1i,2)*(-v2f + v2i + v5f - v5i)*
           (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i) + 
          2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
           Power(-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i,2) + 
          2*((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v4f) - (1 - t)*v4i)*(v3f - v3i - v5f + v5i) + 
             (-v4f + v4i)*(t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i))*
           ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v4f) - (1 - t)*v4i)*
              (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i)) + 
          2*(-(t*v1f) - (1 - t)*v1i)*
           (-(Power(-(t*v2f) - (1 - t)*v2i,2)*(-v3f + v3i)) - 
             2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
              (-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v4f) - (1 - t)*v4i)*(-v3f + v3i + v5f - v5i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i)*
              (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)*
              (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
             (-(t*v2f) - (1 - t)*v2i)*
              ((-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
                (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i - v5f + v5i) + 
                (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i - t*v5f - 
                   (1 - t)*v5i)) + 
             (-v2f + v2i)*((-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i))) + 
          2*(-v1f + v1i)*(-(Power(-(t*v2f) - (1 - t)*v2i,2)*
                (-(t*v3f) - (1 - t)*v3i)) + 
             (-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i)*
              (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
             (-(t*v2f) - (1 - t)*v2i)*
              ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i)))))*
     ((-9*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v2),2))/4. - 
       (5*Power(M_PI,3)*Power(hvs(-v2),3))/4. + 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v3))/2. - 
       (3*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v3))/4. + 
       (3*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v3),2))/2. + 
       Power(M_PI,3)*Power(hvs(-v3),3) - 
       (9*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v4))/4. + 
       (9*Power(M_PI,3)*hvs(-v2)*hvs(-v3)*hvs(-v4))/2. + 
       (3*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v5))/2. - 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v3)*hvs(-v5))/2. + 
       (9*Power(M_PI,3)*hvs(-v2)*hvs(-v3)*hvs(-v5))/2. - 
       (9*Power(M_PI,3)*Power(hvs(-v3),2)*hvs(-v5))/2. - 
       (9*Power(M_PI,3)*hvs(-v3)*hvs(-v4)*hvs(-v5))/2. + 
       (9*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v5),2))/4. - 
       (15*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v5),2))/4. - 
       (3*Power(M_PI,3)*hvs(-v3)*Power(hvs(-v5),2))/4. + 
       (9*Power(M_PI,3)*hvs(-v4)*Power(hvs(-v5),2))/4. + 
       (5*Power(M_PI,3)*Power(hvs(-v5),3))/2. - 
       (9*Power(M_PI,2)*hvs(-v1)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. - 
       (9*Power(M_PI,2)*hvs(-v2)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. - 
       (9*Power(M_PI,2)*hvs(-v3)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. - 
       (9*Power(M_PI,2)*hvs(-v4)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. + 
       3*Power(M_PI,2)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*hvs(-v5)*
        (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)) + 
       (9*Power(M_PI,2)*hvs(-v1)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. + 
       (9*Power(M_PI,2)*hvs(-v2)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. + 
       (9*Power(M_PI,2)*hvs(-v3)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. + 
       (9*Power(M_PI,2)*hvs(-v4)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. - 
       3*Power(M_PI,2)*hvs(-v5)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
        hvs(-1 + v5/v2) + hvs(-(v5/v2))*
        Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2),3) + 
       hvs(-(v5/v3))*Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3),
         3) + (3*hvs(-(v2/v5))*
          Power(M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),3))/2. + 
       (3*hvs(-(v3/v5))*Power(M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),
           3))/2. - 3*(-(hvs((v3 - v5)/v2)*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
              Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                 hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v2) - M_PI*sign(v5),2))/2. - 
          (hvs((v2 - v5)/v3)*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5))*
             Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
               M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v3) - M_PI*sign(v5),2))/2. + 
          hvs(1 + (v5*(-v2 - v3 + v5))/(v2*v3))*
           ((hvs(v2)*hvs(v3)*hvs(-v5)*
                Power(2*M_PI - M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3))),3))/
              6. - (hvs(-1 + v3/v5)*
                Power(M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - M_PI*sign(v2),
                 3))/6. - (hvs(-1 + v2/v5)*
                Power(M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - M_PI*sign(v3),
                 3))/6. - (hvs((v2 - v5)/v3)*
                Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                  M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5),3))/6. - 
             (hvs((v3 - v5)/v2)*
                Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                  M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5),3))/6.) + 
          hvs(-1 - (v5*(-v2 - v3 + v5))/(v2*v3))*
           (Power(-M_PI + 2*M_PI*hvs(-(v2*v3))*hvs(-v5) - 
                M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3)),3)/6. - 
             (hvs(-1 + v3/v5)*
                Power(M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - M_PI*sign(v2),3)\
)/6. - (hvs(-1 + v2/v5)*Power(M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - M_PI*sign(v3),3)\
)/6. - (hvs((v2 - v5)/v3)*Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*
                   hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                  M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5),3))/6. - 
             (hvs((v3 - v5)/v2)*
                Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                  M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5),3))/6.))))/
   (4.*v1*v4*(v2 + v3 - v5)*v5*
     sqrt(-(Power(v1,2)*Power(v2 - v5,2)) - Power(v2*v3 + v4*(-v3 + v5),2) - 
       2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + v2*(v4*v5 + v3*(v4 + v5)))\
)) + ((-2*(Power(v1*v2 + v3*(-v2 + v4),2) + 
          2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + 
             v1*(v3*v4 + v2*(v3 + v4)))*v5 + Power(v1 - v4,2)*Power(v5,2)\
)*(-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (-(v1*v2) + v2*v3 - v3*v4 + v1*v5 - v4*v5)*
        (2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
             (-(t*v3f) - (1 - t)*v3i)*(v2f - v2i - v4f + v4i) + 
             (-v3f + v3i)*(t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i))*
           ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
             (-(t*v3f) - (1 - t)*v3i)*
              (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i)) + 
          2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*
                (-(t*v2f) - (1 - t)*v2i)) + 
             (-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i)*
              (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i)*
              ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                (-(t*v2f) - (1 - t)*v2i)*
                 (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
           (-v5f + v5i) + 2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*
                (-v2f + v2i)) - 
             2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
              (-(t*v2f) - (1 - t)*v2i) + 
             (-(t*v3f) - (1 - t)*v3i)*(-v2f + v2i + v4f - v4i)*
              (-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i)*
              (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i)*
              (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i)*
              ((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
                (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i - v4f + v4i) + 
                (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                (-v2f + v2i)*
                 (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)) + 
             (-v1f + v1i)*((-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v4f) - (1 - t)*v4i) + 
                (-(t*v2f) - (1 - t)*v2i)*
                 (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
           (-(t*v5f) - (1 - t)*v5i) + 
          2*Power(-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i,2)*
           (-v5f + v5i)*(-(t*v5f) - (1 - t)*v5i) + 
          2*(-v1f + v1i + v4f - v4i)*
           (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
           Power(-(t*v5f) - (1 - t)*v5i,2)))*
     (-(Power(M_PI,3)*Power(hvs(-v1),3))/2. + 
       (3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v2))/4. + 
       (3*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v2),2))/2. + 
       (3*Power(M_PI,3)*Power(hvs(-v2),3))/4. - 
       3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v3) + 
       3*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v3) + 
       (3*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v3),2))/2. - 
       (Power(M_PI,3)*Power(hvs(-v3),3))/4. - 
       (3*Power(M_PI,2)*hvs(-v1)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3)))/2. + 
       (3*Power(M_PI,2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*hvs(-v3)*
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3)))/2. - 
       (3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v4))/4. + 
       3*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v4) - 
       (15*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v4))/4. - 
       3*Power(M_PI,3)*hvs(-v2)*hvs(-v3)*hvs(-v4) - 
       (3*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v4),2))/2. - 
       (9*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v4),2))/4. + 
       (9*Power(M_PI,3)*hvs(-v3)*Power(hvs(-v4),2))/4. + 
       (5*Power(M_PI,3)*Power(hvs(-v4),3))/2. - 
       (9*Power(M_PI,2)*hvs(-v1)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)))/2. - 
       3*Power(M_PI,2)*hvs(-v2)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
        (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)) - 
       (9*Power(M_PI,2)*hvs(-v3)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)))/2. + 
       6*Power(M_PI,2)*hvs(-v1)*(M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*
        hvs(-1 + v4/v1) + (3*Power(M_PI,2)*hvs(-v2)*
          (M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*hvs(-1 + v4/v1))/2. + 
       (3*Power(M_PI,2)*hvs(-v3)*(M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*
          hvs(-1 + v4/v1))/2. - 3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v5) + 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v5))/2. - 
       (3*Power(M_PI,3)*Power(hvs(-v3),2)*hvs(-v5))/4. + 
       (3*Power(M_PI,3)*hvs(-v1)*hvs(-v4)*hvs(-v5))/2. - 
       3*Power(M_PI,3)*hvs(-v2)*hvs(-v4)*hvs(-v5) - 
       (3*Power(M_PI,3)*hvs(-v3)*hvs(-v4)*hvs(-v5))/2. + 
       (3*Power(M_PI,3)*Power(hvs(-v4),2)*hvs(-v5))/2. - 
       3*Power(M_PI,2)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
        (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*hvs(-v5) + 
       6*Power(M_PI,2)*(M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*
        hvs(-1 + v4/v1)*hvs(-v5) - 
       (3*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v5),2))/4. + 
       (3*Power(M_PI,3)*hvs(-v3)*Power(hvs(-v5),2))/2. - 
       (Power(M_PI,3)*Power(hvs(-v5),3))/4. - 
       (3*Power(M_PI,2)*hvs(-v3)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. + 
       (3*Power(M_PI,2)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*hvs(-v5)*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. + 
       (3*Power(M_PI,2)*hvs(-v2)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. - 
       (3*Power(M_PI,2)*hvs(-v5)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. + 
       (hvs(-(v3/v1))*Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + 
            M_PI*sign(v1),3))/2. + 
       (3*hvs(-(v4/v1))*Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v4) + 
            M_PI*sign(v1),3))/2. + 
       (hvs(-(v4/v2))*Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + 
            M_PI*sign(v2),3))/2. + 
       (hvs(-(v5/v2))*Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + 
            M_PI*sign(v2),3))/2. + 
       (hvs(-(v1/v3))*Power(M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3),
           3))/2. + (hvs(-(v5/v3))*
          Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3),3))/2. + 
       (hvs(-(v1/v4))*Power(M_PI*hvs(v1 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4),
           3))/2. + (hvs(-(v2/v4))*
          Power(M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4),3))/2. - 
       (3*(-((M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs((-v1 + v4)/v3)*
                Power(2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                   hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                  M_PI*hvs((v1*(1 - v3/v1 - v4/v1))/v3) - M_PI*sign(v1),2))/
             2. - ((M_PI*hvs(-v1) - M_PI*hvs(-v3))*hvs((-v1 + v3)/v4)*
               Power(2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                  hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                 M_PI*hvs((v1*(1 - v3/v1 - v4/v1))/v4) - M_PI*sign(v1),2))/2. \
+ hvs(1 + (v1*(v1 - v3 - v4))/(v3*v4))*
             ((hvs(-v1)*hvs(v3)*
                  Power(2*M_PI - M_PI*hvs(-((v1*(v1 - v3 - v4))/(v3*v4))),
                   3)*hvs(v4))/6. - 
               (hvs((-v1 + v3)/v4)*
                  Power(-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                     hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs((-v1 + v4)/v3)*
                  Power(-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                     hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs(-1 + v4/v1)*
                  Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                     hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs(-1 + v3/v1)*
                  Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                     hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v4),3))/6.) + 
            hvs(-1 - (v1*(v1 - v3 - v4))/(v3*v4))*
             (Power(-M_PI - M_PI*hvs((v1*(v1 - v3 - v4))/(v3*v4)) + 
                  2*M_PI*hvs(-v1)*hvs(-(v3*v4)),3)/6. - 
               (hvs((-v1 + v3)/v4)*
                  Power(-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                     hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs((-v1 + v4)/v3)*
                  Power(-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                     hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs(-1 + v4/v1)*
                  Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                     hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs(-1 + v3/v1)*
                  Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                     hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v4),3))/6.)))/2. - 
       (9*(-(hvs((v2 - v4)/v1)*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
                Power(2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                   hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                  M_PI*hvs(((1 - v1/v4 - v2/v4)*v4)/v1) - M_PI*sign(v4),2))/
             2. - (hvs((v1 - v4)/v2)*(-(M_PI*hvs(-v1)) + M_PI*hvs(-v4))*
               Power(2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                  hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                 M_PI*hvs(((1 - v1/v4 - v2/v4)*v4)/v2) - M_PI*sign(v4),2))/2. \
+ hvs(1 + (v4*(-v1 - v2 + v4))/(v1*v2))*
             ((hvs(v1)*hvs(v2)*hvs(-v4)*
                  Power(2*M_PI - M_PI*hvs(-((v4*(-v1 - v2 + v4))/(v1*v2))),
                   3))/6. - (hvs(-1 + v2/v4)*
                  Power(M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                     hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs(-1 + v1/v4)*
                  Power(M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                     hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs((v1 - v4)/v2)*
                  Power(2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*
                     hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                     hvs(v4) - M_PI*hvs(-1 + (-v1 + v4)/v2) - 
                    M_PI*sign(v4),3))/6. - 
               (hvs((v2 - v4)/v1)*
                  Power(2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*
                     hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                     hvs(v4) - M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4),
                   3))/6.) + hvs(-1 - (v4*(-v1 - v2 + v4))/(v1*v2))*
             (Power(-M_PI + 2*M_PI*hvs(-(v1*v2))*hvs(-v4) - 
                  M_PI*hvs((v4*(-v1 - v2 + v4))/(v1*v2)),3)/6. - 
               (hvs(-1 + v2/v4)*
                  Power(M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                     hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs(-1 + v1/v4)*
                  Power(M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                     hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs((v1 - v4)/v2)*
                  Power(2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*
                     hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                     hvs(v4) - M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4),
                   3))/6. - (hvs((v2 - v4)/v1)*
                  Power(2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*
                     hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                     hvs(v4) - M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4),
                   3))/6.)))/2. + 
       (hvs(-(v2/v5))*Power(M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),
           3))/2. + (hvs(-(v3/v5))*
          Power(M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),3))/2. + 
       (3*(-((M_PI*hvs(-v3) - M_PI*hvs(-v5))*hvs((-v3 + v5)/v1)*
                Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3),2))/
             2. - ((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs((v1 - v3)/v5)*
               Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                  hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                 M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3),2))/2. \
+ hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
             ((hvs(v1)*hvs(-v3)*
                  Power(2*M_PI - 
                    M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))),3)*hvs(v5))/
                6. - (hvs(-1 + v5/v3)*
                  Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                     hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs((v1 - v3)/v5)*
                  Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                     hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs((-v3 + v5)/v1)*
                  Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                     hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs(-1 + v1/v3)*
                  Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                     hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v5),3))/6.) + 
            hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
             (Power(-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                  2*M_PI*hvs(-v3)*hvs(-(v1*v5)),3)/6. - 
               (hvs(-1 + v5/v3)*
                  Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                     hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs((v1 - v3)/v5)*
                  Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                     hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs((-v3 + v5)/v1)*
                  Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                     hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs(-1 + v1/v3)*
                  Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                     hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v5),3))/6.)))/2. - 
       (3*(-((M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs((-v2 + v5)/v4)*
                Power(2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                   hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                  M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v4) - M_PI*sign(v2),2))/
             2. - ((M_PI*hvs(-v2) - M_PI*hvs(-v4))*hvs((-v2 + v4)/v5)*
               Power(2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                  hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                 M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v5) - M_PI*sign(v2),2))/2. \
+ hvs(1 + (v2*(v2 - v4 - v5))/(v4*v5))*
             ((hvs(-v2)*hvs(v4)*
                  Power(2*M_PI - M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5))),
                   3)*hvs(v5))/6. - 
               (hvs((-v2 + v4)/v5)*
                  Power(-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                     hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs((-v2 + v5)/v4)*
                  Power(-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                     hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs(-1 + v5/v2)*
                  Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                     hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v4),3))/6. - 
               (hvs(-1 + v4/v2)*
                  Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                     hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v5),3))/6.) + 
            hvs(-1 - (v2*(v2 - v4 - v5))/(v4*v5))*
             (Power(-M_PI - M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5)) + 
                  2*M_PI*hvs(-v2)*hvs(-(v4*v5)),3)/6. - 
               (hvs((-v2 + v4)/v5)*
                  Power(-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                     hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs((-v2 + v5)/v4)*
                  Power(-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                     hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs(-1 + v5/v2)*
                  Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                     hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v4),3))/6. - 
               (hvs(-1 + v4/v2)*
                  Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                     hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v5),3))/6.)))/2. + 
       (3*(-(hvs((v3 - v5)/v2)*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
                Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                  M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v2) - M_PI*sign(v5),2))/2. \
- (hvs((v2 - v5)/v3)*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5))*
               Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                  hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                 M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v3) - M_PI*sign(v5),2))/2. \
+ hvs(1 + (v5*(-v2 - v3 + v5))/(v2*v3))*
             ((hvs(v2)*hvs(v3)*hvs(-v5)*
                  Power(2*M_PI - M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3))),
                   3))/6. - (hvs(-1 + v3/v5)*
                  Power(M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs(-1 + v2/v5)*
                  Power(M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs((v2 - v5)/v3)*
                  Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*
                     hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*
                     hvs(v5) - M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5),
                   3))/6. - (hvs((v3 - v5)/v2)*
                  Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*
                     hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*
                     hvs(v5) - M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5),
                   3))/6.) + hvs(-1 - (v5*(-v2 - v3 + v5))/(v2*v3))*
             (Power(-M_PI + 2*M_PI*hvs(-(v2*v3))*hvs(-v5) - 
                  M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3)),3)/6. - 
               (hvs(-1 + v3/v5)*
                  Power(M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs(-1 + v2/v5)*
                  Power(M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs((v2 - v5)/v3)*
                  Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5),3))/6. - 
               (hvs((v3 - v5)/v2)*
                  Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5),3))/6.)))/2.))/
   (4.*v3*(v1 + v2 - v4)*v4*v5*
     sqrt(-Power(v1*v2 + v3*(-v2 + v4),2) - 
       2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + v1*(v3*v4 + v2*(v3 + v4)))*
        v5 - Power(v1 - v4,2)*Power(v5,2))) + 
  ((-2*(Power(v2,2)*Power(-v1 + v3,2) + Power(v3*v4 + (v1 - v4)*v5,2) + 
          2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
             v3*(v1*v5 + v4*(v1 + v5))))*
        (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (-(v1*v2) + v2*v3 - v3*v4 - v1*v5 + v4*v5)*
        (2*Power(-(t*v2f) - (1 - t)*v2i,2)*(v1f - v1i - v3f + v3i)*
           (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
          2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
           Power(t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i,2) + 
          2*((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
              (-v5f + v5i) + (-v1f + v1i + v4f - v4i)*
              (-(t*v5f) - (1 - t)*v5i))*
           ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
              (-(t*v5f) - (1 - t)*v5i)) + 
          2*(-(t*v2f) - (1 - t)*v2i)*
           (-(Power(-(t*v3f) - (1 - t)*v3i,2)*(-v4f + v4i)) - 
             2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
              (-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i)*
              (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*(-v5f + v5i) + 
             (-(t*v1f) - (1 - t)*v1i)*(v1f - v1i - v4f + v4i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-v1f + v1i)*(t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v3f) - (1 - t)*v3i)*
              ((-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*(-v1f + v1i - v5f + v5i) + 
                (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-v4f + v4i)*(-(t*v1f) - (1 - t)*v1i - t*v5f - 
                   (1 - t)*v5i)) + 
             (-v3f + v3i)*((-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i))) + 
          2*(-v2f + v2i)*(-(Power(-(t*v3f) - (1 - t)*v3i,2)*
                (-(t*v4f) - (1 - t)*v4i)) + 
             (-(t*v1f) - (1 - t)*v1i)*
              (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v3f) - (1 - t)*v3i)*
              ((-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i)))))*
     ((5*Power(M_PI,3)*Power(hvs(-v1),3))/2. + 
       (3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v2))/4. - 
       (Power(M_PI,3)*Power(hvs(-v2),3))/4. - 
       3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v3) + 
       (3*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v3))/2. - 
       (3*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v3),2))/4. - 
       (9*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v3),2))/4. + 
       (9*Power(M_PI,2)*hvs(-v2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3)))/2. + 
       3*Power(M_PI,2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*hvs(-v3)*
        (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3)) - 
       (3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v4))/4. - 
       (3*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v4))/2. - 
       (3*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v4))/4. + 
       6*Power(M_PI,3)*hvs(-v1)*hvs(-v3)*hvs(-v4) + 
       3*Power(M_PI,3)*hvs(-v2)*hvs(-v3)*hvs(-v4) - 
       (3*Power(M_PI,3)*Power(hvs(-v3),2)*hvs(-v4))/4. + 
       (9*Power(M_PI,2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs(-v4))/2. - 
       (27*Power(M_PI,3)*hvs(-v1)*Power(hvs(-v4),2))/4. + 
       (3*Power(M_PI,3)*hvs(-v3)*Power(hvs(-v4),2))/2. + 
       (7*Power(M_PI,3)*Power(hvs(-v4),3))/4. - 
       (3*Power(M_PI,2)*hvs(-v2)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)))/2. + 
       (3*Power(M_PI,2)*hvs(-1 + v2/v4)*hvs(-(v2/v4))*hvs(-v4)*
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4)))/2. - 
       (3*Power(M_PI,2)*hvs(-v2)*(M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*
          hvs(-1 + v4/v1))/2. - 
       (3*Power(M_PI,2)*hvs(-v3)*(M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*
          hvs(-1 + v4/v1))/2. - 
       6*Power(M_PI,2)*hvs(-v4)*(M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*
        hvs(-1 + v4/v1) + 3*Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-v5) - 
       (3*Power(M_PI,3)*hvs(-v1)*hvs(-v2)*hvs(-v5))/2. + 
       (3*Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-v5))/2. - 
       (15*Power(M_PI,3)*Power(hvs(-v3),2)*hvs(-v5))/4. + 
       3*Power(M_PI,2)*hvs(-1 + v1/v3)*hvs(-(v1/v3))*
        (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs(-v5) - 
       (9*Power(M_PI,3)*hvs(-v1)*hvs(-v4)*hvs(-v5))/2. + 
       (9*Power(M_PI,3)*hvs(-v3)*hvs(-v4)*hvs(-v5))/2. - 
       6*Power(M_PI,2)*(M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs(-(v4/v1))*
        hvs(-1 + v4/v1)*hvs(-v5) - 
       (3*Power(M_PI,3)*hvs(-v2)*Power(hvs(-v5),2))/4. + 
       (3*Power(M_PI,3)*hvs(-v3)*Power(hvs(-v5),2))/2. - 
       (Power(M_PI,3)*Power(hvs(-v5),3))/4. - 
       (3*Power(M_PI,2)*hvs(-v3)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. + 
       (3*Power(M_PI,2)*hvs(-1 + v3/v5)*hvs(-(v3/v5))*hvs(-v5)*
          (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5)))/2. + 
       (3*Power(M_PI,2)*hvs(-v2)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. - 
       (3*Power(M_PI,2)*hvs(-v5)*(M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs(-(v5/v2))*
          hvs(-1 + v5/v2))/2. + 
       (hvs(-(v3/v1))*Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + 
            M_PI*sign(v1),3))/2. + 
       (hvs(-(v4/v1))*Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v4) + 
            M_PI*sign(v1),3))/2. + 
       (hvs(-(v4/v2))*Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + 
            M_PI*sign(v2),3))/2. + 
       (hvs(-(v5/v2))*Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + 
            M_PI*sign(v2),3))/2. + 
       (hvs(-(v1/v3))*Power(M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3),
           3))/2. + (hvs(-(v5/v3))*
          Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3),3))/2. + 
       (3*hvs(-(v1/v4))*Power(M_PI*hvs(v1 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4),
           3))/2. + (hvs(-(v2/v4))*
          Power(M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4),3))/2. - 
       (9*(-((M_PI*hvs(-v1) - M_PI*hvs(-v4))*hvs((-v1 + v4)/v3)*
                Power(2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                   hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                  M_PI*hvs((v1*(1 - v3/v1 - v4/v1))/v3) - M_PI*sign(v1),2))/
             2. - ((M_PI*hvs(-v1) - M_PI*hvs(-v3))*hvs((-v1 + v3)/v4)*
               Power(2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                  hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                 M_PI*hvs((v1*(1 - v3/v1 - v4/v1))/v4) - M_PI*sign(v1),2))/2. \
+ hvs(1 + (v1*(v1 - v3 - v4))/(v3*v4))*
             ((hvs(-v1)*hvs(v3)*
                  Power(2*M_PI - M_PI*hvs(-((v1*(v1 - v3 - v4))/(v3*v4))),
                   3)*hvs(v4))/6. - 
               (hvs((-v1 + v3)/v4)*
                  Power(-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                     hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs((-v1 + v4)/v3)*
                  Power(-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                     hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs(-1 + v4/v1)*
                  Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                     hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs(-1 + v3/v1)*
                  Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                     hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v4),3))/6.) + 
            hvs(-1 - (v1*(v1 - v3 - v4))/(v3*v4))*
             (Power(-M_PI - M_PI*hvs((v1*(v1 - v3 - v4))/(v3*v4)) + 
                  2*M_PI*hvs(-v1)*hvs(-(v3*v4)),3)/6. - 
               (hvs((-v1 + v3)/v4)*
                  Power(-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                     hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs((-v1 + v4)/v3)*
                  Power(-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                     hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs(-1 + v4/v1)*
                  Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                     hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs(-1 + v3/v1)*
                  Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                    2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                     hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                    M_PI*sign(v4),3))/6.)))/2. - 
       (3*(-(hvs((v2 - v4)/v1)*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
                Power(2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                   hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                  M_PI*hvs(((1 - v1/v4 - v2/v4)*v4)/v1) - M_PI*sign(v4),2))/
             2. - (hvs((v1 - v4)/v2)*(-(M_PI*hvs(-v1)) + M_PI*hvs(-v4))*
               Power(2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                  hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                 M_PI*hvs(((1 - v1/v4 - v2/v4)*v4)/v2) - M_PI*sign(v4),2))/2. \
+ hvs(1 + (v4*(-v1 - v2 + v4))/(v1*v2))*
             ((hvs(v1)*hvs(v2)*hvs(-v4)*
                  Power(2*M_PI - M_PI*hvs(-((v4*(-v1 - v2 + v4))/(v1*v2))),
                   3))/6. - (hvs(-1 + v2/v4)*
                  Power(M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                     hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs(-1 + v1/v4)*
                  Power(M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                     hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs((v1 - v4)/v2)*
                  Power(2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*
                     hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                     hvs(v4) - M_PI*hvs(-1 + (-v1 + v4)/v2) - 
                    M_PI*sign(v4),3))/6. - 
               (hvs((v2 - v4)/v1)*
                  Power(2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*
                     hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                     hvs(v4) - M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4),
                   3))/6.) + hvs(-1 - (v4*(-v1 - v2 + v4))/(v1*v2))*
             (Power(-M_PI + 2*M_PI*hvs(-(v1*v2))*hvs(-v4) - 
                  M_PI*hvs((v4*(-v1 - v2 + v4))/(v1*v2)),3)/6. - 
               (hvs(-1 + v2/v4)*
                  Power(M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                     hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs(-1 + v1/v4)*
                  Power(M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                     hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs((v1 - v4)/v2)*
                  Power(2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*
                     hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                     hvs(v4) - M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4),
                   3))/6. - (hvs((v2 - v4)/v1)*
                  Power(2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*
                     hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                     hvs(v4) - M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4),
                   3))/6.)))/2. + 
       (hvs(-(v2/v5))*Power(M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),
           3))/2. + (hvs(-(v3/v5))*
          Power(M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),3))/2. - 
       (3*(-((M_PI*hvs(-v3) - M_PI*hvs(-v5))*hvs((-v3 + v5)/v1)*
                Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3),2))/
             2. - ((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs((v1 - v3)/v5)*
               Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                  hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                 M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3),2))/2. \
+ hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
             ((hvs(v1)*hvs(-v3)*
                  Power(2*M_PI - 
                    M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))),3)*hvs(v5))/
                6. - (hvs(-1 + v5/v3)*
                  Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                     hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs((v1 - v3)/v5)*
                  Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                     hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs((-v3 + v5)/v1)*
                  Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                     hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs(-1 + v1/v3)*
                  Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                     hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v5),3))/6.) + 
            hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
             (Power(-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                  2*M_PI*hvs(-v3)*hvs(-(v1*v5)),3)/6. - 
               (hvs(-1 + v5/v3)*
                  Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                     hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v1),3))/6. - 
               (hvs((v1 - v3)/v5)*
                  Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                     hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs((-v3 + v5)/v1)*
                  Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                     hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs(-1 + v1/v3)*
                  Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                    2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                     hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                    M_PI*sign(v5),3))/6.)))/2. + 
       (3*(-((M_PI*hvs(-v2) - M_PI*hvs(-v5))*hvs((-v2 + v5)/v4)*
                Power(2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                   hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                  M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v4) - M_PI*sign(v2),2))/
             2. - ((M_PI*hvs(-v2) - M_PI*hvs(-v4))*hvs((-v2 + v4)/v5)*
               Power(2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                  hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                 M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v5) - M_PI*sign(v2),2))/2. \
+ hvs(1 + (v2*(v2 - v4 - v5))/(v4*v5))*
             ((hvs(-v2)*hvs(v4)*
                  Power(2*M_PI - M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5))),
                   3)*hvs(v5))/6. - 
               (hvs((-v2 + v4)/v5)*
                  Power(-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                     hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs((-v2 + v5)/v4)*
                  Power(-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                     hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs(-1 + v5/v2)*
                  Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                     hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v4),3))/6. - 
               (hvs(-1 + v4/v2)*
                  Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                     hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v5),3))/6.) + 
            hvs(-1 - (v2*(v2 - v4 - v5))/(v4*v5))*
             (Power(-M_PI - M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5)) + 
                  2*M_PI*hvs(-v2)*hvs(-(v4*v5)),3)/6. - 
               (hvs((-v2 + v4)/v5)*
                  Power(-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                     hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs((-v2 + v5)/v4)*
                  Power(-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                     hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs(-1 + v5/v2)*
                  Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                     hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v4),3))/6. - 
               (hvs(-1 + v4/v2)*
                  Power(-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                    2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                     hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                    M_PI*sign(v5),3))/6.)))/2. + 
       (3*(-(hvs((v3 - v5)/v2)*(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
                Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                   hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                  M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v2) - M_PI*sign(v5),2))/2. \
- (hvs((v2 - v5)/v3)*(-(M_PI*hvs(-v2)) + M_PI*hvs(-v5))*
               Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                  hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                 M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v3) - M_PI*sign(v5),2))/2. \
+ hvs(1 + (v5*(-v2 - v3 + v5))/(v2*v3))*
             ((hvs(v2)*hvs(v3)*hvs(-v5)*
                  Power(2*M_PI - M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3))),
                   3))/6. - (hvs(-1 + v3/v5)*
                  Power(M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs(-1 + v2/v5)*
                  Power(M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs((v2 - v5)/v3)*
                  Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*
                     hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*
                     hvs(v5) - M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5),
                   3))/6. - (hvs((v3 - v5)/v2)*
                  Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*
                     hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*
                     hvs(v5) - M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5),
                   3))/6.) + hvs(-1 - (v5*(-v2 - v3 + v5))/(v2*v3))*
             (Power(-M_PI + 2*M_PI*hvs(-(v2*v3))*hvs(-v5) - 
                  M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3)),3)/6. - 
               (hvs(-1 + v3/v5)*
                  Power(M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*sign(v2),3))/6. - 
               (hvs(-1 + v2/v5)*
                  Power(M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                    2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*sign(v3),3))/6. - 
               (hvs((v2 - v5)/v3)*
                  Power(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5),3))/6. - 
               (hvs((v3 - v5)/v2)*
                  Power(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                     hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                    M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5),3))/6.)))/2.))/
   (4.*v1*v2*(-v1 + v3 + v4)*v5*
     sqrt(-(Power(v2,2)*Power(-v1 + v3,2)) - Power(v3*v4 + (v1 - v4)*v5,2) - 
       2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
          v3*(v1*v5 + v4*(v1 + v5))))) - 
  ((-2*(Power(v4,2)*Power(-v3 + v5,2) + Power(v2*(-v1 + v3) + v1*v5,2) + 
          2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - 
             v1*Power(v5,2)))*
        ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (v1*v2 - v2*v3 - v3*v4 - v1*v5 + v4*v5)*
        (2*Power(-(t*v4f) - (1 - t)*v4i,2)*(v3f - v3i - v5f + v5i)*
           (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i) + 
          2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
           Power(t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i,2) + 
          2*((-(t*v2f) - (1 - t)*v2i)*(v1f - v1i - v3f + v3i) + 
             (-v2f + v2i)*(t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
             (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
             (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i))*
           ((-(t*v2f) - (1 - t)*v2i)*
              (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
             (-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i)) + 
          2*(-(t*v4f) - (1 - t)*v4i)*
           ((-(t*v2f) - (1 - t)*v2i)*(-v1f + v1i + v3f - v3i)*
              (-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i)*
              (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i)*
              (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
             ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
              (-v5f + v5i) + ((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
                (-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i - v3f + v3i) + 
                (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                (-v1f + v1i)*
                 (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
              (-(t*v5f) - (1 - t)*v5i) - 
             2*(-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i)*
              (-(t*v5f) - (1 - t)*v5i) - 
             (-v1f + v1i)*Power(-(t*v5f) - (1 - t)*v5i,2)) + 
          2*(-v4f + v4i)*((-(t*v2f) - (1 - t)*v2i)*
              (-(t*v3f) - (1 - t)*v3i)*
              (-(t*v1f) - (1 - t)*v1i + t*v3f + (1 - t)*v3i) + 
             ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
                (-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v2f) - (1 - t)*v2i - t*v3f - (1 - t)*v3i))*
              (-(t*v5f) - (1 - t)*v5i) - 
             (-(t*v1f) - (1 - t)*v1i)*Power(-(t*v5f) - (1 - t)*v5i,2))))*
     ((Power(M_PI,3)*hvs(-v1))/4. - (3*Power(M_PI,3)*hvs(-v2))/4. - 
       (5*Power(M_PI,3)*hvs(-v3))/4. - (3*Power(M_PI,3)*hvs(-v4))/4. + 
       3*M_PI*hvs(-v1)*Power(rlog(Abs(v1)),2) - 
       (9*(-(M_PI*hvs(-v1)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
                 hvs(-(v1/v3))*
                  (Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                    rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3))))\
)) + hvs(-(v1/v3))*rlog(Abs(v1))*
             (-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(Abs(1 - v1/v3))) - 
               M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))))/2. - 
       (9*(-(M_PI*hvs(-v2)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
                 hvs(-(v1/v3))*
                  (Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                    rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3))))\
)) + hvs(-(v1/v3))*rlog(Abs(v2))*
             (-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(Abs(1 - v1/v3))) - 
               M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))))/2. - 
       (9*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v3)))/2. + 
       (27*M_PI*hvs(-v3)*Power(rlog(Abs(v3)),2))/4. + 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v3)*rlog(Abs(v2))) - 
            M_PI*hvs(-v2)*rlog(Abs(v3))))/2. + 
       (9*(-(M_PI*hvs(-v3)*Power(rlog(Abs(v1)),2)) - 
            2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v3))))/2. + 
       (3*(-(M_PI*hvs(-v3)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
                 hvs(-(v1/v3))*
                  (Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                    rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3))))\
)) + hvs(-(v1/v3))*(-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
                  rlog(Abs(1 - v1/v3))) - 
               M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))*
             rlog(Abs(v3))))/2. + 
       (3*(-2*M_PI*hvs(-v3)*rlog(Abs(v1))*rlog(Abs(v3)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v3)),2)))/4. - 
       (9*(-2*M_PI*hvs(-v3)*rlog(Abs(v2))*rlog(Abs(v3)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v3)),2)))/4. - 
       (9*M_PI*hvs(-v1)*rlog(Abs(v3))*rlog(Abs(v4)))/2. + 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v4)*rlog(Abs(v3))) - 
            M_PI*hvs(-v3)*rlog(Abs(v4))))/2. - 
       (9*(-(M_PI*hvs(-v4)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
                 hvs(-(v1/v3))*
                  (Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                    rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3))))\
)) + hvs(-(v1/v3))*(-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
                  rlog(Abs(1 - v1/v3))) - 
               M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))*
             rlog(Abs(v4))))/2. - 
       (9*(-(M_PI*hvs(-v4)*Power(rlog(Abs(v3)),2)) - 
            2*M_PI*hvs(-v3)*rlog(Abs(v3))*rlog(Abs(v4))))/4. + 
       (9*(-(M_PI*hvs(-v1)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v1))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. + 
       (9*(-(M_PI*hvs(-v2)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v2))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. - 
       (3*(-(M_PI*hvs(-v3)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v3))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. + 
       (9*(-(M_PI*hvs(-v4)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v4))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. + 
       (9*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v5)))/2. + 
       (9*M_PI*hvs(-v1)*rlog(Abs(v3))*rlog(Abs(v5)))/2. + 
       (9*M_PI*hvs(-v1)*rlog(Abs(v4))*rlog(Abs(v5)))/2. - 
       3*M_PI*hvs(-v5)*Power(rlog(Abs(v5)),2) - 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v2))) - 
            M_PI*hvs(-v2)*rlog(Abs(v5))))/2. - 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v3))) - 
            M_PI*hvs(-v3)*rlog(Abs(v5))))/2. - 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
            M_PI*hvs(-v4)*rlog(Abs(v5))))/2. - 
       (3*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v1)),2)) - 
            2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v5))))/2. - 
       3*(-(M_PI*hvs(-v5)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
               hvs(-(v1/v3))*(Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                  rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3)))))) \
+ hvs(-(v1/v3))*(-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(Abs(1 - v1/v3))) - 
             M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))*
           rlog(Abs(v5))) + 3*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v3)),2)) - 
          2*M_PI*hvs(-v3)*rlog(Abs(v3))*rlog(Abs(v5))) + 
       3*(-(M_PI*hvs(-v5)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
               hvs(-(v3/v5))*(Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                  rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5)))))) \
+ hvs(-(v3/v5))*(-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
             M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))*
           rlog(Abs(v5))) + (3*
          (-2*M_PI*hvs(-v5)*rlog(Abs(v1))*rlog(Abs(v5)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v5)),2)))/4. + 
       (9*(-2*M_PI*hvs(-v5)*rlog(Abs(v2))*rlog(Abs(v5)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v5)),2)))/4. - 
       (3*(-2*M_PI*hvs(-v5)*rlog(Abs(v3))*rlog(Abs(v5)) - 
            M_PI*hvs(-v3)*Power(rlog(Abs(v5)),2)))/4. + 
       (9*(-2*M_PI*hvs(-v5)*rlog(Abs(v4))*rlog(Abs(v5)) - 
            M_PI*hvs(-v4)*Power(rlog(Abs(v5)),2)))/4. + 
       6*hvs(-(v3/v1))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1)))/6. + 
          (Power(rlog(Abs(v1)) - rlog(Abs(v1 - v3)),2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1)))/2.) + 
       6*hvs(-(v1/v3))*((Power(M_PI,2)*
             (M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3)))/6. + 
          (Power(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)),2)*
             (M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3)))/2.) + 
       6*hvs(-(v5/v3))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3)))/6. + 
          (Power(rlog(Abs(v3)) - rlog(Abs(v3 - v5)),2)*
             (-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3)))/2.) + 
       3*hvs(-(v3/v5))*((Power(M_PI,2)*
             (M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/6. + 
          (Power(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)),2)*
             (M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/2.) - 
       3*((M_PI*hvs(-v3) - M_PI*hvs(-v5))*
           (hvs((v3 - v5)/v1)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             hvs((-v3 + v5)/v1)*
              (-Power(M_PI,2)/6. - rLi(2,-(v1/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1)),2)/2.)) + 
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
           (hvs((-v1 + v3)/v5)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
             hvs((v1 - v3)/v5)*
              (-Power(M_PI,2)/6. - rLi(2,-(v5/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5)),2)/2.)) + 
          hvs((-v3 + v5)/v1)*(-rlog(Abs(v3)) + rlog(Abs(v5)))*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*hvs(-v1 + v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3)) + 
          hvs((v1 - v3)/v5)*(rlog(Abs(v1)) - rlog(Abs(v3)))*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3)) + 
          hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v3) - 
                  M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5))))/6. + 
             ((-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                  2*M_PI*hvs(-v3)*hvs(-(v1*v5)))*
                Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),2))/2. - 
             hvs(-1 + v5/v3)*((Power(M_PI,2)*
                   (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                      hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v1)))/6. + 
                (Power(rlog(Abs(v3)) - rlog(Abs(v5)),2)*
                   (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                      hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v1)))/2.) - 
             hvs((v1 - v3)/v5)*
              ((Power(M_PI,2)*(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                      hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v3)))/6. + 
                (Power(rlog(Abs(1 - (-v1 + v3)/v5)),2)*
                   (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                      hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v3)))/2.) - 
             hvs((-v3 + v5)/v1)*
              ((Power(M_PI,2)*(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                      hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v3)))/6. + 
                (Power(rlog(Abs(1 - (v3 - v5)/v1)),2)*
                   (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                      hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v3)))/2.) - 
             hvs(-1 + v1/v3)*((Power(M_PI,2)*
                   (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                      hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v5)))/6. + 
                (Power(-rlog(Abs(v1)) + rlog(Abs(v3)),2)*
                   (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                      hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v5)))/2.)) + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (hvs(v1)*hvs(-v3)*hvs(v5)*
              ((Power(M_PI,2)*(2*M_PI - 
                     M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5)))))/6. + 
                ((2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))))*
                   Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),2))/2.) - 
             hvs(-1 + v5/v3)*((Power(M_PI,2)*
                   (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                      hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v1)))/6. + 
                (Power(rlog(Abs(v3)) - rlog(Abs(v5)),2)*
                   (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                      hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v1)))/2.) - 
             hvs((v1 - v3)/v5)*
              ((Power(M_PI,2)*(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                      hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v3)))/6. + 
                (Power(rlog(Abs(1 - (-v1 + v3)/v5)),2)*
                   (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                      hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v3)))/2.) - 
             hvs((-v3 + v5)/v1)*
              ((Power(M_PI,2)*(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                      hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v3)))/6. + 
                (Power(rlog(Abs(1 - (v3 - v5)/v1)),2)*
                   (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                      hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v3)))/2.) - 
             hvs(-1 + v1/v3)*((Power(M_PI,2)*
                   (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                      hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                     M_PI*sign(v5)))/6. + 
                (Power(-rlog(Abs(v1)) + rlog(Abs(v3)),2)*
                   (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                     2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                      hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5)\
))/2.)))))/(4.*v2*v3*v4*(v1 - v3 + v5)*
     sqrt(-(Power(v4,2)*Power(-v3 + v5,2)) - 
       Power(v2*(-v1 + v3) + v1*v5,2) - 
       2*v4*(v2*(v1 - v3)*v3 + (v2*v3 + v1*(v2 + v3))*v5 - v1*Power(v5,2)))) \
- ((-2*(Power(v3,2)*Power(-v2 + v4,2) + Power(v1*(v2 - v5) + v4*v5,2) + 
          2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
             v4*(v1*v2 + (v1 + v2)*v5)))*
        (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (-(v1*v2) - v2*v3 + v3*v4 + v1*v5 - v4*v5)*
        (2*Power(-(t*v3f) - (1 - t)*v3i,2)*(v2f - v2i - v4f + v4i)*
           (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i) + 
          2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
           Power(t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i,2) + 
          2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i + v5f - v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i))*
           ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v1f) - (1 - t)*v1i)*
              (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i)) + 
          2*(-(t*v3f) - (1 - t)*v3i)*
           (-(Power(-(t*v4f) - (1 - t)*v4i,2)*(-v5f + v5i)) + 
             (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
              (v2f - v2i - v5f + v5i) - 
             2*(-v4f + v4i)*(-(t*v4f) - (1 - t)*v4i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)*
              (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i)*
              (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*
              ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
                (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                 (-v5f + v5i) + 
                (-v1f + v1i - v2f + v2i)*(-(t*v5f) - (1 - t)*v5i)) + 
             (-v4f + v4i)*((-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                 (-(t*v5f) - (1 - t)*v5i))) + 
          2*(-v3f + v3i)*(-(Power(-(t*v4f) - (1 - t)*v4i,2)*
                (-(t*v5f) - (1 - t)*v5i)) + 
             (-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i)*
              (t*v2f + (1 - t)*v2i - t*v5f - (1 - t)*v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*
              ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
                (-(t*v1f) - (1 - t)*v1i - t*v2f - (1 - t)*v2i)*
                 (-(t*v5f) - (1 - t)*v5i)))))*
     ((-3*Power(M_PI,3)*hvs(-v1))/4. - (5*Power(M_PI,3)*hvs(-v2))/4. - 
       (3*Power(M_PI,3)*hvs(-v3))/4. + (Power(M_PI,3)*hvs(-v4))/4. + 
       (27*M_PI*hvs(-v2)*Power(rlog(Abs(v2)),2))/4. - 
       (9*(-2*M_PI*hvs(-v2)*rlog(Abs(v1))*rlog(Abs(v2)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v2)),2)))/4. - 
       (9*(-(M_PI*hvs(-v3)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v3))))/4. + 
       (9*(-(M_PI*hvs(-v1)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
                 hvs(-(v2/v4))*
                  (Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                    rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4))))\
)) + hvs(-(v2/v4))*rlog(Abs(v1))*
             (-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(Abs(1 - v2/v4))) - 
               M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))))/2. - 
       (3*(-(M_PI*hvs(-v2)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
                 hvs(-(v2/v4))*
                  (Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                    rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4))))\
)) + hvs(-(v2/v4))*rlog(Abs(v2))*
             (-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(Abs(1 - v2/v4))) - 
               M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))))/2. + 
       (9*(-(M_PI*hvs(-v3)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
                 hvs(-(v2/v4))*
                  (Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                    rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4))))\
)) + hvs(-(v2/v4))*rlog(Abs(v3))*
             (-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(Abs(1 - v2/v4))) - 
               M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))))/2. - 
       (15*M_PI*hvs(-v4)*Power(rlog(Abs(v4)),2))/4. + 
       (9*(-(M_PI*hvs(-v4)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v4))))/4. + 
       (9*(-(M_PI*hvs(-v4)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
                 hvs(-(v2/v4))*
                  (Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                    rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4))))\
)) + hvs(-(v2/v4))*(-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
                  rlog(Abs(1 - v2/v4))) - 
               M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))*
             rlog(Abs(v4))))/2. + 
       (9*(-2*M_PI*hvs(-v4)*rlog(Abs(v1))*rlog(Abs(v4)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v4)),2)))/4. - 
       (3*(-2*M_PI*hvs(-v4)*rlog(Abs(v2))*rlog(Abs(v4)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v4)),2)))/4. + 
       (9*(-2*M_PI*hvs(-v4)*rlog(Abs(v3))*rlog(Abs(v4)) - 
            M_PI*hvs(-v3)*Power(rlog(Abs(v4)),2)))/4. - 
       (9*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v5)))/2. - 
       (9*M_PI*hvs(-v2)*rlog(Abs(v3))*rlog(Abs(v5)))/2. + 
       (9*M_PI*hvs(-v1)*rlog(Abs(v4))*rlog(Abs(v5)))/2. + 
       3*M_PI*hvs(-v2)*rlog(Abs(v4))*rlog(Abs(v5)) + 
       (9*M_PI*hvs(-v3)*rlog(Abs(v4))*rlog(Abs(v5)))/2. + 
       (3*M_PI*hvs(-v5)*Power(rlog(Abs(v5)),2))/2. + 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v2))) - 
            M_PI*hvs(-v2)*rlog(Abs(v5))))/2. + 
       (9*rlog(Abs(v2))*(-(M_PI*hvs(-v5)*rlog(Abs(v3))) - 
            M_PI*hvs(-v3)*rlog(Abs(v5))))/2. - 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
            M_PI*hvs(-v4)*rlog(Abs(v5))))/2. - 
       3*rlog(Abs(v2))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
          M_PI*hvs(-v4)*rlog(Abs(v5))) - 
       (9*rlog(Abs(v3))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
            M_PI*hvs(-v4)*rlog(Abs(v5))))/2. + 
       (3*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v5))))/2. + 
       3*(-(M_PI*hvs(-v5)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
               hvs(-(v2/v4))*(Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                  rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4)))))) \
+ hvs(-(v2/v4))*(-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(Abs(1 - v2/v4))) - 
             M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))*
           rlog(Abs(v5))) + 3*(-2*M_PI*hvs(-v5)*rlog(Abs(v2))*
           rlog(Abs(v5)) - M_PI*hvs(-v2)*Power(rlog(Abs(v5)),2)) - 
       (3*(-2*M_PI*hvs(-v5)*rlog(Abs(v4))*rlog(Abs(v5)) - 
            M_PI*hvs(-v4)*Power(rlog(Abs(v5)),2)))/2. - 
       (9*(hvs(-(v5/v2))*rlog(Abs(v1))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v1)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. + (3*(hvs(-(v5/v2))*rlog(Abs(v2))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v2)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. - (9*(hvs(-(v5/v2))*rlog(Abs(v3))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v3)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. - (9*(hvs(-(v5/v2))*rlog(Abs(v4))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v4)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. - 3*(hvs(-(v5/v2))*rlog(Abs(v5))*
           (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
             (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
          M_PI*hvs(-v5)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
             hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2))))) + 
       6*hvs(-(v4/v2))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + M_PI*sign(v2)))/6. + 
          (Power(rlog(Abs(v2)) - rlog(Abs(v2 - v4)),2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + M_PI*sign(v2)))/2.) + 
       6*hvs(-(v5/v2))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2)))/6. + 
          (Power(rlog(Abs(v2)) - rlog(Abs(v2 - v5)),2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2)))/2.) + 
       6*hvs(-(v2/v4))*((Power(M_PI,2)*
             (M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/6. + 
          (Power(rlog(Abs(v4)) - rlog(Abs(-v2 + v4)),2)*
             (M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/2.) + 
       3*hvs(-(v2/v5))*((Power(M_PI,2)*
             (M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/6. + 
          (Power(rlog(Abs(v5)) - rlog(Abs(-v2 + v5)),2)*
             (M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/2.) - 
       3*((M_PI*hvs(-v2) - M_PI*hvs(-v5))*
           (hvs((v2 - v5)/v4)*rLi(2,-((v2*(1 - v4/v2 - v5/v2))/v4)) + 
             hvs((-v2 + v5)/v4)*
              (-Power(M_PI,2)/6. - rLi(2,-(v4/(v2*(1 - v4/v2 - v5/v2)))) - 
                Power(rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v4)),2)/2.)) + 
          (M_PI*hvs(-v2) - M_PI*hvs(-v4))*
           (hvs((v2 - v4)/v5)*rLi(2,-((v2*(1 - v4/v2 - v5/v2))/v5)) + 
             hvs((-v2 + v4)/v5)*
              (-Power(M_PI,2)/6. - rLi(2,-(v5/(v2*(1 - v4/v2 - v5/v2)))) - 
                Power(rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v5)),2)/2.)) + 
          hvs((-v2 + v5)/v4)*(-rlog(Abs(v2)) + rlog(Abs(v5)))*
           rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v4))*
           (2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*hvs(-v2 + v5)*
              hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
             M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v4) - M_PI*sign(v2)) + 
          hvs((-v2 + v4)/v5)*(-rlog(Abs(v2)) + rlog(Abs(v4)))*
           rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v5))*
           (2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*hvs(v5)*
              hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
             M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v5) - M_PI*sign(v2)) + 
          hvs(-1 - (v2*(v2 - v4 - v5))/(v4*v5))*
           ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v2) - 
                  M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5))))/6. + 
             ((-M_PI - M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5)) + 
                  2*M_PI*hvs(-v2)*hvs(-(v4*v5)))*
                Power(rlog(Abs((v2*(v2 - v4 - v5))/(v4*v5))),2))/2. - 
             hvs((-v2 + v4)/v5)*
              ((Power(M_PI,2)*(-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                      hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v2)))/6. + 
                (Power(rlog(Abs(1 - (v2 - v4)/v5)),2)*
                   (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                      hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v2)))/2.) - 
             hvs((-v2 + v5)/v4)*
              ((Power(M_PI,2)*(-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                      hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v2)))/6. + 
                (Power(rlog(Abs(1 - (v2 - v5)/v4)),2)*
                   (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                      hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v2)))/2.) - 
             hvs(-1 + v5/v2)*((Power(M_PI,2)*
                   (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                      hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v4)))/6. + 
                (Power(rlog(Abs(v2)) - rlog(Abs(v5)),2)*
                   (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                      hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v4)))/2.) - 
             hvs(-1 + v4/v2)*((Power(M_PI,2)*
                   (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                      hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v5)))/6. + 
                (Power(rlog(Abs(v2)) - rlog(Abs(v4)),2)*
                   (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                      hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v5)))/2.)) + 
          hvs(1 + (v2*(v2 - v4 - v5))/(v4*v5))*
           (hvs(-v2)*hvs(v4)*hvs(v5)*
              ((Power(M_PI,2)*(2*M_PI - 
                     M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5)))))/6. + 
                ((2*M_PI - M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5))))*
                   Power(rlog(Abs((v2*(v2 - v4 - v5))/(v4*v5))),2))/2.) - 
             hvs((-v2 + v4)/v5)*
              ((Power(M_PI,2)*(-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                      hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v2)))/6. + 
                (Power(rlog(Abs(1 - (v2 - v4)/v5)),2)*
                   (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                      hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v2)))/2.) - 
             hvs((-v2 + v5)/v4)*
              ((Power(M_PI,2)*(-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                      hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v2)))/6. + 
                (Power(rlog(Abs(1 - (v2 - v5)/v4)),2)*
                   (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                      hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v2)))/2.) - 
             hvs(-1 + v5/v2)*((Power(M_PI,2)*
                   (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                      hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v4)))/6. + 
                (Power(rlog(Abs(v2)) - rlog(Abs(v5)),2)*
                   (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                      hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v4)))/2.) - 
             hvs(-1 + v4/v2)*((Power(M_PI,2)*
                   (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                      hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v5)))/6. + 
                (Power(rlog(Abs(v2)) - rlog(Abs(v4)),2)*
                   (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                     2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                      hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                     M_PI*sign(v5)))/2.)))))/
   (4.*v1*v2*v3*(-v2 + v4 + v5)*
     sqrt(-(Power(v3,2)*Power(-v2 + v4,2)) - Power(v1*(v2 - v5) + v4*v5,2) - 
       2*v3*(-(Power(v4,2)*v5) + v1*v2*(-v2 + v5) + 
          v4*(v1*v2 + (v1 + v2)*v5)))) - 
  ((-2*(Power(v1,2)*Power(v2 - v5,2) + Power(v2*v3 + v4*(-v3 + v5),2) + 
          2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + 
             v2*(v4*v5 + v3*(v4 + v5))))*
        ((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) - 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) - 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (v1*v2 - v2*v3 + v3*v4 - v1*v5 - v4*v5)*
        (2*Power(-(t*v1f) - (1 - t)*v1i,2)*(-v2f + v2i + v5f - v5i)*
           (-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i) + 
          2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
           Power(-(t*v2f) - (1 - t)*v2i + t*v5f + (1 - t)*v5i,2) + 
          2*((-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
             (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v4f) - (1 - t)*v4i)*(v3f - v3i - v5f + v5i) + 
             (-v4f + v4i)*(t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i))*
           ((-(t*v2f) - (1 - t)*v2i)*(-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v4f) - (1 - t)*v4i)*
              (t*v3f + (1 - t)*v3i - t*v5f - (1 - t)*v5i)) + 
          2*(-(t*v1f) - (1 - t)*v1i)*
           (-(Power(-(t*v2f) - (1 - t)*v2i,2)*(-v3f + v3i)) - 
             2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
              (-(t*v3f) - (1 - t)*v3i) + 
             (-(t*v4f) - (1 - t)*v4i)*(-v3f + v3i + v5f - v5i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i)*
              (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
             (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)*
              (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
             (-(t*v2f) - (1 - t)*v2i)*
              ((-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
                (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i - v5f + v5i) + 
                (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i - t*v5f - 
                   (1 - t)*v5i)) + 
             (-v2f + v2i)*((-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i))) + 
          2*(-v1f + v1i)*(-(Power(-(t*v2f) - (1 - t)*v2i,2)*
                (-(t*v3f) - (1 - t)*v3i)) + 
             (-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i)*
              (-(t*v3f) - (1 - t)*v3i + t*v5f + (1 - t)*v5i) + 
             (-(t*v2f) - (1 - t)*v2i)*
              ((-(t*v4f) - (1 - t)*v4i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v4f) - (1 - t)*v4i - t*v5f - (1 - t)*v5i)))))*
     ((-3*Power(M_PI,3)*hvs(-v1))/4. + (Power(M_PI,3)*hvs(-v2))/4. + 
       (Power(M_PI,3)*hvs(-v3))/4. - (3*Power(M_PI,3)*hvs(-v4))/4. - 
       (3*Power(M_PI,3)*hvs(-v5))/2. - 
       (15*M_PI*hvs(-v2)*Power(rlog(Abs(v2)),2))/4. + 
       (9*(-2*M_PI*hvs(-v2)*rlog(Abs(v1))*rlog(Abs(v2)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v2)),2)))/4. + 
       (9*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v3)))/2. + 
       3*M_PI*hvs(-v3)*Power(rlog(Abs(v3)),2) - 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v3)*rlog(Abs(v2))) - 
            M_PI*hvs(-v2)*rlog(Abs(v3))))/2. + 
       (3*(-(M_PI*hvs(-v3)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v3))))/4. - 
       (3*(-2*M_PI*hvs(-v3)*rlog(Abs(v2))*rlog(Abs(v3)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v3)),2)))/2. + 
       (9*M_PI*hvs(-v2)*rlog(Abs(v3))*rlog(Abs(v4)))/2. - 
       (9*rlog(Abs(v2))*(-(M_PI*hvs(-v4)*rlog(Abs(v3))) - 
            M_PI*hvs(-v3)*rlog(Abs(v4))))/2. + 
       (9*(-(M_PI*hvs(-v4)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v4))))/4. - 
       (9*(-(M_PI*hvs(-v1)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v1))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. - 
       (9*(-(M_PI*hvs(-v2)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v2))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. - 
       (9*(-(M_PI*hvs(-v3)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v3))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. - 
       (9*(-(M_PI*hvs(-v4)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v4))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. - 
       (9*M_PI*hvs(-v1)*rlog(Abs(v3))*rlog(Abs(v5)))/2. + 
       (9*M_PI*hvs(-v2)*rlog(Abs(v3))*rlog(Abs(v5)))/2. - 
       (9*M_PI*hvs(-v3)*rlog(Abs(v4))*rlog(Abs(v5)))/2. + 
       (15*M_PI*hvs(-v5)*Power(rlog(Abs(v5)),2))/2. + 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v3))) - 
            M_PI*hvs(-v3)*rlog(Abs(v5))))/2. - 
       (9*rlog(Abs(v2))*(-(M_PI*hvs(-v5)*rlog(Abs(v3))) - 
            M_PI*hvs(-v3)*rlog(Abs(v5))))/2. + 
       (9*rlog(Abs(v3))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
            M_PI*hvs(-v4)*rlog(Abs(v5))))/2. - 
       (3*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v5))))/2. + 
       (9*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v3)),2)) - 
            2*M_PI*hvs(-v3)*rlog(Abs(v3))*rlog(Abs(v5))))/2. + 
       3*(-(M_PI*hvs(-v5)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
               hvs(-(v3/v5))*(Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                  rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5)))))) \
+ hvs(-(v3/v5))*(-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
             M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))*
           rlog(Abs(v5))) - (9*
          (-2*M_PI*hvs(-v5)*rlog(Abs(v1))*rlog(Abs(v5)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v5)),2)))/4. + 
       (15*(-2*M_PI*hvs(-v5)*rlog(Abs(v2))*rlog(Abs(v5)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v5)),2)))/4. + 
       (3*(-2*M_PI*hvs(-v5)*rlog(Abs(v3))*rlog(Abs(v5)) - 
            M_PI*hvs(-v3)*Power(rlog(Abs(v5)),2)))/4. - 
       (9*(-2*M_PI*hvs(-v5)*rlog(Abs(v4))*rlog(Abs(v5)) - 
            M_PI*hvs(-v4)*Power(rlog(Abs(v5)),2)))/4. + 
       (9*(hvs(-(v5/v2))*rlog(Abs(v1))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v1)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. + (9*(hvs(-(v5/v2))*rlog(Abs(v2))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v2)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. + (9*(hvs(-(v5/v2))*rlog(Abs(v3))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v3)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. + (9*(hvs(-(v5/v2))*rlog(Abs(v4))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v4)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. - 3*(hvs(-(v5/v2))*rlog(Abs(v5))*
           (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
             (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
          M_PI*hvs(-v5)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
             hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2))))) + 
       6*hvs(-(v5/v2))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2)))/6. + 
          (Power(rlog(Abs(v2)) - rlog(Abs(v2 - v5)),2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2)))/2.) + 
       6*hvs(-(v5/v3))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3)))/6. + 
          (Power(rlog(Abs(v3)) - rlog(Abs(v3 - v5)),2)*
             (-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3)))/2.) + 
       9*hvs(-(v2/v5))*((Power(M_PI,2)*
             (M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/6. + 
          (Power(rlog(Abs(v5)) - rlog(Abs(-v2 + v5)),2)*
             (M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/2.) + 
       9*hvs(-(v3/v5))*((Power(M_PI,2)*
             (M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/6. + 
          (Power(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)),2)*
             (M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/2.) - 
       3*((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
           (hvs((-v3 + v5)/v2)*rLi(2,-(((1 - v2/v5 - v3/v5)*v5)/v2)) + 
             hvs((v3 - v5)/v2)*
              (-Power(M_PI,2)/6. - rLi(2,-(v2/((1 - v2/v5 - v3/v5)*v5))) - 
                Power(rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v2)),2)/2.)) + 
          (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5))*
           (hvs((-v2 + v5)/v3)*rLi(2,-(((1 - v2/v5 - v3/v5)*v5)/v3)) + 
             hvs((v2 - v5)/v3)*
              (-Power(M_PI,2)/6. - rLi(2,-(v3/((1 - v2/v5 - v3/v5)*v5))) - 
                Power(rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v3)),2)/2.)) + 
          hvs((v3 - v5)/v2)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
           rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v2))*
           (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*hvs(v3 - v5)*
              hvs(v2 + v3 - v5)*hvs(v5) - 
             M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v2) - M_PI*sign(v5)) + 
          hvs((v2 - v5)/v3)*(rlog(Abs(v2)) - rlog(Abs(v5)))*
           rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v3))*
           (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*hvs(v3 - v5)*
              hvs(v2 + v3 - v5)*hvs(v5) - 
             M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v3) - M_PI*sign(v5)) + 
          hvs(-1 - (v5*(-v2 - v3 + v5))/(v2*v3))*
           ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v5) - 
                  M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3))))/6. + 
             ((-M_PI + 2*M_PI*hvs(-(v2*v3))*hvs(-v5) - 
                  M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3)))*
                Power(rlog(Abs((v5*(-v2 - v3 + v5))/(v2*v3))),2))/2. - 
             hvs(-1 + v3/v5)*((Power(M_PI,2)*
                   (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*sign(v2)))/6. + 
                (Power(-rlog(Abs(v3)) + rlog(Abs(v5)),2)*
                   (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*sign(v2)))/2.) - 
             hvs(-1 + v2/v5)*((Power(M_PI,2)*
                   (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*sign(v3)))/6. + 
                (Power(-rlog(Abs(v2)) + rlog(Abs(v5)),2)*
                   (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*sign(v3)))/2.) - 
             hvs((v2 - v5)/v3)*
              ((Power(M_PI,2)*(2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*
                      hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*
                      hvs(v5) - M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)\
))/6. + (Power(rlog(Abs(1 - (-v2 + v5)/v3)),2)*
                   (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/2.) - 
             hvs((v3 - v5)/v2)*
              ((Power(M_PI,2)*(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*
                      hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*
                      hvs(v5) - M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5))\
)/6. + (Power(rlog(Abs(1 - (-v3 + v5)/v2)),2)*
                   (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/2.)) + 
          hvs(1 + (v5*(-v2 - v3 + v5))/(v2*v3))*
           (hvs(v2)*hvs(v3)*hvs(-v5)*
              ((Power(M_PI,2)*(2*M_PI - 
                     M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3)))))/6. + 
                ((2*M_PI - M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3))))*
                   Power(rlog(Abs((v5*(-v2 - v3 + v5))/(v2*v3))),2))/2.) - 
             hvs(-1 + v3/v5)*((Power(M_PI,2)*
                   (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*sign(v2)))/6. + 
                (Power(-rlog(Abs(v3)) + rlog(Abs(v5)),2)*
                   (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - M_PI*sign(v2)\
))/2.) - hvs(-1 + v2/v5)*((Power(M_PI,2)*
                   (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*sign(v3)))/6. + 
                (Power(-rlog(Abs(v2)) + rlog(Abs(v5)),2)*
                   (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                     2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - M_PI*sign(v3)\
))/2.) - hvs((v2 - v5)/v3)*((Power(M_PI,2)*
                   (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/6. + 
                (Power(rlog(Abs(1 - (-v2 + v5)/v3)),2)*
                   (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/2.) - 
             hvs((v3 - v5)/v2)*
              ((Power(M_PI,2)*(2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*
                      hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) \
- M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/6. + 
                (Power(rlog(Abs(1 - (-v3 + v5)/v2)),2)*
                   (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                      hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                     M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/2.)))))/
   (4.*v1*v4*(v2 + v3 - v5)*v5*
     sqrt(-(Power(v1,2)*Power(v2 - v5,2)) - Power(v2*v3 + v4*(-v3 + v5),2) - 
       2*v1*(-(Power(v2,2)*v3) + v4*(v3 - v5)*v5 + v2*(v4*v5 + v3*(v4 + v5)))\
)) - ((-2*(Power(v1*v2 + v3*(-v2 + v4),2) + 
          2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + 
             v1*(v3*v4 + v2*(v3 + v4)))*v5 + Power(v1 - v4,2)*Power(v5,2)\
)*(-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) - 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) + 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) - 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (-(v1*v2) + v2*v3 - v3*v4 + v1*v5 - v4*v5)*
        (2*((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i) + 
             (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
             (-(t*v3f) - (1 - t)*v3i)*(v2f - v2i - v4f + v4i) + 
             (-v3f + v3i)*(t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i))*
           ((-(t*v1f) - (1 - t)*v1i)*(-(t*v2f) - (1 - t)*v2i) + 
             (-(t*v3f) - (1 - t)*v3i)*
              (t*v2f + (1 - t)*v2i - t*v4f - (1 - t)*v4i)) + 
          2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*
                (-(t*v2f) - (1 - t)*v2i)) + 
             (-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i)*
              (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i)*
              ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                (-(t*v2f) - (1 - t)*v2i)*
                 (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
           (-v5f + v5i) + 2*(-(Power(-(t*v1f) - (1 - t)*v1i,2)*
                (-v2f + v2i)) - 
             2*(-v1f + v1i)*(-(t*v1f) - (1 - t)*v1i)*
              (-(t*v2f) - (1 - t)*v2i) + 
             (-(t*v3f) - (1 - t)*v3i)*(-v2f + v2i + v4f - v4i)*
              (-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i)*
              (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i)*
              (-(t*v2f) - (1 - t)*v2i + t*v4f + (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i)*
              ((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
                (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i - v4f + v4i) + 
                (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
                (-v2f + v2i)*
                 (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)) + 
             (-v1f + v1i)*((-(t*v3f) - (1 - t)*v3i)*
                 (-(t*v4f) - (1 - t)*v4i) + 
                (-(t*v2f) - (1 - t)*v2i)*
                 (-(t*v3f) - (1 - t)*v3i - t*v4f - (1 - t)*v4i)))*
           (-(t*v5f) - (1 - t)*v5i) + 
          2*Power(-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i,2)*
           (-v5f + v5i)*(-(t*v5f) - (1 - t)*v5i) + 
          2*(-v1f + v1i + v4f - v4i)*
           (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
           Power(-(t*v5f) - (1 - t)*v5i,2)))*
     ((Power(M_PI,3)*hvs(-v1))/4. - (Power(M_PI,3)*hvs(-v2))/4. - 
       (Power(M_PI,3)*hvs(-v3))/4. - (7*Power(M_PI,3)*hvs(-v4))/4. - 
       (Power(M_PI,3)*hvs(-v5))/2. - 
       (3*M_PI*hvs(-v1)*Power(rlog(Abs(v1)),2))/2. + 
       (9*M_PI*hvs(-v2)*Power(rlog(Abs(v2)),2))/4. - 
       (3*(-(M_PI*hvs(-v2)*Power(rlog(Abs(v1)),2)) - 
            2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v2))))/4. - 
       (3*(-2*M_PI*hvs(-v2)*rlog(Abs(v1))*rlog(Abs(v2)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v2)),2)))/2. - 
       (3*(-(M_PI*hvs(-v1)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
                 hvs(-(v1/v3))*
                  (Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                    rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3))))\
)) + hvs(-(v1/v3))*rlog(Abs(v1))*
             (-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(Abs(1 - v1/v3))) - 
               M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))))/2. + 
       3*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v3)) - 
       (3*M_PI*hvs(-v3)*Power(rlog(Abs(v3)),2))/4. - 
       3*rlog(Abs(v1))*(-(M_PI*hvs(-v3)*rlog(Abs(v2))) - 
          M_PI*hvs(-v2)*rlog(Abs(v3))) + 
       3*(-(M_PI*hvs(-v3)*Power(rlog(Abs(v1)),2)) - 
          2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v3))) + 
       (3*(-(M_PI*hvs(-v3)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
                 hvs(-(v1/v3))*
                  (Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                    rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3))))\
)) + hvs(-(v1/v3))*(-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
                  rlog(Abs(1 - v1/v3))) - 
               M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))*
             rlog(Abs(v3))))/2. - 
       (3*(-2*M_PI*hvs(-v3)*rlog(Abs(v1))*rlog(Abs(v3)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v3)),2)))/2. - 
       (9*(-(M_PI*hvs(-v1)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
                 hvs(-(v2/v4))*
                  (Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                    rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4))))\
)) + hvs(-(v2/v4))*rlog(Abs(v1))*
             (-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(Abs(1 - v2/v4))) - 
               M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))))/2. - 
       3*(-(M_PI*hvs(-v2)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
               hvs(-(v2/v4))*(Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                  rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4)))))) \
+ hvs(-(v2/v4))*rlog(Abs(v2))*(-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
                rlog(Abs(1 - v2/v4))) - 
             M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))) - 
       (9*(-(M_PI*hvs(-v3)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
                 hvs(-(v2/v4))*
                  (Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                    rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4))))\
)) + hvs(-(v2/v4))*rlog(Abs(v3))*
             (-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(Abs(1 - v2/v4))) - 
               M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))))/2. + 
       3*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v4)) - 
       3*M_PI*hvs(-v2)*rlog(Abs(v3))*rlog(Abs(v4)) + 
       (15*M_PI*hvs(-v4)*Power(rlog(Abs(v4)),2))/2. - 
       3*rlog(Abs(v1))*(-(M_PI*hvs(-v4)*rlog(Abs(v2))) - 
          M_PI*hvs(-v2)*rlog(Abs(v4))) + 
       3*rlog(Abs(v2))*(-(M_PI*hvs(-v4)*rlog(Abs(v3))) - 
          M_PI*hvs(-v3)*rlog(Abs(v4))) + 
       (3*(-(M_PI*hvs(-v4)*Power(rlog(Abs(v1)),2)) - 
            2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v4))))/4. + 
       (15*(-(M_PI*hvs(-v4)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v4))))/4. + 
       (3*(-2*M_PI*hvs(-v4)*rlog(Abs(v1))*rlog(Abs(v4)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v4)),2)))/2. + 
       (9*(-2*M_PI*hvs(-v4)*rlog(Abs(v2))*rlog(Abs(v4)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v4)),2)))/4. - 
       (9*(-2*M_PI*hvs(-v4)*rlog(Abs(v3))*rlog(Abs(v4)) - 
            M_PI*hvs(-v3)*Power(rlog(Abs(v4)),2)))/4. + 
       6*(hvs(-(v4/v1))*rlog(Abs(v1))*
           (-(M_PI*hvs(-1 + v4/v1)*(-rlog(Abs(v1)) + rlog(Abs(v4)))) - 
             (M_PI*hvs(-v1) - M_PI*hvs(-v4))*rlog(Abs(1 - v4/v1))) - 
          M_PI*hvs(-v1)*(hvs(v4/v1)*rLi(2,1 - v4/v1) + 
             hvs(-(v4/v1))*(Power(M_PI,2)/6. - rLi(2,v4/v1) - 
                (-rlog(Abs(v1)) + rlog(Abs(v4)))*rlog(Abs(1 - v4/v1))))) + 
       (3*(hvs(-(v4/v1))*rlog(Abs(v2))*
             (-(M_PI*hvs(-1 + v4/v1)*(-rlog(Abs(v1)) + rlog(Abs(v4)))) - 
               (M_PI*hvs(-v1) - M_PI*hvs(-v4))*rlog(Abs(1 - v4/v1))) - 
            M_PI*hvs(-v2)*(hvs(v4/v1)*rLi(2,1 - v4/v1) + 
               hvs(-(v4/v1))*(Power(M_PI,2)/6. - rLi(2,v4/v1) - 
                  (-rlog(Abs(v1)) + rlog(Abs(v4)))*rlog(Abs(1 - v4/v1)))))\
)/2. + (3*(hvs(-(v4/v1))*rlog(Abs(v3))*
             (-(M_PI*hvs(-1 + v4/v1)*(-rlog(Abs(v1)) + rlog(Abs(v4)))) - 
               (M_PI*hvs(-v1) - M_PI*hvs(-v4))*rlog(Abs(1 - v4/v1))) - 
            M_PI*hvs(-v3)*(hvs(v4/v1)*rLi(2,1 - v4/v1) + 
               hvs(-(v4/v1))*(Power(M_PI,2)/6. - rLi(2,v4/v1) - 
                  (-rlog(Abs(v1)) + rlog(Abs(v4)))*rlog(Abs(1 - v4/v1)))))\
)/2. - (3*(-(M_PI*hvs(-v3)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*rlog(Abs(v3))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. + 
       (9*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v5)))/2. + 
       (3*M_PI*hvs(-v1)*rlog(Abs(v4))*rlog(Abs(v5)))/2. - 
       3*M_PI*hvs(-v2)*rlog(Abs(v4))*rlog(Abs(v5)) - 
       (3*M_PI*hvs(-v3)*rlog(Abs(v4))*rlog(Abs(v5)))/2. - 
       (3*M_PI*hvs(-v5)*Power(rlog(Abs(v5)),2))/4. - 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v2))) - 
            M_PI*hvs(-v2)*rlog(Abs(v5))))/2. - 
       (3*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
            M_PI*hvs(-v4)*rlog(Abs(v5))))/2. + 
       3*rlog(Abs(v2))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
          M_PI*hvs(-v4)*rlog(Abs(v5))) + 
       (3*rlog(Abs(v3))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
            M_PI*hvs(-v4)*rlog(Abs(v5))))/2. + 
       3*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v1)),2)) - 
          2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v5))) + 
       (3*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v3)),2)) - 
            2*M_PI*hvs(-v3)*rlog(Abs(v3))*rlog(Abs(v5))))/4. - 
       3*(-(M_PI*hvs(-v5)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
               hvs(-(v2/v4))*(Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                  rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4)))))) \
+ hvs(-(v2/v4))*(-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(Abs(1 - v2/v4))) - 
             M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))*
           rlog(Abs(v5))) - (3*
          (-(M_PI*hvs(-v5)*Power(rlog(Abs(v4)),2)) - 
            2*M_PI*hvs(-v4)*rlog(Abs(v4))*rlog(Abs(v5))))/2. + 
       6*(-(M_PI*hvs(-v5)*(hvs(v4/v1)*rLi(2,1 - v4/v1) + 
               hvs(-(v4/v1))*(Power(M_PI,2)/6. - rLi(2,v4/v1) - 
                  (-rlog(Abs(v1)) + rlog(Abs(v4)))*rlog(Abs(1 - v4/v1))))) \
+ hvs(-(v4/v1))*(-(M_PI*hvs(-1 + v4/v1)*(-rlog(Abs(v1)) + rlog(Abs(v4)))) - 
             (M_PI*hvs(-v1) - M_PI*hvs(-v4))*rlog(Abs(1 - v4/v1)))*
           rlog(Abs(v5))) + (3*
          (-(M_PI*hvs(-v5)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5))))\
)) + hvs(-(v3/v5))*(-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
                  rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))*
             rlog(Abs(v5))))/2. + 
       (3*(-2*M_PI*hvs(-v5)*rlog(Abs(v2))*rlog(Abs(v5)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v5)),2)))/4. - 
       (3*(-2*M_PI*hvs(-v5)*rlog(Abs(v3))*rlog(Abs(v5)) - 
            M_PI*hvs(-v3)*Power(rlog(Abs(v5)),2)))/2. + 
       (3*(hvs(-(v5/v2))*rlog(Abs(v2))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v2)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. - (3*(hvs(-(v5/v2))*rlog(Abs(v5))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v5)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2)))))\
)/2. + 3*hvs(-(v3/v1))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1)))/6. + 
          (Power(rlog(Abs(v1)) - rlog(Abs(v1 - v3)),2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1)))/2.) + 
       9*hvs(-(v4/v1))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v4) + M_PI*sign(v1)))/6. + 
          (Power(rlog(Abs(v1)) - rlog(Abs(v1 - v4)),2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v4) + M_PI*sign(v1)))/2.) + 
       3*hvs(-(v4/v2))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + M_PI*sign(v2)))/6. + 
          (Power(rlog(Abs(v2)) - rlog(Abs(v2 - v4)),2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + M_PI*sign(v2)))/2.) + 
       3*hvs(-(v5/v2))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2)))/6. + 
          (Power(rlog(Abs(v2)) - rlog(Abs(v2 - v5)),2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2)))/2.) + 
       3*hvs(-(v1/v3))*((Power(M_PI,2)*
             (M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3)))/6. + 
          (Power(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)),2)*
             (M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3)))/2.) + 
       3*hvs(-(v5/v3))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3)))/6. + 
          (Power(rlog(Abs(v3)) - rlog(Abs(v3 - v5)),2)*
             (-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3)))/2.) + 
       3*hvs(-(v1/v4))*((Power(M_PI,2)*
             (M_PI*hvs(v1 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/6. + 
          (Power(rlog(Abs(v4)) - rlog(Abs(-v1 + v4)),2)*
             (M_PI*hvs(v1 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/2.) + 
       3*hvs(-(v2/v4))*((Power(M_PI,2)*
             (M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/6. + 
          (Power(rlog(Abs(v4)) - rlog(Abs(-v2 + v4)),2)*
             (M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/2.) - 
       (3*((M_PI*hvs(-v1) - M_PI*hvs(-v4))*
             (hvs((v1 - v4)/v3)*rLi(2,-((v1*(1 - v3/v1 - v4/v1))/v3)) + 
               hvs((-v1 + v4)/v3)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v3/(v1*(1 - v3/v1 - v4/v1)))) - 
                  Power(rlog(Abs((v1*(1 - v3/v1 - v4/v1))/v3)),2)/2.)) + 
            (M_PI*hvs(-v1) - M_PI*hvs(-v3))*
             (hvs((v1 - v3)/v4)*rLi(2,-((v1*(1 - v3/v1 - v4/v1))/v4)) + 
               hvs((-v1 + v3)/v4)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v4/(v1*(1 - v3/v1 - v4/v1)))) - 
                  Power(rlog(Abs((v1*(1 - v3/v1 - v4/v1))/v4)),2)/2.)) + 
            hvs((-v1 + v4)/v3)*(-rlog(Abs(v1)) + rlog(Abs(v4)))*
             rlog(Abs((v1*(1 - v3/v1 - v4/v1))/v3))*
             (2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*hvs(-v1 + v4)*
                hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
               M_PI*hvs((v1*(1 - v3/v1 - v4/v1))/v3) - M_PI*sign(v1)) + 
            hvs((-v1 + v3)/v4)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
             rlog(Abs((v1*(1 - v3/v1 - v4/v1))/v4))*
             (2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*hvs(v4)*
                hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
               M_PI*hvs((v1*(1 - v3/v1 - v4/v1))/v4) - M_PI*sign(v1)) + 
            hvs(-1 - (v1*(v1 - v3 - v4))/(v3*v4))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v1) - 
                    M_PI*hvs((v1*(v1 - v3 - v4))/(v3*v4))))/6. + 
               ((-M_PI - M_PI*hvs((v1*(v1 - v3 - v4))/(v3*v4)) + 
                    2*M_PI*hvs(-v1)*hvs(-(v3*v4)))*
                  Power(rlog(Abs((v1*(v1 - v3 - v4))/(v3*v4))),2))/2. - 
               hvs((-v1 + v3)/v4)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*
                        hvs(v3 - v4)*hvs(v4)*hvs(-v1 + v4)*
                        hvs(-v1 + v3 + v4) - M_PI*sign(v1)))/6. + 
                  (Power(rlog(Abs(1 - (v1 - v3)/v4)),2)*
                     (-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*
                        hvs(v3 - v4)*hvs(v4)*hvs(-v1 + v4)*
                        hvs(-v1 + v3 + v4) - M_PI*sign(v1)))/2.) - 
               hvs((-v1 + v4)/v3)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*
                        hvs(-v1 + v3 + v4) - M_PI*sign(v1)))/6. + 
                  (Power(rlog(Abs(1 - (v1 - v4)/v3)),2)*
                     (-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v1)))/2.) - hvs(-1 + v4/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*
                        hvs(-v1 + v3 + v4) - M_PI*sign(v3)))/6. + 
                  (Power(rlog(Abs(v1)) - rlog(Abs(v4)),2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v3)))/2.) - hvs(-1 + v3/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*
                        hvs(v3 - v4)*hvs(v4)*hvs(-v1 + v4)*
                        hvs(-v1 + v3 + v4) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(v1)) - rlog(Abs(v3)),2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v4)))/2.)) + 
            hvs(1 + (v1*(v1 - v3 - v4))/(v3*v4))*
             (hvs(-v1)*hvs(v3)*hvs(v4)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v1*(v1 - v3 - v4))/(v3*v4)))))/6. \
+ ((2*M_PI - M_PI*hvs(-((v1*(v1 - v3 - v4))/(v3*v4))))*
                     Power(rlog(Abs((v1*(v1 - v3 - v4))/(v3*v4))),2))/2.) \
- hvs((-v1 + v3)/v4)*((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*
                        hvs(v3 - v4)*hvs(v4)*hvs(-v1 + v4)*
                        hvs(-v1 + v3 + v4) - M_PI*sign(v1)))/6. + 
                  (Power(rlog(Abs(1 - (v1 - v3)/v4)),2)*
                     (-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v1)))/2.) - 
               hvs((-v1 + v4)/v3)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v1)))/6. + (Power(rlog(Abs(1 - (v1 - v4)/v3)),2)*
                     (-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v1)))/2.) - hvs(-1 + v4/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v3)))/6. + (Power(rlog(Abs(v1)) - rlog(Abs(v4)),2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v3)))/2.) - hvs(-1 + v3/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(v1)) - rlog(Abs(v3)),2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v4)))/2.))))/2. - 
       (9*((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
             (hvs((-v2 + v4)/v1)*rLi(2,-(((1 - v1/v4 - v2/v4)*v4)/v1)) + 
               hvs((v2 - v4)/v1)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v1/((1 - v1/v4 - v2/v4)*v4))) - 
                  Power(rlog(Abs(((1 - v1/v4 - v2/v4)*v4)/v1)),2)/2.)) + 
            (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4))*
             (hvs((-v1 + v4)/v2)*rLi(2,-(((1 - v1/v4 - v2/v4)*v4)/v2)) + 
               hvs((v1 - v4)/v2)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v2/((1 - v1/v4 - v2/v4)*v4))) - 
                  Power(rlog(Abs(((1 - v1/v4 - v2/v4)*v4)/v2)),2)/2.)) + 
            hvs((v2 - v4)/v1)*(rlog(Abs(v2)) - rlog(Abs(v4)))*
             rlog(Abs(((1 - v1/v4 - v2/v4)*v4)/v1))*
             (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
               M_PI*hvs(((1 - v1/v4 - v2/v4)*v4)/v1) - M_PI*sign(v4)) + 
            hvs((v1 - v4)/v2)*(rlog(Abs(v1)) - rlog(Abs(v4)))*
             rlog(Abs(((1 - v1/v4 - v2/v4)*v4)/v2))*
             (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
               M_PI*hvs(((1 - v1/v4 - v2/v4)*v4)/v2) - M_PI*sign(v4)) + 
            hvs(-1 - (v4*(-v1 - v2 + v4))/(v1*v2))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v4) - 
                    M_PI*hvs((v4*(-v1 - v2 + v4))/(v1*v2))))/6. + 
               ((-M_PI + 2*M_PI*hvs(-(v1*v2))*hvs(-v4) - 
                    M_PI*hvs((v4*(-v1 - v2 + v4))/(v1*v2)))*
                  Power(rlog(Abs((v4*(-v1 - v2 + v4))/(v1*v2))),2))/2. - 
               hvs(-1 + v2/v4)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*
                        hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                        hvs(v4) - M_PI*sign(v1)))/6. + 
                  (Power(-rlog(Abs(v2)) + rlog(Abs(v4)),2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*
                        hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                        hvs(v4) - M_PI*sign(v1)))/2.) - 
               hvs(-1 + v1/v4)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*
                        hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                        hvs(v4) - M_PI*sign(v2)))/6. + 
                  (Power(-rlog(Abs(v1)) + rlog(Abs(v4)),2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v2)))/2.) - 
               hvs((v1 - v4)/v2)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(1 - (-v1 + v4)/v2)),2)*
                     (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4)))/2.) - 
               hvs((v2 - v4)/v1)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(1 - (-v2 + v4)/v1)),2)*
                     (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4)))/2.)) + 
            hvs(1 + (v4*(-v1 - v2 + v4))/(v1*v2))*
             (hvs(v1)*hvs(v2)*hvs(-v4)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v4*(-v1 - v2 + v4))/(v1*v2)))))/
                   6. + ((2*M_PI - 
                       M_PI*hvs(-((v4*(-v1 - v2 + v4))/(v1*v2))))*
                     Power(rlog(Abs((v4*(-v1 - v2 + v4))/(v1*v2))),2))/2.\
) - hvs(-1 + v2/v4)*((Power(M_PI,2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*
                        hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                        hvs(v4) - M_PI*sign(v1)))/6. + 
                  (Power(-rlog(Abs(v2)) + rlog(Abs(v4)),2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v1)))/2.) - 
               hvs(-1 + v1/v4)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v2)))/6. + 
                  (Power(-rlog(Abs(v1)) + rlog(Abs(v4)),2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v2)))/2.) - 
               hvs((v1 - v4)/v2)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(1 - (-v1 + v4)/v2)),2)*
                     (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4)))/2.) - 
               hvs((v2 - v4)/v1)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(1 - (-v2 + v4)/v1)),2)*
                     (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4)))/2.))))/2. \
+ 3*hvs(-(v2/v5))*((Power(M_PI,2)*
             (M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/6. + 
          (Power(rlog(Abs(v5)) - rlog(Abs(-v2 + v5)),2)*
             (M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/2.) + 
       3*hvs(-(v3/v5))*((Power(M_PI,2)*
             (M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/6. + 
          (Power(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)),2)*
             (M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/2.) + 
       (3*((M_PI*hvs(-v3) - M_PI*hvs(-v5))*
             (hvs((v3 - v5)/v1)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
               hvs((-v3 + v5)/v1)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v1/(v3*(1 - v1/v3 - v5/v3)))) - 
                  Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1)),2)/2.)) + 
            (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
             (hvs((-v1 + v3)/v5)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
               hvs((v1 - v3)/v5)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v5/(v3*(1 - v1/v3 - v5/v3)))) - 
                  Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5)),2)/2.)) + 
            hvs((-v3 + v5)/v1)*(-rlog(Abs(v3)) + rlog(Abs(v5)))*
             rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1))*
             (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*hvs(-v1 + v5)*
                hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3)) + 
            hvs((v1 - v3)/v5)*(rlog(Abs(v1)) - rlog(Abs(v3)))*
             rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5))*
             (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3)) + 
            hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v3) - 
                    M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5))))/6. + 
               ((-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                    2*M_PI*hvs(-v3)*hvs(-(v1*v5)))*
                  Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),2))/2. - 
               hvs(-1 + v5/v3)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) \
- M_PI*sign(v1)))/6. + (Power(rlog(Abs(v3)) - rlog(Abs(v5)),2)*
                     (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) \
- M_PI*sign(v1)))/2.) - hvs((v1 - v3)/v5)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*
                        hvs(v1 - v5)*hvs(v5)*hvs(-v3 + v5)*
                        hvs(v1 - v3 + v5) - M_PI*sign(v3)))/6. + 
                  (Power(rlog(Abs(1 - (-v1 + v3)/v5)),2)*
                     (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs((-v3 + v5)/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) \
- M_PI*sign(v3)))/6. + (Power(rlog(Abs(1 - (v3 - v5)/v1)),2)*
                     (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) \
- M_PI*sign(v3)))/2.) - hvs(-1 + v1/v3)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v5)))/6. + 
                  (Power(-rlog(Abs(v1)) + rlog(Abs(v3)),2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v5)))/2.)) + 
            hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
             (hvs(v1)*hvs(-v3)*hvs(v5)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5)))))/
                   6. + ((2*M_PI - 
                       M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))))*
                     Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),2))/2.\
) - hvs(-1 + v5/v3)*((Power(M_PI,2)*
                     (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) \
- M_PI*sign(v1)))/6. + (Power(rlog(Abs(v3)) - rlog(Abs(v5)),2)*
                     (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v1)))/2.) - 
               hvs((v1 - v3)/v5)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/6. + 
                  (Power(rlog(Abs(1 - (-v1 + v3)/v5)),2)*
                     (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs((-v3 + v5)/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) \
- M_PI*sign(v3)))/6. + (Power(rlog(Abs(1 - (v3 - v5)/v1)),2)*
                     (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs(-1 + v1/v3)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v5)))/6. + 
                  (Power(-rlog(Abs(v1)) + rlog(Abs(v3)),2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v5)))/2.))))/2. - 
       (3*((M_PI*hvs(-v2) - M_PI*hvs(-v5))*
             (hvs((v2 - v5)/v4)*rLi(2,-((v2*(1 - v4/v2 - v5/v2))/v4)) + 
               hvs((-v2 + v5)/v4)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v4/(v2*(1 - v4/v2 - v5/v2)))) - 
                  Power(rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v4)),2)/2.)) + 
            (M_PI*hvs(-v2) - M_PI*hvs(-v4))*
             (hvs((v2 - v4)/v5)*rLi(2,-((v2*(1 - v4/v2 - v5/v2))/v5)) + 
               hvs((-v2 + v4)/v5)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v5/(v2*(1 - v4/v2 - v5/v2)))) - 
                  Power(rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v5)),2)/2.)) + 
            hvs((-v2 + v5)/v4)*(-rlog(Abs(v2)) + rlog(Abs(v5)))*
             rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v4))*
             (2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*hvs(-v2 + v5)*
                hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
               M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v4) - M_PI*sign(v2)) + 
            hvs((-v2 + v4)/v5)*(-rlog(Abs(v2)) + rlog(Abs(v4)))*
             rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v5))*
             (2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*hvs(v5)*
                hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
               M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v5) - M_PI*sign(v2)) + 
            hvs(-1 - (v2*(v2 - v4 - v5))/(v4*v5))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v2) - 
                    M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5))))/6. + 
               ((-M_PI - M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5)) + 
                    2*M_PI*hvs(-v2)*hvs(-(v4*v5)))*
                  Power(rlog(Abs((v2*(v2 - v4 - v5))/(v4*v5))),2))/2. - 
               hvs((-v2 + v4)/v5)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*
                        hvs(v4 - v5)*hvs(v5)*hvs(-v2 + v5)*
                        hvs(-v2 + v4 + v5) - M_PI*sign(v2)))/6. + 
                  (Power(rlog(Abs(1 - (v2 - v4)/v5)),2)*
                     (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*
                        hvs(v4 - v5)*hvs(v5)*hvs(-v2 + v5)*
                        hvs(-v2 + v4 + v5) - M_PI*sign(v2)))/2.) - 
               hvs((-v2 + v5)/v4)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*
                        hvs(-v2 + v4 + v5) - M_PI*sign(v2)))/6. + 
                  (Power(rlog(Abs(1 - (v2 - v5)/v4)),2)*
                     (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v2)))/2.) - hvs(-1 + v5/v2)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*
                        hvs(-v2 + v4 + v5) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(v2)) - rlog(Abs(v5)),2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v4)))/2.) - hvs(-1 + v4/v2)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*
                        hvs(v4 - v5)*hvs(v5)*hvs(-v2 + v5)*
                        hvs(-v2 + v4 + v5) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(v2)) - rlog(Abs(v4)),2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v5)))/2.)) + 
            hvs(1 + (v2*(v2 - v4 - v5))/(v4*v5))*
             (hvs(-v2)*hvs(v4)*hvs(v5)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5)))))/6. \
+ ((2*M_PI - M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5))))*
                     Power(rlog(Abs((v2*(v2 - v4 - v5))/(v4*v5))),2))/2.) \
- hvs((-v2 + v4)/v5)*((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*
                        hvs(v4 - v5)*hvs(v5)*hvs(-v2 + v5)*
                        hvs(-v2 + v4 + v5) - M_PI*sign(v2)))/6. + 
                  (Power(rlog(Abs(1 - (v2 - v4)/v5)),2)*
                     (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v2)))/2.) - 
               hvs((-v2 + v5)/v4)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v2)))/6. + (Power(rlog(Abs(1 - (v2 - v5)/v4)),2)*
                     (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v2)))/2.) - hvs(-1 + v5/v2)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v4)))/6. + (Power(rlog(Abs(v2)) - rlog(Abs(v5)),2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v4)))/2.) - hvs(-1 + v4/v2)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(v2)) - rlog(Abs(v4)),2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v5)))/2.))))/2. + 
       (3*((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
             (hvs((-v3 + v5)/v2)*rLi(2,-(((1 - v2/v5 - v3/v5)*v5)/v2)) + 
               hvs((v3 - v5)/v2)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v2/((1 - v2/v5 - v3/v5)*v5))) - 
                  Power(rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v2)),2)/2.)) + 
            (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5))*
             (hvs((-v2 + v5)/v3)*rLi(2,-(((1 - v2/v5 - v3/v5)*v5)/v3)) + 
               hvs((v2 - v5)/v3)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v3/((1 - v2/v5 - v3/v5)*v5))) - 
                  Power(rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v3)),2)/2.)) + 
            hvs((v3 - v5)/v2)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
             rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v2))*
             (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
               M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v2) - M_PI*sign(v5)) + 
            hvs((v2 - v5)/v3)*(rlog(Abs(v2)) - rlog(Abs(v5)))*
             rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v3))*
             (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
               M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v3) - M_PI*sign(v5)) + 
            hvs(-1 - (v5*(-v2 - v3 + v5))/(v2*v3))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v5) - 
                    M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3))))/6. + 
               ((-M_PI + 2*M_PI*hvs(-(v2*v3))*hvs(-v5) - 
                    M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3)))*
                  Power(rlog(Abs((v5*(-v2 - v3 + v5))/(v2*v3))),2))/2. - 
               hvs(-1 + v3/v5)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*
                        hvs(v2 - v5)*hvs(v3 - v5)*hvs(v2 + v3 - v5)*
                        hvs(v5) - M_PI*sign(v2)))/6. + 
                  (Power(-rlog(Abs(v3)) + rlog(Abs(v5)),2)*
                     (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v2)))/2.) - 
               hvs(-1 + v2/v5)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v3)))/6. + 
                  (Power(-rlog(Abs(v2)) + rlog(Abs(v5)),2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs((v2 - v5)/v3)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(1 - (-v2 + v5)/v3)),2)*
                     (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/2.) - 
               hvs((v3 - v5)/v2)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(1 - (-v3 + v5)/v2)),2)*
                     (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/2.)) + 
            hvs(1 + (v5*(-v2 - v3 + v5))/(v2*v3))*
             (hvs(v2)*hvs(v3)*hvs(-v5)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3)))))/6. \
+ ((2*M_PI - M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3))))*
                     Power(rlog(Abs((v5*(-v2 - v3 + v5))/(v2*v3))),2))/2.) \
- hvs(-1 + v3/v5)*((Power(M_PI,2)*
                     (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v2)))/6. + 
                  (Power(-rlog(Abs(v3)) + rlog(Abs(v5)),2)*
                     (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v2)))/2.) - 
               hvs(-1 + v2/v5)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v3)))/6. + 
                  (Power(-rlog(Abs(v2)) + rlog(Abs(v5)),2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs((v2 - v5)/v3)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(1 - (-v2 + v5)/v3)),2)*
                     (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/2.) - 
               hvs((v3 - v5)/v2)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(1 - (-v3 + v5)/v2)),2)*
                     (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/2.))))/2.)\
)/(4.*v3*(v1 + v2 - v4)*v4*v5*sqrt(-Power(v1*v2 + v3*(-v2 + v4),2) - 
       2*(-(Power(v1,2)*v2) + v3*(v2 - v4)*v4 + v1*(v3*v4 + v2*(v3 + v4)))*
        v5 - Power(v1 - v4,2)*Power(v5,2))) - 
  ((-2*(Power(v2,2)*Power(-v1 + v3,2) + Power(v3*v4 + (v1 - v4)*v5,2) + 
          2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + 
             v3*(v1*v5 + v4*(v1 + v5))))*
        (-((-(t*v1f) - (1 - t)*v1i)*(-v2f + v2i)) - 
          (-v1f + v1i)*(-(t*v2f) - (1 - t)*v2i) + 
          (-(t*v2f) - (1 - t)*v2i)*(-v3f + v3i) + 
          (-v2f + v2i)*(-(t*v3f) - (1 - t)*v3i) - 
          (-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) - 
          (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) - 
          (-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
          (-(t*v4f) - (1 - t)*v4i)*(-v5f + v5i) - 
          (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
          (-v4f + v4i)*(-(t*v5f) - (1 - t)*v5i)) + 
       (-(v1*v2) + v2*v3 - v3*v4 - v1*v5 + v4*v5)*
        (2*Power(-(t*v2f) - (1 - t)*v2i,2)*(v1f - v1i - v3f + v3i)*
           (t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i) + 
          2*(-v2f + v2i)*(-(t*v2f) - (1 - t)*v2i)*
           Power(t*v1f + (1 - t)*v1i - t*v3f - (1 - t)*v3i,2) + 
          2*((-(t*v3f) - (1 - t)*v3i)*(-v4f + v4i) + 
             (-v3f + v3i)*(-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
              (-v5f + v5i) + (-v1f + v1i + v4f - v4i)*
              (-(t*v5f) - (1 - t)*v5i))*
           ((-(t*v3f) - (1 - t)*v3i)*(-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i + t*v4f + (1 - t)*v4i)*
              (-(t*v5f) - (1 - t)*v5i)) + 
          2*(-(t*v2f) - (1 - t)*v2i)*
           (-(Power(-(t*v3f) - (1 - t)*v3i,2)*(-v4f + v4i)) - 
             2*(-v3f + v3i)*(-(t*v3f) - (1 - t)*v3i)*
              (-(t*v4f) - (1 - t)*v4i) + 
             (-(t*v1f) - (1 - t)*v1i)*
              (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*(-v5f + v5i) + 
             (-(t*v1f) - (1 - t)*v1i)*(v1f - v1i - v4f + v4i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-v1f + v1i)*(t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v3f) - (1 - t)*v3i)*
              ((-(t*v1f) - (1 - t)*v1i)*(-v5f + v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*(-v1f + v1i - v5f + v5i) + 
                (-v1f + v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-v4f + v4i)*(-(t*v1f) - (1 - t)*v1i - t*v5f - 
                   (1 - t)*v5i)) + 
             (-v3f + v3i)*((-(t*v1f) - (1 - t)*v1i)*
                 (-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i))) + 
          2*(-v2f + v2i)*(-(Power(-(t*v3f) - (1 - t)*v3i,2)*
                (-(t*v4f) - (1 - t)*v4i)) + 
             (-(t*v1f) - (1 - t)*v1i)*
              (t*v1f + (1 - t)*v1i - t*v4f - (1 - t)*v4i)*
              (-(t*v5f) - (1 - t)*v5i) + 
             (-(t*v3f) - (1 - t)*v3i)*
              ((-(t*v1f) - (1 - t)*v1i)*(-(t*v5f) - (1 - t)*v5i) + 
                (-(t*v4f) - (1 - t)*v4i)*
                 (-(t*v1f) - (1 - t)*v1i - t*v5f - (1 - t)*v5i)))))*
     ((-7*Power(M_PI,3)*hvs(-v1))/4. - (Power(M_PI,3)*hvs(-v2))/4. - 
       (Power(M_PI,3)*hvs(-v3))/4. + (Power(M_PI,3)*hvs(-v4))/4. - 
       (Power(M_PI,3)*hvs(-v5))/2. + 
       (15*M_PI*hvs(-v1)*Power(rlog(Abs(v1)),2))/2. - 
       (3*M_PI*hvs(-v2)*Power(rlog(Abs(v2)),2))/4. - 
       (3*(-(M_PI*hvs(-v2)*Power(rlog(Abs(v1)),2)) - 
            2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v2))))/4. + 
       (9*(-(M_PI*hvs(-v2)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
                 hvs(-(v1/v3))*
                  (Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                    rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3)))))\
) + hvs(-(v1/v3))*rlog(Abs(v2))*
             (-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*rlog(Abs(1 - v1/v3))) - 
               M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))))/2. + 
       (3*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v3)))/2. - 
       (3*rlog(Abs(v1))*(-(M_PI*hvs(-v3)*rlog(Abs(v2))) - 
            M_PI*hvs(-v2)*rlog(Abs(v3))))/2. + 
       3*(-(M_PI*hvs(-v3)*Power(rlog(Abs(v1)),2)) - 
          2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v3))) + 
       3*(-(M_PI*hvs(-v3)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
               hvs(-(v1/v3))*(Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                  rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3)))))) + 
          hvs(-(v1/v3))*(-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
                rlog(Abs(1 - v1/v3))) - 
             M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))*
           rlog(Abs(v3))) + (3*
          (-2*M_PI*hvs(-v3)*rlog(Abs(v1))*rlog(Abs(v3)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v3)),2)))/4. + 
       (9*(-2*M_PI*hvs(-v3)*rlog(Abs(v2))*rlog(Abs(v3)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v3)),2)))/4. - 
       (3*(-(M_PI*hvs(-v2)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
                 hvs(-(v2/v4))*
                  (Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                    rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4)))))\
) + hvs(-(v2/v4))*rlog(Abs(v2))*
             (-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*rlog(Abs(1 - v2/v4))) - 
               M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))))/2. - 
       (3*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v4)))/2. + 
       6*M_PI*hvs(-v1)*rlog(Abs(v3))*rlog(Abs(v4)) + 
       3*M_PI*hvs(-v2)*rlog(Abs(v3))*rlog(Abs(v4)) + 
       (21*M_PI*hvs(-v4)*Power(rlog(Abs(v4)),2))/4. + 
       (3*rlog(Abs(v1))*(-(M_PI*hvs(-v4)*rlog(Abs(v2))) - 
            M_PI*hvs(-v2)*rlog(Abs(v4))))/2. - 
       6*rlog(Abs(v1))*(-(M_PI*hvs(-v4)*rlog(Abs(v3))) - 
          M_PI*hvs(-v3)*rlog(Abs(v4))) - 
       3*rlog(Abs(v2))*(-(M_PI*hvs(-v4)*rlog(Abs(v3))) - 
          M_PI*hvs(-v3)*rlog(Abs(v4))) + 
       (3*(-(M_PI*hvs(-v4)*Power(rlog(Abs(v1)),2)) - 
            2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v4))))/4. + 
       (3*(-(M_PI*hvs(-v4)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v4))))/4. + 
       (9*(-(M_PI*hvs(-v4)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
                 hvs(-(v1/v3))*
                  (Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                    rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3)))))\
) + hvs(-(v1/v3))*(-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
                  rlog(Abs(1 - v1/v3))) - 
               M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))*
             rlog(Abs(v4))))/2. + 
       (3*(-(M_PI*hvs(-v4)*Power(rlog(Abs(v3)),2)) - 
            2*M_PI*hvs(-v3)*rlog(Abs(v3))*rlog(Abs(v4))))/4. + 
       (3*(-(M_PI*hvs(-v4)*(hvs(v2/v4)*rLi(2,1 - v2/v4) + 
                 hvs(-(v2/v4))*
                  (Power(M_PI,2)/6. - rLi(2,v2/v4) - 
                    rlog(Abs(1 - v2/v4))*(rlog(Abs(v2)) - rlog(Abs(v4)))))\
) + hvs(-(v2/v4))*(-((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
                  rlog(Abs(1 - v2/v4))) - 
               M_PI*hvs(-1 + v2/v4)*(rlog(Abs(v2)) - rlog(Abs(v4))))*
             rlog(Abs(v4))))/2. + 
       (27*(-2*M_PI*hvs(-v4)*rlog(Abs(v1))*rlog(Abs(v4)) - 
            M_PI*hvs(-v1)*Power(rlog(Abs(v4)),2)))/4. - 
       (3*(-2*M_PI*hvs(-v4)*rlog(Abs(v3))*rlog(Abs(v4)) - 
            M_PI*hvs(-v3)*Power(rlog(Abs(v4)),2)))/2. - 
       (3*(hvs(-(v4/v1))*rlog(Abs(v2))*
             (-(M_PI*hvs(-1 + v4/v1)*(-rlog(Abs(v1)) + rlog(Abs(v4)))) - 
               (M_PI*hvs(-v1) - M_PI*hvs(-v4))*rlog(Abs(1 - v4/v1))) - 
            M_PI*hvs(-v2)*(hvs(v4/v1)*rLi(2,1 - v4/v1) + 
               hvs(-(v4/v1))*(Power(M_PI,2)/6. - rLi(2,v4/v1) - 
                  (-rlog(Abs(v1)) + rlog(Abs(v4)))*rlog(Abs(1 - v4/v1))))))/
        2. - (3*(hvs(-(v4/v1))*rlog(Abs(v3))*
             (-(M_PI*hvs(-1 + v4/v1)*(-rlog(Abs(v1)) + rlog(Abs(v4)))) - 
               (M_PI*hvs(-v1) - M_PI*hvs(-v4))*rlog(Abs(1 - v4/v1))) - 
            M_PI*hvs(-v3)*(hvs(v4/v1)*rLi(2,1 - v4/v1) + 
               hvs(-(v4/v1))*(Power(M_PI,2)/6. - rLi(2,v4/v1) - 
                  (-rlog(Abs(v1)) + rlog(Abs(v4)))*rlog(Abs(1 - v4/v1))))))/
        2. - 6*(hvs(-(v4/v1))*rlog(Abs(v4))*
           (-(M_PI*hvs(-1 + v4/v1)*(-rlog(Abs(v1)) + rlog(Abs(v4)))) - 
             (M_PI*hvs(-v1) - M_PI*hvs(-v4))*rlog(Abs(1 - v4/v1))) - 
          M_PI*hvs(-v4)*(hvs(v4/v1)*rLi(2,1 - v4/v1) + 
             hvs(-(v4/v1))*(Power(M_PI,2)/6. - rLi(2,v4/v1) - 
                (-rlog(Abs(v1)) + rlog(Abs(v4)))*rlog(Abs(1 - v4/v1))))) - 
       (3*(-(M_PI*hvs(-v3)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5)))))\
) + hvs(-(v3/v5))*rlog(Abs(v3))*
             (-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))))/2. - 
       (3*M_PI*hvs(-v1)*rlog(Abs(v2))*rlog(Abs(v5)))/2. - 
       (9*M_PI*hvs(-v1)*rlog(Abs(v4))*rlog(Abs(v5)))/2. + 
       (9*M_PI*hvs(-v3)*rlog(Abs(v4))*rlog(Abs(v5)))/2. - 
       (3*M_PI*hvs(-v5)*Power(rlog(Abs(v5)),2))/4. + 
       (3*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v2))) - 
            M_PI*hvs(-v2)*rlog(Abs(v5))))/2. + 
       (9*rlog(Abs(v1))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
            M_PI*hvs(-v4)*rlog(Abs(v5))))/2. - 
       (9*rlog(Abs(v3))*(-(M_PI*hvs(-v5)*rlog(Abs(v4))) - 
            M_PI*hvs(-v4)*rlog(Abs(v5))))/2. - 
       3*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v1)),2)) - 
          2*M_PI*hvs(-v1)*rlog(Abs(v1))*rlog(Abs(v5))) - 
       (3*(-(M_PI*hvs(-v5)*Power(rlog(Abs(v2)),2)) - 
            2*M_PI*hvs(-v2)*rlog(Abs(v2))*rlog(Abs(v5))))/2. + 
       3*(-(M_PI*hvs(-v5)*(hvs(v1/v3)*rLi(2,1 - v1/v3) + 
               hvs(-(v1/v3))*(Power(M_PI,2)/6. - rLi(2,v1/v3) - 
                  rlog(Abs(1 - v1/v3))*(rlog(Abs(v1)) - rlog(Abs(v3)))))) + 
          hvs(-(v1/v3))*(-((-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
                rlog(Abs(1 - v1/v3))) - 
             M_PI*hvs(-1 + v1/v3)*(rlog(Abs(v1)) - rlog(Abs(v3))))*
           rlog(Abs(v5))) + (15*
          (-(M_PI*hvs(-v5)*Power(rlog(Abs(v3)),2)) - 
            2*M_PI*hvs(-v3)*rlog(Abs(v3))*rlog(Abs(v5))))/4. - 
       6*(-(M_PI*hvs(-v5)*(hvs(v4/v1)*rLi(2,1 - v4/v1) + 
               hvs(-(v4/v1))*(Power(M_PI,2)/6. - rLi(2,v4/v1) - 
                  (-rlog(Abs(v1)) + rlog(Abs(v4)))*rlog(Abs(1 - v4/v1))))) \
+ hvs(-(v4/v1))*(-(M_PI*hvs(-1 + v4/v1)*(-rlog(Abs(v1)) + rlog(Abs(v4)))) - 
             (M_PI*hvs(-v1) - M_PI*hvs(-v4))*rlog(Abs(1 - v4/v1)))*rlog(Abs(v5))\
) + (3*(-(M_PI*hvs(-v5)*(hvs(v3/v5)*rLi(2,1 - v3/v5) + 
                 hvs(-(v3/v5))*
                  (Power(M_PI,2)/6. - rLi(2,v3/v5) - 
                    rlog(Abs(1 - v3/v5))*(rlog(Abs(v3)) - rlog(Abs(v5)))))\
) + hvs(-(v3/v5))*(-((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
                  rlog(Abs(1 - v3/v5))) - 
               M_PI*hvs(-1 + v3/v5)*(rlog(Abs(v3)) - rlog(Abs(v5))))*
             rlog(Abs(v5))))/2. + 
       (3*(-2*M_PI*hvs(-v5)*rlog(Abs(v2))*rlog(Abs(v5)) - 
            M_PI*hvs(-v2)*Power(rlog(Abs(v5)),2)))/4. - 
       (3*(-2*M_PI*hvs(-v5)*rlog(Abs(v3))*rlog(Abs(v5)) - 
            M_PI*hvs(-v3)*Power(rlog(Abs(v5)),2)))/2. + 
       (3*(hvs(-(v5/v2))*rlog(Abs(v2))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v2)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2))))))/
        2. - (3*(hvs(-(v5/v2))*rlog(Abs(v5))*
             (-(M_PI*hvs(-1 + v5/v2)*(-rlog(Abs(v2)) + rlog(Abs(v5)))) - 
               (M_PI*hvs(-v2) - M_PI*hvs(-v5))*rlog(Abs(1 - v5/v2))) - 
            M_PI*hvs(-v5)*(hvs(v5/v2)*rLi(2,1 - v5/v2) + 
               hvs(-(v5/v2))*(Power(M_PI,2)/6. - rLi(2,v5/v2) - 
                  (-rlog(Abs(v2)) + rlog(Abs(v5)))*rlog(Abs(1 - v5/v2))))))/
        2. + 3*hvs(-(v3/v1))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1)))/6. + 
          (Power(rlog(Abs(v1)) - rlog(Abs(v1 - v3)),2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1)))/2.) + 
       3*hvs(-(v4/v1))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v4) + M_PI*sign(v1)))/6. + 
          (Power(rlog(Abs(v1)) - rlog(Abs(v1 - v4)),2)*
             (-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v4) + M_PI*sign(v1)))/2.) + 
       3*hvs(-(v4/v2))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + M_PI*sign(v2)))/6. + 
          (Power(rlog(Abs(v2)) - rlog(Abs(v2 - v4)),2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v4) + M_PI*sign(v2)))/2.) + 
       3*hvs(-(v5/v2))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2)))/6. + 
          (Power(rlog(Abs(v2)) - rlog(Abs(v2 - v5)),2)*
             (-(M_PI*hvs(-v2)) + M_PI*hvs(-v2 + v5) + M_PI*sign(v2)))/2.) + 
       3*hvs(-(v1/v3))*((Power(M_PI,2)*
             (M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3)))/6. + 
          (Power(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)),2)*
             (M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3)))/2.) + 
       3*hvs(-(v5/v3))*((Power(M_PI,2)*
             (-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3)))/6. + 
          (Power(rlog(Abs(v3)) - rlog(Abs(v3 - v5)),2)*
             (-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3)))/2.) + 
       9*hvs(-(v1/v4))*((Power(M_PI,2)*
             (M_PI*hvs(v1 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/6. + 
          (Power(rlog(Abs(v4)) - rlog(Abs(-v1 + v4)),2)*
             (M_PI*hvs(v1 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/2.) + 
       3*hvs(-(v2/v4))*((Power(M_PI,2)*
             (M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/6. + 
          (Power(rlog(Abs(v4)) - rlog(Abs(-v2 + v4)),2)*
             (M_PI*hvs(v2 - v4) - M_PI*hvs(-v4) + M_PI*sign(v4)))/2.) - 
       (9*((M_PI*hvs(-v1) - M_PI*hvs(-v4))*
             (hvs((v1 - v4)/v3)*rLi(2,-((v1*(1 - v3/v1 - v4/v1))/v3)) + 
               hvs((-v1 + v4)/v3)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v3/(v1*(1 - v3/v1 - v4/v1)))) - 
                  Power(rlog(Abs((v1*(1 - v3/v1 - v4/v1))/v3)),2)/2.)) + 
            (M_PI*hvs(-v1) - M_PI*hvs(-v3))*
             (hvs((v1 - v3)/v4)*rLi(2,-((v1*(1 - v3/v1 - v4/v1))/v4)) + 
               hvs((-v1 + v3)/v4)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v4/(v1*(1 - v3/v1 - v4/v1)))) - 
                  Power(rlog(Abs((v1*(1 - v3/v1 - v4/v1))/v4)),2)/2.)) + 
            hvs((-v1 + v4)/v3)*(-rlog(Abs(v1)) + rlog(Abs(v4)))*
             rlog(Abs((v1*(1 - v3/v1 - v4/v1))/v3))*
             (2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*hvs(-v1 + v4)*
                hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
               M_PI*hvs((v1*(1 - v3/v1 - v4/v1))/v3) - M_PI*sign(v1)) + 
            hvs((-v1 + v3)/v4)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
             rlog(Abs((v1*(1 - v3/v1 - v4/v1))/v4))*
             (2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*hvs(v4)*
                hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
               M_PI*hvs((v1*(1 - v3/v1 - v4/v1))/v4) - M_PI*sign(v1)) + 
            hvs(-1 - (v1*(v1 - v3 - v4))/(v3*v4))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v1) - 
                    M_PI*hvs((v1*(v1 - v3 - v4))/(v3*v4))))/6. + 
               ((-M_PI - M_PI*hvs((v1*(v1 - v3 - v4))/(v3*v4)) + 
                    2*M_PI*hvs(-v1)*hvs(-(v3*v4)))*
                  Power(rlog(Abs((v1*(v1 - v3 - v4))/(v3*v4))),2))/2. - 
               hvs((-v1 + v3)/v4)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*
                        hvs(v3 - v4)*hvs(v4)*hvs(-v1 + v4)*
                        hvs(-v1 + v3 + v4) - M_PI*sign(v1)))/6. + 
                  (Power(rlog(Abs(1 - (v1 - v3)/v4)),2)*
                     (-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v1)))/2.) - 
               hvs((-v1 + v4)/v3)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v1)))/6. + (Power(rlog(Abs(1 - (v1 - v4)/v3)),2)*
                     (-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v1)))/2.) - hvs(-1 + v4/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v3)))/6. + (Power(rlog(Abs(v1)) - rlog(Abs(v4)),2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v3)))/2.) - hvs(-1 + v3/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(v1)) - rlog(Abs(v3)),2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v4)))/2.)) + 
            hvs(1 + (v1*(v1 - v3 - v4))/(v3*v4))*
             (hvs(-v1)*hvs(v3)*hvs(v4)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v1*(v1 - v3 - v4))/(v3*v4)))))/6. \
+ ((2*M_PI - M_PI*hvs(-((v1*(v1 - v3 - v4))/(v3*v4))))*
                     Power(rlog(Abs((v1*(v1 - v3 - v4))/(v3*v4))),2))/2.) \
- hvs((-v1 + v3)/v4)*((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v1)))/6. + 
                  (Power(rlog(Abs(1 - (v1 - v3)/v4)),2)*
                     (-(M_PI*hvs(-1 + (v1 - v3)/v4)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v1)))/2.) - 
               hvs((-v1 + v4)/v3)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v1)))/6. + (Power(rlog(Abs(1 - (v1 - v4)/v3)),2)*
                     (-(M_PI*hvs(-1 + (v1 - v4)/v3)) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v1)))/2.) - 
               hvs(-1 + v4/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) \
- M_PI*sign(v3)))/6. + (Power(rlog(Abs(v1)) - rlog(Abs(v4)),2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v4)*
                        hvs(-v1 + v4)*hvs(-v3 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v3)))/2.) - 
               hvs(-1 + v3/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(v1)) - rlog(Abs(v3)),2)*
                     (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v3)*hvs(-v1 + v3)*hvs(v3 - v4)*
                        hvs(v4)*hvs(-v1 + v4)*hvs(-v1 + v3 + v4) - 
                       M_PI*sign(v4)))/2.))))/2. - 
       (3*((-(M_PI*hvs(-v2)) + M_PI*hvs(-v4))*
             (hvs((-v2 + v4)/v1)*rLi(2,-(((1 - v1/v4 - v2/v4)*v4)/v1)) + 
               hvs((v2 - v4)/v1)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v1/((1 - v1/v4 - v2/v4)*v4))) - 
                  Power(rlog(Abs(((1 - v1/v4 - v2/v4)*v4)/v1)),2)/2.)) + 
            (-(M_PI*hvs(-v1)) + M_PI*hvs(-v4))*
             (hvs((-v1 + v4)/v2)*rLi(2,-(((1 - v1/v4 - v2/v4)*v4)/v2)) + 
               hvs((v1 - v4)/v2)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v2/((1 - v1/v4 - v2/v4)*v4))) - 
                  Power(rlog(Abs(((1 - v1/v4 - v2/v4)*v4)/v2)),2)/2.)) + 
            hvs((v2 - v4)/v1)*(rlog(Abs(v2)) - rlog(Abs(v4)))*
             rlog(Abs(((1 - v1/v4 - v2/v4)*v4)/v1))*
             (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
               M_PI*hvs(((1 - v1/v4 - v2/v4)*v4)/v1) - M_PI*sign(v4)) + 
            hvs((v1 - v4)/v2)*(rlog(Abs(v1)) - rlog(Abs(v4)))*
             rlog(Abs(((1 - v1/v4 - v2/v4)*v4)/v2))*
             (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
               M_PI*hvs(((1 - v1/v4 - v2/v4)*v4)/v2) - M_PI*sign(v4)) + 
            hvs(-1 - (v4*(-v1 - v2 + v4))/(v1*v2))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v4) - 
                    M_PI*hvs((v4*(-v1 - v2 + v4))/(v1*v2))))/6. + 
               ((-M_PI + 2*M_PI*hvs(-(v1*v2))*hvs(-v4) - 
                    M_PI*hvs((v4*(-v1 - v2 + v4))/(v1*v2)))*
                  Power(rlog(Abs((v4*(-v1 - v2 + v4))/(v1*v2))),2))/2. - 
               hvs(-1 + v2/v4)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*
                        hvs(v1 - v4)*hvs(v2 - v4)*hvs(v1 + v2 - v4)*
                        hvs(v4) - M_PI*sign(v1)))/6. + 
                  (Power(-rlog(Abs(v2)) + rlog(Abs(v4)),2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v1)))/2.) - 
               hvs(-1 + v1/v4)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v2)))/6. + 
                  (Power(-rlog(Abs(v1)) + rlog(Abs(v4)),2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v2)))/2.) - 
               hvs((v1 - v4)/v2)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(1 - (-v1 + v4)/v2)),2)*
                     (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4)))/2.) - 
               hvs((v2 - v4)/v1)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(1 - (-v2 + v4)/v1)),2)*
                     (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4)))/2.)) + 
            hvs(1 + (v4*(-v1 - v2 + v4))/(v1*v2))*
             (hvs(v1)*hvs(v2)*hvs(-v4)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v4*(-v1 - v2 + v4))/(v1*v2)))))/6. \
+ ((2*M_PI - M_PI*hvs(-((v4*(-v1 - v2 + v4))/(v1*v2))))*
                     Power(rlog(Abs((v4*(-v1 - v2 + v4))/(v1*v2))),2))/2.) \
- hvs(-1 + v2/v4)*((Power(M_PI,2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v1)))/6. + 
                  (Power(-rlog(Abs(v2)) + rlog(Abs(v4)),2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v1)))/2.) - 
               hvs(-1 + v1/v4)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v2)))/6. + 
                  (Power(-rlog(Abs(v1)) + rlog(Abs(v4)),2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*sign(v2)))/2.) - 
               hvs((v1 - v4)/v2)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(1 - (-v1 + v4)/v2)),2)*
                     (2*M_PI*hvs(v1)*hvs(v1 - v2)*hvs(v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v1 + v4)/v2) - M_PI*sign(v4)))/2.) - 
               hvs((v2 - v4)/v1)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4)))/6. + 
                  (Power(rlog(Abs(1 - (-v2 + v4)/v1)),2)*
                     (2*M_PI*hvs(v1)*hvs(v2)*hvs(-v1 + v2)*hvs(v1 - v4)*
                        hvs(v2 - v4)*hvs(v1 + v2 - v4)*hvs(v4) - 
                       M_PI*hvs(-1 + (-v2 + v4)/v1) - M_PI*sign(v4)))/2.))))/2. \
+ 3*hvs(-(v2/v5))*((Power(M_PI,2)*
             (M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/6. + 
          (Power(rlog(Abs(v5)) - rlog(Abs(-v2 + v5)),2)*
             (M_PI*hvs(v2 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/2.) + 
       3*hvs(-(v3/v5))*((Power(M_PI,2)*
             (M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/6. + 
          (Power(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)),2)*
             (M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5)))/2.) - 
       (3*((M_PI*hvs(-v3) - M_PI*hvs(-v5))*
             (hvs((v3 - v5)/v1)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
               hvs((-v3 + v5)/v1)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v1/(v3*(1 - v1/v3 - v5/v3)))) - 
                  Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1)),2)/2.)) + 
            (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*
             (hvs((-v1 + v3)/v5)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
               hvs((v1 - v3)/v5)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v5/(v3*(1 - v1/v3 - v5/v3)))) - 
                  Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5)),2)/2.)) + 
            hvs((-v3 + v5)/v1)*(-rlog(Abs(v3)) + rlog(Abs(v5)))*
             rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1))*
             (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*hvs(-v1 + v5)*
                hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3)) + 
            hvs((v1 - v3)/v5)*(rlog(Abs(v1)) - rlog(Abs(v3)))*
             rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5))*
             (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3)) + 
            hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v3) - 
                    M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5))))/6. + 
               ((-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                    2*M_PI*hvs(-v3)*hvs(-(v1*v5)))*
                  Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),2))/2. - 
               hvs(-1 + v5/v3)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) \
- M_PI*sign(v1)))/6. + (Power(rlog(Abs(v3)) - rlog(Abs(v5)),2)*
                     (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v1)))/2.) - 
               hvs((v1 - v3)/v5)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/6. + 
                  (Power(rlog(Abs(1 - (-v1 + v3)/v5)),2)*
                     (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs((-v3 + v5)/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) \
- M_PI*sign(v3)))/6. + (Power(rlog(Abs(1 - (v3 - v5)/v1)),2)*
                     (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs(-1 + v1/v3)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v5)))/6. + 
                  (Power(-rlog(Abs(v1)) + rlog(Abs(v3)),2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v5)))/2.)) + 
            hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
             (hvs(v1)*hvs(-v3)*hvs(v5)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5)))))/6. \
+ ((2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))))*
                     Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),2))/2.) \
- hvs(-1 + v5/v3)*((Power(M_PI,2)*
                     (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v1)))/6. + 
                  (Power(rlog(Abs(v3)) - rlog(Abs(v5)),2)*
                     (-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v1)))/2.) - 
               hvs((v1 - v3)/v5)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/6. + 
                  (Power(rlog(Abs(1 - (-v1 + v3)/v5)),2)*
                     (-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs((-v3 + v5)/v1)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/6. + 
                  (Power(rlog(Abs(1 - (v3 - v5)/v1)),2)*
                     (-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                        hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs(-1 + v1/v3)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v5)))/6. + 
                  (Power(-rlog(Abs(v1)) + rlog(Abs(v3)),2)*
                     (M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                       2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                        hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                       M_PI*sign(v5)))/2.))))/2. + 
       (3*((M_PI*hvs(-v2) - M_PI*hvs(-v5))*
             (hvs((v2 - v5)/v4)*rLi(2,-((v2*(1 - v4/v2 - v5/v2))/v4)) + 
               hvs((-v2 + v5)/v4)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v4/(v2*(1 - v4/v2 - v5/v2)))) - 
                  Power(rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v4)),2)/2.)) + 
            (M_PI*hvs(-v2) - M_PI*hvs(-v4))*
             (hvs((v2 - v4)/v5)*rLi(2,-((v2*(1 - v4/v2 - v5/v2))/v5)) + 
               hvs((-v2 + v4)/v5)*
                (-Power(M_PI,2)/6. - 
                  rLi(2,-(v5/(v2*(1 - v4/v2 - v5/v2)))) - 
                  Power(rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v5)),2)/2.)) + 
            hvs((-v2 + v5)/v4)*(-rlog(Abs(v2)) + rlog(Abs(v5)))*
             rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v4))*
             (2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*hvs(-v2 + v5)*
                hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
               M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v4) - M_PI*sign(v2)) + 
            hvs((-v2 + v4)/v5)*(-rlog(Abs(v2)) + rlog(Abs(v4)))*
             rlog(Abs((v2*(1 - v4/v2 - v5/v2))/v5))*
             (2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*hvs(v5)*
                hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
               M_PI*hvs((v2*(1 - v4/v2 - v5/v2))/v5) - M_PI*sign(v2)) + 
            hvs(-1 - (v2*(v2 - v4 - v5))/(v4*v5))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v2) - 
                    M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5))))/6. + 
               ((-M_PI - M_PI*hvs((v2*(v2 - v4 - v5))/(v4*v5)) + 
                    2*M_PI*hvs(-v2)*hvs(-(v4*v5)))*
                  Power(rlog(Abs((v2*(v2 - v4 - v5))/(v4*v5))),2))/2. - 
               hvs((-v2 + v4)/v5)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*
                        hvs(v4 - v5)*hvs(v5)*hvs(-v2 + v5)*
                        hvs(-v2 + v4 + v5) - M_PI*sign(v2)))/6. + 
                  (Power(rlog(Abs(1 - (v2 - v4)/v5)),2)*
                     (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v2)))/2.) - 
               hvs((-v2 + v5)/v4)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v2)))/6. + (Power(rlog(Abs(1 - (v2 - v5)/v4)),2)*
                     (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v2)))/2.) - hvs(-1 + v5/v2)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v4)))/6. + (Power(rlog(Abs(v2)) - rlog(Abs(v5)),2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v4)))/2.) - hvs(-1 + v4/v2)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(v2)) - rlog(Abs(v4)),2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v5)))/2.)) + 
            hvs(1 + (v2*(v2 - v4 - v5))/(v4*v5))*
             (hvs(-v2)*hvs(v4)*hvs(v5)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5)))))/6. \
+ ((2*M_PI - M_PI*hvs(-((v2*(v2 - v4 - v5))/(v4*v5))))*
                     Power(rlog(Abs((v2*(v2 - v4 - v5))/(v4*v5))),2))/2.) \
- hvs((-v2 + v4)/v5)*((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v2)))/6. + 
                  (Power(rlog(Abs(1 - (v2 - v4)/v5)),2)*
                     (-(M_PI*hvs(-1 + (v2 - v4)/v5)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v2)))/2.) - 
               hvs((-v2 + v5)/v4)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v2)))/6. + (Power(rlog(Abs(1 - (v2 - v5)/v4)),2)*
                     (-(M_PI*hvs(-1 + (v2 - v5)/v4)) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v2)))/2.) - 
               hvs(-1 + v5/v2)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) \
- M_PI*sign(v4)))/6. + (Power(rlog(Abs(v2)) - rlog(Abs(v5)),2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v5)*
                        hvs(-v2 + v5)*hvs(-v4 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v4)))/2.) - 
               hvs(-1 + v4/v2)*
                ((Power(M_PI,2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(v2)) - rlog(Abs(v4)),2)*
                     (-(M_PI*hvs(-v2)) + M_PI*hvs(-v4) + 
                       2*M_PI*hvs(v2)*hvs(v4)*hvs(-v2 + v4)*hvs(v4 - v5)*
                        hvs(v5)*hvs(-v2 + v5)*hvs(-v2 + v4 + v5) - 
                       M_PI*sign(v5)))/2.))))/2. + 
       (3*((-(M_PI*hvs(-v3)) + M_PI*hvs(-v5))*
             (hvs((-v3 + v5)/v2)*rLi(2,-(((1 - v2/v5 - v3/v5)*v5)/v2)) + 
               hvs((v3 - v5)/v2)*
                (-Power(M_PI,2)/6. - rLi(2,-(v2/((1 - v2/v5 - v3/v5)*v5))) - 
                  Power(rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v2)),2)/2.)) + 
            (-(M_PI*hvs(-v2)) + M_PI*hvs(-v5))*
             (hvs((-v2 + v5)/v3)*rLi(2,-(((1 - v2/v5 - v3/v5)*v5)/v3)) + 
               hvs((v2 - v5)/v3)*
                (-Power(M_PI,2)/6. - rLi(2,-(v3/((1 - v2/v5 - v3/v5)*v5))) - 
                  Power(rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v3)),2)/2.)) + 
            hvs((v3 - v5)/v2)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
             rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v2))*
             (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
               M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v2) - M_PI*sign(v5)) + 
            hvs((v2 - v5)/v3)*(rlog(Abs(v2)) - rlog(Abs(v5)))*
             rlog(Abs(((1 - v2/v5 - v3/v5)*v5)/v3))*
             (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*hvs(v3 - v5)*
                hvs(v2 + v3 - v5)*hvs(v5) - 
               M_PI*hvs(((1 - v2/v5 - v3/v5)*v5)/v3) - M_PI*sign(v5)) + 
            hvs(-1 - (v5*(-v2 - v3 + v5))/(v2*v3))*
             ((Power(M_PI,2)*(-M_PI + 2*M_PI*hvs(-v5) - 
                    M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3))))/6. + 
               ((-M_PI + 2*M_PI*hvs(-(v2*v3))*hvs(-v5) - 
                    M_PI*hvs((v5*(-v2 - v3 + v5))/(v2*v3)))*
                  Power(rlog(Abs((v5*(-v2 - v3 + v5))/(v2*v3))),2))/2. - 
               hvs(-1 + v3/v5)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v2)))/6. + 
                  (Power(-rlog(Abs(v3)) + rlog(Abs(v5)),2)*
                     (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v2)))/2.) - 
               hvs(-1 + v2/v5)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v3)))/6. + 
                  (Power(-rlog(Abs(v2)) + rlog(Abs(v5)),2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs((v2 - v5)/v3)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(1 - (-v2 + v5)/v3)),2)*
                     (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/2.) - 
               hvs((v3 - v5)/v2)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(1 - (-v3 + v5)/v2)),2)*
                     (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/2.)) + 
            hvs(1 + (v5*(-v2 - v3 + v5))/(v2*v3))*
             (hvs(v2)*hvs(v3)*hvs(-v5)*
                ((Power(M_PI,2)*
                     (2*M_PI - M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3)))))/6. \
+ ((2*M_PI - M_PI*hvs(-((v5*(-v2 - v3 + v5))/(v2*v3))))*
                     Power(rlog(Abs((v5*(-v2 - v3 + v5))/(v2*v3))),2))/2.) \
- hvs(-1 + v3/v5)*((Power(M_PI,2)*
                     (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v2)))/6. + 
                  (Power(-rlog(Abs(v3)) + rlog(Abs(v5)),2)*
                     (M_PI*hvs(-v3) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v2)))/2.) - 
               hvs(-1 + v2/v5)*
                ((Power(M_PI,2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v3)))/6. + 
                  (Power(-rlog(Abs(v2)) + rlog(Abs(v5)),2)*
                     (M_PI*hvs(-v2) - M_PI*hvs(-v5) + 
                       2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*sign(v3)))/2.) - 
               hvs((v2 - v5)/v3)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(1 - (-v2 + v5)/v3)),2)*
                     (2*M_PI*hvs(v2)*hvs(v2 - v3)*hvs(v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v2 + v5)/v3) - M_PI*sign(v5)))/2.) - 
               hvs((v3 - v5)/v2)*
                ((Power(M_PI,2)*
                     (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/6. + 
                  (Power(rlog(Abs(1 - (-v3 + v5)/v2)),2)*
                     (2*M_PI*hvs(v2)*hvs(v3)*hvs(-v2 + v3)*hvs(v2 - v5)*
                        hvs(v3 - v5)*hvs(v2 + v3 - v5)*hvs(v5) - 
                       M_PI*hvs(-1 + (-v3 + v5)/v2) - M_PI*sign(v5)))/2.))))/2.))/
   (4.*v1*v2*(-v1 + v3 + v4)*v5*
     sqrt(-(Power(v2,2)*Power(-v1 + v3,2)) - Power(v3*v4 + (v1 - v4)*v5,2) - 
       2*v2*(-(Power(v3,2)*v4) + v1*(-v1 + v4)*v5 + v3*(v1*v5 + v4*(v1 + v5)))\
))
;
 return integrandRe50;
}  
