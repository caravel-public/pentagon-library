#include "../header.h"

double FAw3pt5Re(double v1, double v2, double v3, double v4, double v5);
double FAw3pt5Im(double v1, double v2, double v3, double v4, double v5);

double lowerweight26(int imag, void* p) {
    using namespace GiNaC;

    struct my_f_prm* prm = (struct my_f_prm*)p;
    double v1f = (prm->v1f);
    double v2f = (prm->v2f);
    double v3f = (prm->v3f);
    double v4f = (prm->v4f);
    double v5f = (prm->v5f);
    double v1 = -v1f;
    double v2 = -v2f;
    double v3 = -v3f;
    double v4 = -v4f;
    double v5 = -v5f;
    double zeta3 = 1.2020569031595942;

    double result26;
    if (imag == 0) {
        result26 = FAw3pt5Re(v1f, v2f, v3f, v4f, v5f);
    } else {
        result26 = FAw3pt5Im(v1f, v2f, v3f, v4f, v5f);
    }
    return result26;
}
