
#include "../header.h"  
double FAw3pt5Re(double v1,double v2,double v3,double v4, double v5);
double FAw3pt5Im(double v1,double v2,double v3,double v4, double v5);

double lowerweight7 (int imag, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;  
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double v1=-v1f; double v2=-v2f; double v3=-v3f; double v4=-v4f; double v5=-v5f;
   double zeta3=1.2020569031595942;

   double result7;
    if(imag==0){
         result7 =  -(Power(M_PI,2)*hvs(-(v2/v4)))/6. - hvs(v2/v4)*rLi(2,1 - v2/v4) + hvs(-(v2/v4))*rLi(2,v2/v4) + hvs(-(v2/v4))*rlog(1 - v2/v4)*rlog(Abs(v2)) - hvs(-(v2/v4))*rlog(1 - v2/v4)*rlog(Abs(v4)) ;
    }else{
         result7 =  -(M_PI*hvs(-v2)*hvs(-(v2/v4))*rlog(1 - v2/v4)) + M_PI*hvs(-(v2/v4))*hvs(-v4)*rlog(1 - v2/v4) ;
    }
 return result7;
}  
