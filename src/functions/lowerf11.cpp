
#include "../header.h"  
double FAw3pt5Re(double v1,double v2,double v3,double v4, double v5);
double FAw3pt5Im(double v1,double v2,double v3,double v4, double v5);

double lowerweight11 (int imag, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;  
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double v1=-v1f; double v2=-v2f; double v3=-v3f; double v4=-v4f; double v5=-v5f;
   double zeta3=1.2020569031595942;

   double result11;
    if(imag==0){
         result11 =  -(hvs(v1/v3)*rLi(3,1 - v1/v3)) - hvs(-(v1/v3))*rLi(3,v3/(-v1 + v3)) - (Power(M_PI,2)*hvs(-(v1/v3))*rlog(Abs(v3)))/6. + (Power(M_PI,2)*Power(hvs(v1 - v3),2)*hvs(-(v1/v3))*rlog(Abs(v3)))/2. - Power(M_PI,2)*hvs(v1 - v3)*hvs(-(v1/v3))*hvs(-v3)*rlog(Abs(v3)) + (Power(M_PI,2)*hvs(-(v1/v3))*Power(hvs(-v3),2)*rlog(Abs(v3)))/2. - (hvs(-(v1/v3))*Power(rlog(Abs(v3)),3))/6. + (Power(M_PI,2)*hvs(-(v1/v3))*rlog(Abs(-v1 + v3)))/6. - (Power(M_PI,2)*Power(hvs(v1 - v3),2)*hvs(-(v1/v3))*rlog(Abs(-v1 + v3)))/2. + Power(M_PI,2)*hvs(v1 - v3)*hvs(-(v1/v3))*hvs(-v3)*rlog(Abs(-v1 + v3)) - (Power(M_PI,2)*hvs(-(v1/v3))*Power(hvs(-v3),2)*rlog(Abs(-v1 + v3)))/2. + (hvs(-(v1/v3))*Power(rlog(Abs(v3)),2)*rlog(Abs(-v1 + v3)))/2. - (hvs(-(v1/v3))*rlog(Abs(v3))*Power(rlog(Abs(-v1 + v3)),2))/2. + (hvs(-(v1/v3))*Power(rlog(Abs(-v1 + v3)),3))/6. + Power(M_PI,2)*hvs(v1 - v3)*hvs(-(v1/v3))*rlog(Abs(v3))*sign(v3) - Power(M_PI,2)*hvs(-(v1/v3))*hvs(-v3)*rlog(Abs(v3))*sign(v3) - Power(M_PI,2)*hvs(v1 - v3)*hvs(-(v1/v3))*rlog(Abs(-v1 + v3))*sign(v3) + Power(M_PI,2)*hvs(-(v1/v3))*hvs(-v3)*rlog(Abs(-v1 + v3))*sign(v3) + (Power(M_PI,2)*hvs(-(v1/v3))*rlog(Abs(v3))*Power(sign(v3),2))/2. - (Power(M_PI,2)*hvs(-(v1/v3))*rlog(Abs(-v1 + v3))*Power(sign(v3),2))/2. ;
    }else{
         result11 =  -(Power(M_PI,3)*hvs(v1 - v3)*hvs(-(v1/v3)))/6. + (Power(M_PI,3)*Power(hvs(v1 - v3),3)*hvs(-(v1/v3)))/6. + (Power(M_PI,3)*hvs(-(v1/v3))*hvs(-v3))/6. - (Power(M_PI,3)*Power(hvs(v1 - v3),2)*hvs(-(v1/v3))*hvs(-v3))/2. + (Power(M_PI,3)*hvs(v1 - v3)*hvs(-(v1/v3))*Power(hvs(-v3),2))/2. - (Power(M_PI,3)*hvs(-(v1/v3))*Power(hvs(-v3),3))/6. - (M_PI*hvs(v1 - v3)*hvs(-(v1/v3))*Power(rlog(Abs(v3)),2))/2. + (M_PI*hvs(-(v1/v3))*hvs(-v3)*Power(rlog(Abs(v3)),2))/2. + M_PI*hvs(v1 - v3)*hvs(-(v1/v3))*rlog(Abs(v3))*rlog(Abs(-v1 + v3)) - M_PI*hvs(-(v1/v3))*hvs(-v3)*rlog(Abs(v3))*rlog(Abs(-v1 + v3)) - (M_PI*hvs(v1 - v3)*hvs(-(v1/v3))*Power(rlog(Abs(-v1 + v3)),2))/2. + (M_PI*hvs(-(v1/v3))*hvs(-v3)*Power(rlog(Abs(-v1 + v3)),2))/2. - (Power(M_PI,3)*hvs(-(v1/v3))*sign(v3))/6. + (Power(M_PI,3)*Power(hvs(v1 - v3),2)*hvs(-(v1/v3))*sign(v3))/2. - Power(M_PI,3)*hvs(v1 - v3)*hvs(-(v1/v3))*hvs(-v3)*sign(v3) + (Power(M_PI,3)*hvs(-(v1/v3))*Power(hvs(-v3),2)*sign(v3))/2. - (M_PI*hvs(-(v1/v3))*Power(rlog(Abs(v3)),2)*sign(v3))/2. + M_PI*hvs(-(v1/v3))*rlog(Abs(v3))*rlog(Abs(-v1 + v3))*sign(v3) - (M_PI*hvs(-(v1/v3))*Power(rlog(Abs(-v1 + v3)),2)*sign(v3))/2. + (Power(M_PI,3)*hvs(v1 - v3)*hvs(-(v1/v3))*Power(sign(v3),2))/2. - (Power(M_PI,3)*hvs(-(v1/v3))*hvs(-v3)*Power(sign(v3),2))/2. + (Power(M_PI,3)*hvs(-(v1/v3))*Power(sign(v3),3))/6. ;
    }
 return result11;
}  
