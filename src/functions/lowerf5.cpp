
#include "../header.h"  
double FAw3pt5Re(double v1,double v2,double v3,double v4, double v5);
double FAw3pt5Im(double v1,double v2,double v3,double v4, double v5);

double lowerweight5 (int imag, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;  
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double v1=-v1f; double v2=-v2f; double v3=-v3f; double v4=-v4f; double v5=-v5f;
   double zeta3=1.2020569031595942;

   double result5;
    if(imag==0){
         result5 =  rlog(Abs(v5)) ;
    }else{
         result5 =  -(M_PI*hvs(-v5)) ;
    }
 return result5;
}  
