
#include "../header.h"  
double FAw3pt5Re(double v1,double v2,double v3,double v4, double v5);
double FAw3pt5Im(double v1,double v2,double v3,double v4, double v5);

double lowerweight6 (int imag, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;  
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double v1=-v1f; double v2=-v2f; double v3=-v3f; double v4=-v4f; double v5=-v5f;
   double zeta3=1.2020569031595942;

   double result6;
    if(imag==0){
         result6 =  -(Power(M_PI,2)*hvs(-(v1/v3)))/6. - hvs(v1/v3)*rLi(2,1 - v1/v3) + hvs(-(v1/v3))*rLi(2,v1/v3) + hvs(-(v1/v3))*rlog(1 - v1/v3)*rlog(Abs(v1)) - hvs(-(v1/v3))*rlog(1 - v1/v3)*rlog(Abs(v3)) ;
    }else{
         result6 =  -(M_PI*hvs(-v1)*hvs(-(v1/v3))*rlog(1 - v1/v3)) + M_PI*hvs(-(v1/v3))*hvs(-v3)*rlog(1 - v1/v3) ;
    }
 return result6;
}  
