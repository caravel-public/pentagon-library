
#include "../header.h"


double integrandIm7 (double t, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double bcphi5Re(0.);
   double bcphi5Im(0.);
   double v1i, v2i, v3i, v4i, v5i, val;

   double d37s3 =9.03301789873836469269;
   double d38s3 = 22.0377098912592295221202221000567;
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=0;}
 if((v1f>0)&&(v2f<0)&&(v3f>0)&&(v4f>0)&&(v5f<0)){val=1;}
 if((v1f<0)&&(v2f>0)&&(v3f<0)&&(v4f>0)&&(v5f>0)){val=2;}
 if((v1f>0)&&(v2f<0)&&(v3f>0)&&(v4f<0)&&(v5f>0)){val=3;}
 if((v1f>0)&&(v2f>0)&&(v3f<0)&&(v4f>0)&&(v5f<0)){val=4;}
 if((v1f<0)&&(v2f>0)&&(v3f>0)&&(v4f<0)&&(v5f>0)){val=5;}
 if((v1f>0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=6;}
 if((v1f<0)&&(v2f>0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=7;}
 if((v1f<0)&&(v2f<0)&&(v3f>0)&&(v4f<0)&&(v5f<0)){val=8;}
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f>0)&&(v5f<0)){val=9;}
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f>0)){val=10;}

if(val==0){ v1i = -1.; v2i = -1.; v3i = -1.; v4i = -1.; v5i = -1.;
      bcphi5Re = 9.0330178987383646926900837570428; bcphi5Im = 0;};
if(val==1){ v1i = 1.; v2i = -1./3.; v3i = 1./3.; v4i = 1./3.; v5i = -1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==2){ v1i = -1./3.; v2i = 1.; v3i = -1./3.; v4i = 1./3.; v5i = 1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==3){ v1i = 1./3.; v2i = -1./3.; v3i = 1.; v4i = -1./3.; v5i = 1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==4){ v1i = 1./3.; v2i = 1./3.; v3i = -1./3.; v4i = 1.; v5i = -1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==5){ v1i = -1./3.; v2i = 1./3.; v3i = 1./3.; v4i = -1./3.; v5i = 1.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==6){ v1i = 1./3.; v2i = -1./3.; v3i = -1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==7){ v1i = -1./3.; v2i = 1./3.; v3i = -1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==8){ v1i = -1./3.; v2i = -1./3.; v3i = 1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==9){ v1i = -1./3.; v2i = -1./3.; v3i = -1./3.; v4i = 1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==10){ v1i = -1./3.; v2i = -1./3.; v3i = -1./3.; v4i = -1./3.; v5i = 1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};

   double v1=-v1i*(1.-t)-v1f*t;
   double v2=-v2i*(1.-t)-v2f*t;
   double v3=-v3i*(1.-t)-v3f*t;
   double v4=-v4i*(1.-t)-v4f*t;
   double v5=-v5i*(1.-t)-v5f*t;

   double zeta3=1.2020569031595942;
   
   double integrandIm7=  (Power(M_PI,3)*v2f*hvs(-v2)*hvs(-(v4/v2)))/(6.*v2) - 
  (Power(M_PI,3)*v2i*hvs(-v2)*hvs(-(v4/v2)))/(6.*v2) + 
  (Power(M_PI,3)*v2f*hvs(-v2)*hvs(-(v4/v2)))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2i*hvs(-v2)*hvs(-(v4/v2)))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4f*hvs(-v2)*hvs(-(v4/v2)))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v4i*hvs(-v2)*hvs(-(v4/v2)))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2f*Power(hvs(-v2),3)*hvs(-(v4/v2)))/(6.*v2) + 
  (Power(M_PI,3)*v2i*Power(hvs(-v2),3)*hvs(-(v4/v2)))/(6.*v2) - 
  (Power(M_PI,3)*v2f*Power(hvs(-v2),3)*hvs(-(v4/v2)))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v2i*Power(hvs(-v2),3)*hvs(-(v4/v2)))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v4f*Power(hvs(-v2),3)*hvs(-(v4/v2)))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4i*Power(hvs(-v2),3)*hvs(-(v4/v2)))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*hvs(-v2 + v4))/(6.*v2) + 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*hvs(-v2 + v4))/(6.*v2) - 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*hvs(-v2 + v4))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*hvs(-v2 + v4))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v4f*hvs(-(v4/v2))*hvs(-v2 + v4))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4i*hvs(-(v4/v2))*hvs(-v2 + v4))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v2f*Power(hvs(-v2),2)*hvs(-(v4/v2))*hvs(-v2 + v4))/(2.*v2) - 
  (Power(M_PI,3)*v2i*Power(hvs(-v2),2)*hvs(-(v4/v2))*hvs(-v2 + v4))/(2.*v2) + 
  (Power(M_PI,3)*v2f*Power(hvs(-v2),2)*hvs(-(v4/v2))*hvs(-v2 + v4))/
   (2.*(-v2 + v4)) - (Power(M_PI,3)*v2i*Power(hvs(-v2),2)*hvs(-(v4/v2))*
     hvs(-v2 + v4))/(2.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4f*Power(hvs(-v2),2)*hvs(-(v4/v2))*hvs(-v2 + v4))/
   (2.*(-v2 + v4)) + (Power(M_PI,3)*v4i*Power(hvs(-v2),2)*hvs(-(v4/v2))*
     hvs(-v2 + v4))/(2.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2f*hvs(-v2)*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2))/(2.*v2) + 
  (Power(M_PI,3)*v2i*hvs(-v2)*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2))/(2.*v2) - 
  (Power(M_PI,3)*v2f*hvs(-v2)*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2))/
   (2.*(-v2 + v4)) + (Power(M_PI,3)*v2i*hvs(-v2)*hvs(-(v4/v2))*
     Power(hvs(-v2 + v4),2))/(2.*(-v2 + v4)) + 
  (Power(M_PI,3)*v4f*hvs(-v2)*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2))/
   (2.*(-v2 + v4)) - (Power(M_PI,3)*v4i*hvs(-v2)*hvs(-(v4/v2))*
     Power(hvs(-v2 + v4),2))/(2.*(-v2 + v4)) + 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*Power(hvs(-v2 + v4),3))/(6.*v2) - 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*Power(hvs(-v2 + v4),3))/(6.*v2) + 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*Power(hvs(-v2 + v4),3))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*Power(hvs(-v2 + v4),3))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4f*hvs(-(v4/v2))*Power(hvs(-v2 + v4),3))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v4i*hvs(-(v4/v2))*Power(hvs(-v2 + v4),3))/(6.*(-v2 + v4)) + 
  (M_PI*v2f*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2))/(2.*v2) - 
  (M_PI*v2i*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2))/(2.*v2) + 
  (M_PI*v2f*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2))/(2.*(-v2 + v4)) - 
  (M_PI*v2i*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2))/(2.*(-v2 + v4)) - 
  (M_PI*v4f*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2))/(2.*(-v2 + v4)) + 
  (M_PI*v4i*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2))/(2.*(-v2 + v4)) - 
  (M_PI*v2f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(rlog(Abs(v2)),2))/(2.*v2) + 
  (M_PI*v2i*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(rlog(Abs(v2)),2))/(2.*v2) - 
  (M_PI*v2f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(rlog(Abs(v2)),2))/
   (2.*(-v2 + v4)) + (M_PI*v2i*hvs(-(v4/v2))*hvs(-v2 + v4)*
     Power(rlog(Abs(v2)),2))/(2.*(-v2 + v4)) + 
  (M_PI*v4f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(rlog(Abs(v2)),2))/
   (2.*(-v2 + v4)) - (M_PI*v4i*hvs(-(v4/v2))*hvs(-v2 + v4)*
     Power(rlog(Abs(v2)),2))/(2.*(-v2 + v4)) - 
  (M_PI*v2f*hvs(-v2)*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4)))/v2 + 
  (M_PI*v2i*hvs(-v2)*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4)))/v2 - 
  (M_PI*v2f*hvs(-v2)*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4)))/
   (-v2 + v4) + (M_PI*v2i*hvs(-v2)*hvs(-(v4/v2))*rlog(Abs(v2))*
     rlog(Abs(v2 - v4)))/(-v2 + v4) + 
  (M_PI*v4f*hvs(-v2)*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4)))/
   (-v2 + v4) - (M_PI*v4i*hvs(-v2)*hvs(-(v4/v2))*rlog(Abs(v2))*
     rlog(Abs(v2 - v4)))/(-v2 + v4) + 
  (M_PI*v2f*hvs(-(v4/v2))*hvs(-v2 + v4)*rlog(Abs(v2))*rlog(Abs(v2 - v4)))/v2 - 
  (M_PI*v2i*hvs(-(v4/v2))*hvs(-v2 + v4)*rlog(Abs(v2))*rlog(Abs(v2 - v4)))/v2 + 
  (M_PI*v2f*hvs(-(v4/v2))*hvs(-v2 + v4)*rlog(Abs(v2))*rlog(Abs(v2 - v4)))/
   (-v2 + v4) - (M_PI*v2i*hvs(-(v4/v2))*hvs(-v2 + v4)*rlog(Abs(v2))*
     rlog(Abs(v2 - v4)))/(-v2 + v4) - 
  (M_PI*v4f*hvs(-(v4/v2))*hvs(-v2 + v4)*rlog(Abs(v2))*rlog(Abs(v2 - v4)))/
   (-v2 + v4) + (M_PI*v4i*hvs(-(v4/v2))*hvs(-v2 + v4)*rlog(Abs(v2))*
     rlog(Abs(v2 - v4)))/(-v2 + v4) + 
  (M_PI*v2f*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2))/(2.*v2) - 
  (M_PI*v2i*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2))/(2.*v2) + 
  (M_PI*v2f*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2))/
   (2.*(-v2 + v4)) - (M_PI*v2i*hvs(-v2)*hvs(-(v4/v2))*
     Power(rlog(Abs(v2 - v4)),2))/(2.*(-v2 + v4)) - 
  (M_PI*v4f*hvs(-v2)*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2))/
   (2.*(-v2 + v4)) + (M_PI*v4i*hvs(-v2)*hvs(-(v4/v2))*
     Power(rlog(Abs(v2 - v4)),2))/(2.*(-v2 + v4)) - 
  (M_PI*v2f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(rlog(Abs(v2 - v4)),2))/(2.*v2) + 
  (M_PI*v2i*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(rlog(Abs(v2 - v4)),2))/(2.*v2) - 
  (M_PI*v2f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(rlog(Abs(v2 - v4)),2))/
   (2.*(-v2 + v4)) + (M_PI*v2i*hvs(-(v4/v2))*hvs(-v2 + v4)*
     Power(rlog(Abs(v2 - v4)),2))/(2.*(-v2 + v4)) + 
  (M_PI*v4f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(rlog(Abs(v2 - v4)),2))/
   (2.*(-v2 + v4)) - (M_PI*v4i*hvs(-(v4/v2))*hvs(-v2 + v4)*
     Power(rlog(Abs(v2 - v4)),2))/(2.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*sign(v2))/(6.*v2) + 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*sign(v2))/(6.*v2) - 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*sign(v2))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*sign(v2))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v4f*hvs(-(v4/v2))*sign(v2))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4i*hvs(-(v4/v2))*sign(v2))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v2f*Power(hvs(-v2),2)*hvs(-(v4/v2))*sign(v2))/(2.*v2) - 
  (Power(M_PI,3)*v2i*Power(hvs(-v2),2)*hvs(-(v4/v2))*sign(v2))/(2.*v2) + 
  (Power(M_PI,3)*v2f*Power(hvs(-v2),2)*hvs(-(v4/v2))*sign(v2))/
   (2.*(-v2 + v4)) - (Power(M_PI,3)*v2i*Power(hvs(-v2),2)*hvs(-(v4/v2))*
     sign(v2))/(2.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4f*Power(hvs(-v2),2)*hvs(-(v4/v2))*sign(v2))/
   (2.*(-v2 + v4)) + (Power(M_PI,3)*v4i*Power(hvs(-v2),2)*hvs(-(v4/v2))*
     sign(v2))/(2.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2f*hvs(-v2)*hvs(-(v4/v2))*hvs(-v2 + v4)*sign(v2))/v2 + 
  (Power(M_PI,3)*v2i*hvs(-v2)*hvs(-(v4/v2))*hvs(-v2 + v4)*sign(v2))/v2 - 
  (Power(M_PI,3)*v2f*hvs(-v2)*hvs(-(v4/v2))*hvs(-v2 + v4)*sign(v2))/
   (-v2 + v4) + (Power(M_PI,3)*v2i*hvs(-v2)*hvs(-(v4/v2))*hvs(-v2 + v4)*
     sign(v2))/(-v2 + v4) + (Power(M_PI,3)*v4f*hvs(-v2)*hvs(-(v4/v2))*
     hvs(-v2 + v4)*sign(v2))/(-v2 + v4) - 
  (Power(M_PI,3)*v4i*hvs(-v2)*hvs(-(v4/v2))*hvs(-v2 + v4)*sign(v2))/
   (-v2 + v4) + (Power(M_PI,3)*v2f*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2)*
     sign(v2))/(2.*v2) - (Power(M_PI,3)*v2i*hvs(-(v4/v2))*
     Power(hvs(-v2 + v4),2)*sign(v2))/(2.*v2) + 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2)*sign(v2))/
   (2.*(-v2 + v4)) - (Power(M_PI,3)*v2i*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2)*
     sign(v2))/(2.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4f*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2)*sign(v2))/
   (2.*(-v2 + v4)) + (Power(M_PI,3)*v4i*hvs(-(v4/v2))*Power(hvs(-v2 + v4),2)*
     sign(v2))/(2.*(-v2 + v4)) - 
  (M_PI*v2f*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2)*sign(v2))/(2.*v2) + 
  (M_PI*v2i*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2)*sign(v2))/(2.*v2) - 
  (M_PI*v2f*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2)*sign(v2))/(2.*(-v2 + v4)) + 
  (M_PI*v2i*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2)*sign(v2))/(2.*(-v2 + v4)) + 
  (M_PI*v4f*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2)*sign(v2))/(2.*(-v2 + v4)) - 
  (M_PI*v4i*hvs(-(v4/v2))*Power(rlog(Abs(v2)),2)*sign(v2))/(2.*(-v2 + v4)) + 
  (M_PI*v2f*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4))*sign(v2))/v2 - 
  (M_PI*v2i*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4))*sign(v2))/v2 + 
  (M_PI*v2f*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4))*sign(v2))/
   (-v2 + v4) - (M_PI*v2i*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4))*
     sign(v2))/(-v2 + v4) - (M_PI*v4f*hvs(-(v4/v2))*rlog(Abs(v2))*
     rlog(Abs(v2 - v4))*sign(v2))/(-v2 + v4) + 
  (M_PI*v4i*hvs(-(v4/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v4))*sign(v2))/
   (-v2 + v4) - (M_PI*v2f*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2)*sign(v2))/
   (2.*v2) + (M_PI*v2i*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2)*sign(v2))/
   (2.*v2) - (M_PI*v2f*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2)*sign(v2))/
   (2.*(-v2 + v4)) + (M_PI*v2i*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2)*
     sign(v2))/(2.*(-v2 + v4)) + 
  (M_PI*v4f*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2)*sign(v2))/
   (2.*(-v2 + v4)) - (M_PI*v4i*hvs(-(v4/v2))*Power(rlog(Abs(v2 - v4)),2)*
     sign(v2))/(2.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2f*hvs(-v2)*hvs(-(v4/v2))*Power(sign(v2),2))/(2.*v2) + 
  (Power(M_PI,3)*v2i*hvs(-v2)*hvs(-(v4/v2))*Power(sign(v2),2))/(2.*v2) - 
  (Power(M_PI,3)*v2f*hvs(-v2)*hvs(-(v4/v2))*Power(sign(v2),2))/
   (2.*(-v2 + v4)) + (Power(M_PI,3)*v2i*hvs(-v2)*hvs(-(v4/v2))*
     Power(sign(v2),2))/(2.*(-v2 + v4)) + 
  (Power(M_PI,3)*v4f*hvs(-v2)*hvs(-(v4/v2))*Power(sign(v2),2))/
   (2.*(-v2 + v4)) - (Power(M_PI,3)*v4i*hvs(-v2)*hvs(-(v4/v2))*
     Power(sign(v2),2))/(2.*(-v2 + v4)) + 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(sign(v2),2))/(2.*v2) - 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(sign(v2),2))/(2.*v2) + 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(sign(v2),2))/
   (2.*(-v2 + v4)) - (Power(M_PI,3)*v2i*hvs(-(v4/v2))*hvs(-v2 + v4)*
     Power(sign(v2),2))/(2.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4f*hvs(-(v4/v2))*hvs(-v2 + v4)*Power(sign(v2),2))/
   (2.*(-v2 + v4)) + (Power(M_PI,3)*v4i*hvs(-(v4/v2))*hvs(-v2 + v4)*
     Power(sign(v2),2))/(2.*(-v2 + v4)) + 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*Power(sign(v2),3))/(6.*v2) - 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*Power(sign(v2),3))/(6.*v2) + 
  (Power(M_PI,3)*v2f*hvs(-(v4/v2))*Power(sign(v2),3))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v2i*hvs(-(v4/v2))*Power(sign(v2),3))/(6.*(-v2 + v4)) - 
  (Power(M_PI,3)*v4f*hvs(-(v4/v2))*Power(sign(v2),3))/(6.*(-v2 + v4)) + 
  (Power(M_PI,3)*v4i*hvs(-(v4/v2))*Power(sign(v2),3))/(6.*(-v2 + v4))
;
 return integrandIm7;
}  
