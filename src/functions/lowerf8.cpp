
#include "../header.h"  
double FAw3pt5Re(double v1,double v2,double v3,double v4, double v5);
double FAw3pt5Im(double v1,double v2,double v3,double v4, double v5);

double lowerweight8 (int imag, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;  
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double v1=-v1f; double v2=-v2f; double v3=-v3f; double v4=-v4f; double v5=-v5f;
   double zeta3=1.2020569031595942;

   double result8;
    if(imag==0){
         result8 =  -(Power(M_PI,2)*hvs(-(v3/v5)))/6. - hvs(v3/v5)*rLi(2,1 - v3/v5) + hvs(-(v3/v5))*rLi(2,v3/v5) + hvs(-(v3/v5))*rlog(1 - v3/v5)*rlog(Abs(v3)) - hvs(-(v3/v5))*rlog(1 - v3/v5)*rlog(Abs(v5)) ;
    }else{
         result8 =  -(M_PI*hvs(-v3)*hvs(-(v3/v5))*rlog(1 - v3/v5)) + M_PI*hvs(-(v3/v5))*hvs(-v5)*rlog(1 - v3/v5) ;
    }
 return result8;
}  
