
#include "../header.h"


double integrandEu20 (double t, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double bcphi5Re(0.);
   double bcphi5Im(0.);
   double v1i, v2i, v3i, v4i, v5i, val;

   double d37s3 =9.03301789873836469269;
   double d38s3 = 22.0377098912592295221202221000567;
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=0;}
 if((v1f>0)&&(v2f<0)&&(v3f>0)&&(v4f>0)&&(v5f<0)){val=1;}
 if((v1f<0)&&(v2f>0)&&(v3f<0)&&(v4f>0)&&(v5f>0)){val=2;}
 if((v1f>0)&&(v2f<0)&&(v3f>0)&&(v4f<0)&&(v5f>0)){val=3;}
 if((v1f>0)&&(v2f>0)&&(v3f<0)&&(v4f>0)&&(v5f<0)){val=4;}
 if((v1f<0)&&(v2f>0)&&(v3f>0)&&(v4f<0)&&(v5f>0)){val=5;}
 if((v1f>0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=6;}
 if((v1f<0)&&(v2f>0)&&(v3f<0)&&(v4f<0)&&(v5f<0)){val=7;}
 if((v1f<0)&&(v2f<0)&&(v3f>0)&&(v4f<0)&&(v5f<0)){val=8;}
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f>0)&&(v5f<0)){val=9;}
 if((v1f<0)&&(v2f<0)&&(v3f<0)&&(v4f<0)&&(v5f>0)){val=10;}

if(val==0){ v1i = -1.; v2i = -1.; v3i = -1.; v4i = -1.; v5i = -1.;
      bcphi5Re = 9.0330178987383646926900837570428; bcphi5Im = 0;};
if(val==1){ v1i = 1.; v2i = -1./3.; v3i = 1./3.; v4i = 1./3.; v5i = -1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==2){ v1i = -1./3.; v2i = 1.; v3i = -1./3.; v4i = 1./3.; v5i = 1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==3){ v1i = 1./3.; v2i = -1./3.; v3i = 1.; v4i = -1./3.; v5i = 1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==4){ v1i = 1./3.; v2i = 1./3.; v3i = -1./3.; v4i = 1.; v5i = -1./3.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==5){ v1i = -1./3.; v2i = 1./3.; v3i = 1./3.; v4i = -1./3.; v5i = 1.;
      bcphi5Re = 3.1885330945191912022096403234172612; bcphi5Im = -4.9904004202455238825564265425782650709;};
if(val==6){ v1i = 1./3.; v2i = -1./3.; v3i = -1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==7){ v1i = -1./3.; v2i = 1./3.; v3i = -1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==8){ v1i = -1./3.; v2i = -1./3.; v3i = 1./3.; v4i = -1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==9){ v1i = -1./3.; v2i = -1./3.; v3i = -1./3.; v4i = 1./3.; v5i = -1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};
if(val==10){ v1i = -1./3.; v2i = -1./3.; v3i = -1./3.; v4i = -1./3.; v5i = 1./3.;
      bcphi5Re = -9.565599283557574494807340670377016067; bcphi5Im = 3.4451418533666466892384505626978;};

   double v1=-v1i*(1.-t)-v1f*t;
   double v2=-v2i*(1.-t)-v2f*t;
   double v3=-v3i*(1.-t)-v3f*t;
   double v4=-v4i*(1.-t)-v4f*t;
   double v5=-v5i*(1.-t)-v5f*t;

   double zeta3=1.2020569031595942;
   
   double integrandEu20=  ((v1f - v1i - v3f + v3i)*(Power(rlog(Abs(v1)),3) - 
       3*Power(rlog(Abs(v1)),2)*rlog(Abs(v3)) + 
       3*rlog(Abs(v1))*Power(rlog(Abs(v3)),2) - Power(rlog(Abs(v3)),3)))/
   (-v1 + v3) - ((v1f - v1i - v3f + v3i)*
     (3*Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v1)) - 
       3*Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v3)) - 
       3*(2*Power(M_PI,2)*hvs(-v1)*hvs(-v3)*rlog(Abs(v1)) + 
          Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v3))) + 
       3*(Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v1)) + 
          2*Power(M_PI,2)*hvs(-v1)*hvs(-v3)*rlog(Abs(v3)))))/(-v1 + v3) + 
  ((v3f - v3i - v5f + v5i)*(-Power(rlog(Abs(v3)),3) + 
       3*Power(rlog(Abs(v3)),2)*rlog(Abs(v5)) - 
       3*rlog(Abs(v3))*Power(rlog(Abs(v5)),2) + Power(rlog(Abs(v5)),3)))/
   (-v3 + v5) - ((v3f - v3i - v5f + v5i)*
     (-3*Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v3)) + 
       3*Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v5)) + 
       3*(2*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v3)) + 
          Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v5))) - 
       3*(Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v3)) + 
          2*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v5)))))/(-v3 + v5) + 
  ((-v3f + v3i)*(-6*zeta3 + Power(M_PI,2)*rlog(Abs(v1)) - 
       2*Power(rlog(Abs(v1)),3) + 
       6*(hvs(v3/v1)*rLi(3,1 - v3/v1) + 
          hvs(-(v3/v1))*(rLi(3,v1/(v1 - v3)) + 
             (Power(M_PI,2)*(rlog(Abs(v1)) - rlog(Abs(v1 - v3))))/6. + 
             Power(rlog(Abs(v1)) - rlog(Abs(v1 - v3)),3)/6.)) - 
       2*Power(M_PI,2)*rlog(Abs(v3)) + 
       6*Power(rlog(Abs(v1)),2)*rlog(Abs(v3)) - 
       6*rlog(Abs(v1))*Power(rlog(Abs(v3)),2) + 4*Power(rlog(Abs(v3)),3) + 
       6*(hvs(v1/v3)*rLi(3,1 - v1/v3) + 
          hvs(-(v1/v3))*(rLi(3,v3/(-v1 + v3)) + 
             (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(-v1 + v3))))/6. + 
             Power(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)),3)/6.)) + 
       6*(hvs(v5/v3)*rLi(3,1 - v5/v3) + 
          hvs(-(v5/v3))*(rLi(3,v3/(v3 - v5)) + 
             (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v3 - v5))))/6. + 
             Power(rlog(Abs(v3)) - rlog(Abs(v3 - v5)),3)/6.)) + 
       Power(M_PI,2)*rlog(Abs(v5)) - 6*Power(rlog(Abs(v3)),2)*rlog(Abs(v5)) + 
       6*rlog(Abs(v3))*Power(rlog(Abs(v5)),2) - 2*Power(rlog(Abs(v5)),3) + 
       6*(hvs(v3/v5)*rLi(3,1 - v3/v5) + 
          hvs(-(v3/v5))*(rLi(3,v5/(-v3 + v5)) + 
             (Power(M_PI,2)*(rlog(Abs(v5)) - rlog(Abs(-v3 + v5))))/6. + 
             Power(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)),3)/6.)) - 
       6*(3*zeta3 - rLi(3,v1/v3) - rLi(3,v5/v3) - 
          rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) - 
          rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
          hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (rLi(3,v1/v3) - hvs(1 - v1/v3)*rLi(3,v1/v3) - 
             hvs((v3 - v5)/v1)*rLi(3,1 - (v3 - v5)/v1) - 
             hvs((-v1 + v3)/v5)*rLi(3,1 - (-v1 + v3)/v5) + rLi(3,v5/v3) - 
             hvs(1 - v5/v3)*rLi(3,v5/v3) + 
             rLi(3,-((v1*v5)/(v3*(-v1 + v3 - v5)))) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) - 
             hvs(-1 + v1/v3)*(rLi(3,v3/v1) + 
                (Power(M_PI,2)*(-rlog(Abs(v1)) + rlog(Abs(v3))))/6. + 
                Power(-rlog(Abs(v1)) + rlog(Abs(v3)),3)/6.) - 
             hvs((-v3 + v5)/v1)*
              (rLi(3,-(v1/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (v3 - v5)/v1)))/6. - 
                Power(rlog(Abs(1 - (v3 - v5)/v1)),3)/6.) - 
             hvs((v1 - v3)/v5)*
              (rLi(3,-(v5/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (-v1 + v3)/v5)))/6. - 
                Power(rlog(Abs(1 - (-v1 + v3)/v5)),3)/6.) - 
             (Power(M_PI,2)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/6. - 
             Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),3)/6. - 
             hvs(-1 + v5/v3)*(rLi(3,v3/v5) + 
                (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v5))))/6. + 
                Power(rlog(Abs(v3)) - rlog(Abs(v5)),3)/6.)) + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (rLi(3,v1/v3) - hvs(1 - v1/v3)*rLi(3,v1/v3) - 
             hvs((v3 - v5)/v1)*rLi(3,1 - (v3 - v5)/v1) - 
             hvs((-v1 + v3)/v5)*rLi(3,1 - (-v1 + v3)/v5) + 
             rLi(3,-((v3*(-v1 + v3 - v5))/(v1*v5))) + rLi(3,v5/v3) - 
             hvs(1 - v5/v3)*rLi(3,v5/v3) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) - 
             hvs(-1 + v1/v3)*(rLi(3,v3/v1) + 
                (Power(M_PI,2)*(-rlog(Abs(v1)) + rlog(Abs(v3))))/6. + 
                Power(-rlog(Abs(v1)) + rlog(Abs(v3)),3)/6.) - 
             hvs((-v3 + v5)/v1)*
              (rLi(3,-(v1/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (v3 - v5)/v1)))/6. - 
                Power(rlog(Abs(1 - (v3 - v5)/v1)),3)/6.) - 
             hvs((v1 - v3)/v5)*
              (rLi(3,-(v5/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (-v1 + v3)/v5)))/6. - 
                Power(rlog(Abs(1 - (-v1 + v3)/v5)),3)/6.) + 
             hvs(v1)*hvs(-v3)*hvs(v5)*
              (-rLi(3,-((v3*(-v1 + v3 - v5))/(v1*v5))) + 
                rLi(3,-((v1*v5)/(v3*(-v1 + v3 - v5)))) - 
                (Power(M_PI,2)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/
                 6. - Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),3)/6.) \
- hvs(-1 + v5/v3)*(rLi(3,v3/v5) + 
                (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v5))))/6. + 
                Power(rlog(Abs(v3)) - rlog(Abs(v5)),3)/6.)) + 
          (-rlog(Abs(v3)) + rlog(Abs(v5)))*
           (hvs((v3 - v5)/v1)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             hvs((-v3 + v5)/v1)*
              (-Power(M_PI,2)/6. - rLi(2,-(v1/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1)),2)/2.)) + 
          (rlog(Abs(v1)) - rlog(Abs(v3)))*
           (hvs((-v1 + v3)/v5)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
             hvs((v1 - v3)/v5)*
              (-Power(M_PI,2)/6. - rLi(2,-(v5/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5)),2)/2.)))))/v3 + 
  ((-v1f + v1i + v3f - v3i - v5f + v5i)*
     (6*zeta3 - Power(M_PI,2)*rlog(Abs(v1)) - Power(rlog(Abs(v1)),3) + 
       6*(hvs(v3/v1)*rLi(3,1 - v3/v1) + 
          hvs(-(v3/v1))*(rLi(3,v1/(v1 - v3)) + 
             (Power(M_PI,2)*(rlog(Abs(v1)) - rlog(Abs(v1 - v3))))/6. + 
             Power(rlog(Abs(v1)) - rlog(Abs(v1 - v3)),3)/6.)) + 
       2*Power(M_PI,2)*rlog(Abs(v3)) + 
       6*rlog(Abs(v1))*Power(rlog(Abs(v3)),2) - 4*Power(rlog(Abs(v3)),3) + 
       6*(hvs(v1/v3)*rLi(3,1 - v1/v3) + 
          hvs(-(v1/v3))*(rLi(3,v3/(-v1 + v3)) + 
             (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(-v1 + v3))))/6. + 
             Power(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)),3)/6.)) + 
       6*(hvs(v5/v3)*rLi(3,1 - v5/v3) + 
          hvs(-(v5/v3))*(rLi(3,v3/(v3 - v5)) + 
             (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v3 - v5))))/6. + 
             Power(rlog(Abs(v3)) - rlog(Abs(v3 - v5)),3)/6.)) - 
       Power(M_PI,2)*rlog(Abs(v5)) + 3*Power(rlog(Abs(v1)),2)*rlog(Abs(v5)) - 
       12*rlog(Abs(v1))*rlog(Abs(v3))*rlog(Abs(v5)) + 
       6*Power(rlog(Abs(v3)),2)*rlog(Abs(v5)) + 
       3*rlog(Abs(v1))*Power(rlog(Abs(v5)),2) - Power(rlog(Abs(v5)),3) + 
       6*(hvs(v3/v5)*rLi(3,1 - v3/v5) + 
          hvs(-(v3/v5))*(rLi(3,v5/(-v3 + v5)) + 
             (Power(M_PI,2)*(rlog(Abs(v5)) - rlog(Abs(-v3 + v5))))/6. + 
             Power(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)),3)/6.)) - 
       6*(3*zeta3 - rLi(3,v1/v3) - rLi(3,v5/v3) - 
          rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) - 
          rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
          hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (rLi(3,v1/v3) - hvs(1 - v1/v3)*rLi(3,v1/v3) - 
             hvs((v3 - v5)/v1)*rLi(3,1 - (v3 - v5)/v1) - 
             hvs((-v1 + v3)/v5)*rLi(3,1 - (-v1 + v3)/v5) + rLi(3,v5/v3) - 
             hvs(1 - v5/v3)*rLi(3,v5/v3) + 
             rLi(3,-((v1*v5)/(v3*(-v1 + v3 - v5)))) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) - 
             hvs(-1 + v1/v3)*(rLi(3,v3/v1) + 
                (Power(M_PI,2)*(-rlog(Abs(v1)) + rlog(Abs(v3))))/6. + 
                Power(-rlog(Abs(v1)) + rlog(Abs(v3)),3)/6.) - 
             hvs((-v3 + v5)/v1)*
              (rLi(3,-(v1/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (v3 - v5)/v1)))/6. - 
                Power(rlog(Abs(1 - (v3 - v5)/v1)),3)/6.) - 
             hvs((v1 - v3)/v5)*
              (rLi(3,-(v5/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (-v1 + v3)/v5)))/6. - 
                Power(rlog(Abs(1 - (-v1 + v3)/v5)),3)/6.) - 
             (Power(M_PI,2)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/6. - 
             Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),3)/6. - 
             hvs(-1 + v5/v3)*(rLi(3,v3/v5) + 
                (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v5))))/6. + 
                Power(rlog(Abs(v3)) - rlog(Abs(v5)),3)/6.)) + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (rLi(3,v1/v3) - hvs(1 - v1/v3)*rLi(3,v1/v3) - 
             hvs((v3 - v5)/v1)*rLi(3,1 - (v3 - v5)/v1) - 
             hvs((-v1 + v3)/v5)*rLi(3,1 - (-v1 + v3)/v5) + 
             rLi(3,-((v3*(-v1 + v3 - v5))/(v1*v5))) + rLi(3,v5/v3) - 
             hvs(1 - v5/v3)*rLi(3,v5/v3) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) - 
             hvs(-1 + v1/v3)*(rLi(3,v3/v1) + 
                (Power(M_PI,2)*(-rlog(Abs(v1)) + rlog(Abs(v3))))/6. + 
                Power(-rlog(Abs(v1)) + rlog(Abs(v3)),3)/6.) - 
             hvs((-v3 + v5)/v1)*
              (rLi(3,-(v1/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (v3 - v5)/v1)))/6. - 
                Power(rlog(Abs(1 - (v3 - v5)/v1)),3)/6.) - 
             hvs((v1 - v3)/v5)*
              (rLi(3,-(v5/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (-v1 + v3)/v5)))/6. - 
                Power(rlog(Abs(1 - (-v1 + v3)/v5)),3)/6.) + 
             hvs(v1)*hvs(-v3)*hvs(v5)*
              (-rLi(3,-((v3*(-v1 + v3 - v5))/(v1*v5))) + 
                rLi(3,-((v1*v5)/(v3*(-v1 + v3 - v5)))) - 
                (Power(M_PI,2)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/
                 6. - Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),3)/6.) \
- hvs(-1 + v5/v3)*(rLi(3,v3/v5) + 
                (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v5))))/6. + 
                Power(rlog(Abs(v3)) - rlog(Abs(v5)),3)/6.)) + 
          (-rlog(Abs(v3)) + rlog(Abs(v5)))*
           (hvs((v3 - v5)/v1)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             hvs((-v3 + v5)/v1)*
              (-Power(M_PI,2)/6. - rLi(2,-(v1/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1)),2)/2.)) + 
          (rlog(Abs(v1)) - rlog(Abs(v3)))*
           (hvs((-v1 + v3)/v5)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
             hvs((v1 - v3)/v5)*
              (-Power(M_PI,2)/6. - rLi(2,-(v5/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5)),2)/2.)))))/
   (v1 - v3 + v5) + ((-v1f + v1i)*
     (Power(rlog(Abs(v1)),3) - 
       6*(hvs(v3/v1)*rLi(3,1 - v3/v1) + 
          hvs(-(v3/v1))*(rLi(3,v1/(v1 - v3)) + 
             (Power(M_PI,2)*(rlog(Abs(v1)) - rlog(Abs(v1 - v3))))/6. + 
             Power(rlog(Abs(v1)) - rlog(Abs(v1 - v3)),3)/6.)) - 
       3*Power(rlog(Abs(v1)),2)*rlog(Abs(v3)) + Power(rlog(Abs(v3)),3) - 
       6*(hvs(v1/v3)*rLi(3,1 - v1/v3) + 
          hvs(-(v1/v3))*(rLi(3,v3/(-v1 + v3)) + 
             (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(-v1 + v3))))/6. + 
             Power(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)),3)/6.)) - 
       6*(hvs(v5/v3)*rLi(3,1 - v5/v3) + 
          hvs(-(v5/v3))*(rLi(3,v3/(v3 - v5)) + 
             (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v3 - v5))))/6. + 
             Power(rlog(Abs(v3)) - rlog(Abs(v3 - v5)),3)/6.)) + 
       6*rlog(Abs(v1))*rlog(Abs(v3))*rlog(Abs(v5)) - 
       3*Power(rlog(Abs(v3)),2)*rlog(Abs(v5)) - 
       3*rlog(Abs(v1))*Power(rlog(Abs(v5)),2) + Power(rlog(Abs(v5)),3) - 
       6*(hvs(v3/v5)*rLi(3,1 - v3/v5) + 
          hvs(-(v3/v5))*(rLi(3,v5/(-v3 + v5)) + 
             (Power(M_PI,2)*(rlog(Abs(v5)) - rlog(Abs(-v3 + v5))))/6. + 
             Power(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)),3)/6.)) + 
       6*(3*zeta3 - rLi(3,v1/v3) - rLi(3,v5/v3) - 
          rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) - 
          rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
          hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (rLi(3,v1/v3) - hvs(1 - v1/v3)*rLi(3,v1/v3) - 
             hvs((v3 - v5)/v1)*rLi(3,1 - (v3 - v5)/v1) - 
             hvs((-v1 + v3)/v5)*rLi(3,1 - (-v1 + v3)/v5) + rLi(3,v5/v3) - 
             hvs(1 - v5/v3)*rLi(3,v5/v3) + 
             rLi(3,-((v1*v5)/(v3*(-v1 + v3 - v5)))) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) - 
             hvs(-1 + v1/v3)*(rLi(3,v3/v1) + 
                (Power(M_PI,2)*(-rlog(Abs(v1)) + rlog(Abs(v3))))/6. + 
                Power(-rlog(Abs(v1)) + rlog(Abs(v3)),3)/6.) - 
             hvs((-v3 + v5)/v1)*
              (rLi(3,-(v1/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (v3 - v5)/v1)))/6. - 
                Power(rlog(Abs(1 - (v3 - v5)/v1)),3)/6.) - 
             hvs((v1 - v3)/v5)*
              (rLi(3,-(v5/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (-v1 + v3)/v5)))/6. - 
                Power(rlog(Abs(1 - (-v1 + v3)/v5)),3)/6.) - 
             (Power(M_PI,2)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/6. - 
             Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),3)/6. - 
             hvs(-1 + v5/v3)*(rLi(3,v3/v5) + 
                (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v5))))/6. + 
                Power(rlog(Abs(v3)) - rlog(Abs(v5)),3)/6.)) + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (rLi(3,v1/v3) - hvs(1 - v1/v3)*rLi(3,v1/v3) - 
             hvs((v3 - v5)/v1)*rLi(3,1 - (v3 - v5)/v1) - 
             hvs((-v1 + v3)/v5)*rLi(3,1 - (-v1 + v3)/v5) + 
             rLi(3,-((v3*(-v1 + v3 - v5))/(v1*v5))) + rLi(3,v5/v3) - 
             hvs(1 - v5/v3)*rLi(3,v5/v3) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) - 
             hvs(-1 + v1/v3)*(rLi(3,v3/v1) + 
                (Power(M_PI,2)*(-rlog(Abs(v1)) + rlog(Abs(v3))))/6. + 
                Power(-rlog(Abs(v1)) + rlog(Abs(v3)),3)/6.) - 
             hvs((-v3 + v5)/v1)*
              (rLi(3,-(v1/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (v3 - v5)/v1)))/6. - 
                Power(rlog(Abs(1 - (v3 - v5)/v1)),3)/6.) - 
             hvs((v1 - v3)/v5)*
              (rLi(3,-(v5/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (-v1 + v3)/v5)))/6. - 
                Power(rlog(Abs(1 - (-v1 + v3)/v5)),3)/6.) + 
             hvs(v1)*hvs(-v3)*hvs(v5)*
              (-rLi(3,-((v3*(-v1 + v3 - v5))/(v1*v5))) + 
                rLi(3,-((v1*v5)/(v3*(-v1 + v3 - v5)))) - 
                (Power(M_PI,2)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/
                 6. - Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),3)/6.) \
- hvs(-1 + v5/v3)*(rLi(3,v3/v5) + 
                (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v5))))/6. + 
                Power(rlog(Abs(v3)) - rlog(Abs(v5)),3)/6.)) + 
          (-rlog(Abs(v3)) + rlog(Abs(v5)))*
           (hvs((v3 - v5)/v1)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             hvs((-v3 + v5)/v1)*
              (-Power(M_PI,2)/6. - rLi(2,-(v1/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1)),2)/2.)) + 
          (rlog(Abs(v1)) - rlog(Abs(v3)))*
           (hvs((-v1 + v3)/v5)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
             hvs((v1 - v3)/v5)*
              (-Power(M_PI,2)/6. - rLi(2,-(v5/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5)),2)/2.)))))/v1 + 
  ((-v5f + v5i)*(Power(rlog(Abs(v1)),3) - 
       6*(hvs(v3/v1)*rLi(3,1 - v3/v1) + 
          hvs(-(v3/v1))*(rLi(3,v1/(v1 - v3)) + 
             (Power(M_PI,2)*(rlog(Abs(v1)) - rlog(Abs(v1 - v3))))/6. + 
             Power(rlog(Abs(v1)) - rlog(Abs(v1 - v3)),3)/6.)) - 
       3*rlog(Abs(v1))*Power(rlog(Abs(v3)),2) + Power(rlog(Abs(v3)),3) - 
       6*(hvs(v1/v3)*rLi(3,1 - v1/v3) + 
          hvs(-(v1/v3))*(rLi(3,v3/(-v1 + v3)) + 
             (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(-v1 + v3))))/6. + 
             Power(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)),3)/6.)) - 
       6*(hvs(v5/v3)*rLi(3,1 - v5/v3) + 
          hvs(-(v5/v3))*(rLi(3,v3/(v3 - v5)) + 
             (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v3 - v5))))/6. + 
             Power(rlog(Abs(v3)) - rlog(Abs(v3 - v5)),3)/6.)) - 
       3*Power(rlog(Abs(v1)),2)*rlog(Abs(v5)) + 
       6*rlog(Abs(v1))*rlog(Abs(v3))*rlog(Abs(v5)) - 
       3*rlog(Abs(v3))*Power(rlog(Abs(v5)),2) + Power(rlog(Abs(v5)),3) - 
       6*(hvs(v3/v5)*rLi(3,1 - v3/v5) + 
          hvs(-(v3/v5))*(rLi(3,v5/(-v3 + v5)) + 
             (Power(M_PI,2)*(rlog(Abs(v5)) - rlog(Abs(-v3 + v5))))/6. + 
             Power(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)),3)/6.)) + 
       6*(3*zeta3 - rLi(3,v1/v3) - rLi(3,v5/v3) - 
          rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) - 
          rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
          hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (rLi(3,v1/v3) - hvs(1 - v1/v3)*rLi(3,v1/v3) - 
             hvs((v3 - v5)/v1)*rLi(3,1 - (v3 - v5)/v1) - 
             hvs((-v1 + v3)/v5)*rLi(3,1 - (-v1 + v3)/v5) + rLi(3,v5/v3) - 
             hvs(1 - v5/v3)*rLi(3,v5/v3) + 
             rLi(3,-((v1*v5)/(v3*(-v1 + v3 - v5)))) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) - 
             hvs(-1 + v1/v3)*(rLi(3,v3/v1) + 
                (Power(M_PI,2)*(-rlog(Abs(v1)) + rlog(Abs(v3))))/6. + 
                Power(-rlog(Abs(v1)) + rlog(Abs(v3)),3)/6.) - 
             hvs((-v3 + v5)/v1)*
              (rLi(3,-(v1/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (v3 - v5)/v1)))/6. - 
                Power(rlog(Abs(1 - (v3 - v5)/v1)),3)/6.) - 
             hvs((v1 - v3)/v5)*
              (rLi(3,-(v5/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (-v1 + v3)/v5)))/6. - 
                Power(rlog(Abs(1 - (-v1 + v3)/v5)),3)/6.) - 
             (Power(M_PI,2)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/6. - 
             Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),3)/6. - 
             hvs(-1 + v5/v3)*(rLi(3,v3/v5) + 
                (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v5))))/6. + 
                Power(rlog(Abs(v3)) - rlog(Abs(v5)),3)/6.)) + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (rLi(3,v1/v3) - hvs(1 - v1/v3)*rLi(3,v1/v3) - 
             hvs((v3 - v5)/v1)*rLi(3,1 - (v3 - v5)/v1) - 
             hvs((-v1 + v3)/v5)*rLi(3,1 - (-v1 + v3)/v5) + 
             rLi(3,-((v3*(-v1 + v3 - v5))/(v1*v5))) + rLi(3,v5/v3) - 
             hvs(1 - v5/v3)*rLi(3,v5/v3) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             rLi(3,-((v3*(1 - v1/v3 - v5/v3))/v5)) - 
             hvs(-1 + v1/v3)*(rLi(3,v3/v1) + 
                (Power(M_PI,2)*(-rlog(Abs(v1)) + rlog(Abs(v3))))/6. + 
                Power(-rlog(Abs(v1)) + rlog(Abs(v3)),3)/6.) - 
             hvs((-v3 + v5)/v1)*
              (rLi(3,-(v1/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (v3 - v5)/v1)))/6. - 
                Power(rlog(Abs(1 - (v3 - v5)/v1)),3)/6.) - 
             hvs((v1 - v3)/v5)*
              (rLi(3,-(v5/(-v1 + v3 - v5))) - 
                (Power(M_PI,2)*rlog(Abs(1 - (-v1 + v3)/v5)))/6. - 
                Power(rlog(Abs(1 - (-v1 + v3)/v5)),3)/6.) + 
             hvs(v1)*hvs(-v3)*hvs(v5)*
              (-rLi(3,-((v3*(-v1 + v3 - v5))/(v1*v5))) + 
                rLi(3,-((v1*v5)/(v3*(-v1 + v3 - v5)))) - 
                (Power(M_PI,2)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/
                 6. - Power(rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))),3)/6.) \
- hvs(-1 + v5/v3)*(rLi(3,v3/v5) + 
                (Power(M_PI,2)*(rlog(Abs(v3)) - rlog(Abs(v5))))/6. + 
                Power(rlog(Abs(v3)) - rlog(Abs(v5)),3)/6.)) + 
          (-rlog(Abs(v3)) + rlog(Abs(v5)))*
           (hvs((v3 - v5)/v1)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v1)) + 
             hvs((-v3 + v5)/v1)*
              (-Power(M_PI,2)/6. - rLi(2,-(v1/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1)),2)/2.)) + 
          (rlog(Abs(v1)) - rlog(Abs(v3)))*
           (hvs((-v1 + v3)/v5)*rLi(2,-((v3*(1 - v1/v3 - v5/v3))/v5)) + 
             hvs((v1 - v3)/v5)*
              (-Power(M_PI,2)/6. - rLi(2,-(v5/(v3*(1 - v1/v3 - v5/v3)))) - 
                Power(rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5)),2)/2.)))))/v5 - 
  ((-v1f + v1i + v3f - v3i - v5f + v5i)*
     (-3*Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v1)) - 
       12*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v1)) - 
       12*Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v3)) + 
       6*(Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v1)) + 
          2*Power(M_PI,2)*hvs(-v1)*hvs(-v3)*rlog(Abs(v3))) - 
       3*Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v5)) + 
       3*(2*Power(M_PI,2)*hvs(-v1)*hvs(-v5)*rlog(Abs(v1)) + 
          Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v5))) + 
       12*M_PI*hvs(-v1)*(-(M_PI*hvs(-v5)*rlog(Abs(v3))) - 
          M_PI*hvs(-v3)*rlog(Abs(v5))) + 
       6*(2*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v3)) + 
          Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v5))) + 
       3*(Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v1)) + 
          2*Power(M_PI,2)*hvs(-v1)*hvs(-v5)*rlog(Abs(v5))) + 
       3*hvs(-(v3/v1))*(rlog(Abs(v1)) - rlog(Abs(v1 - v3)))*
        Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1),2) + 
       3*hvs(-(v1/v3))*(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)))*
        Power(M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3),2) + 
       3*hvs(-(v5/v3))*(rlog(Abs(v3)) - rlog(Abs(v3 - v5)))*
        Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3),2) + 
       3*hvs(-(v3/v5))*(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)))*
        Power(M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),2) - 
       6*((M_PI*hvs(-v3) - M_PI*hvs(-v5))*hvs((-v3 + v5)/v1)*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*hvs(-v1 + v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3)) - 
          (hvs((-v3 + v5)/v1)*(-rlog(Abs(v3)) + rlog(Abs(v5)))*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3),2))/2. + 
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs((v1 - v3)/v5)*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3)) - 
          (hvs((v1 - v3)/v5)*(rlog(Abs(v1)) - rlog(Abs(v3)))*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3),2))/2. + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (-(hvs(v1)*hvs(-v3)*
                 Power(2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))),2)*
                 hvs(v5)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/2. - 
             (hvs(-1 + v5/v3)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),2))/2. + 
             (hvs((v1 - v3)/v5)*rlog(Abs(1 - (-v1 + v3)/v5))*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 2))/2. + (hvs((-v3 + v5)/v1)*
                rlog(Abs(1 - (v3 - v5)/v1))*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),2))/2. - 
             (hvs(-1 + v1/v3)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),
                 2))/2.) + hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (-(Power(-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                   2*M_PI*hvs(-v3)*hvs(-(v1*v5)),2)*
                 rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/2. - 
             (hvs(-1 + v5/v3)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),2))/2. + 
             (hvs((v1 - v3)/v5)*rlog(Abs(1 - (-v1 + v3)/v5))*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 2))/2. + (hvs((-v3 + v5)/v1)*rlog(Abs(1 - (v3 - v5)/v1))*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),2))/2. - 
             (hvs(-1 + v1/v3)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                   hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),2))/2.))))/
   (v1 - v3 + v5) - ((-v3f + v3i)*
     (-6*Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v1)) + 
       12*Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v3)) + 
       6*(2*Power(M_PI,2)*hvs(-v1)*hvs(-v3)*rlog(Abs(v1)) + 
          Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v3))) - 
       6*(Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v1)) + 
          2*Power(M_PI,2)*hvs(-v1)*hvs(-v3)*rlog(Abs(v3))) - 
       6*Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v5)) - 
       6*(2*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v3)) + 
          Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v5))) + 
       6*(Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v3)) + 
          2*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v5))) + 
       3*hvs(-(v3/v1))*(rlog(Abs(v1)) - rlog(Abs(v1 - v3)))*
        Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1),2) + 
       3*hvs(-(v1/v3))*(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)))*
        Power(M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3),2) + 
       3*hvs(-(v5/v3))*(rlog(Abs(v3)) - rlog(Abs(v3 - v5)))*
        Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3),2) + 
       3*hvs(-(v3/v5))*(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)))*
        Power(M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),2) - 
       6*((M_PI*hvs(-v3) - M_PI*hvs(-v5))*hvs((-v3 + v5)/v1)*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*hvs(-v1 + v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3)) - 
          (hvs((-v3 + v5)/v1)*(-rlog(Abs(v3)) + rlog(Abs(v5)))*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3),2))/2. + 
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs((v1 - v3)/v5)*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3)) - 
          (hvs((v1 - v3)/v5)*(rlog(Abs(v1)) - rlog(Abs(v3)))*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3),2))/2. + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (-(hvs(v1)*hvs(-v3)*
                 Power(2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))),2)*
                 hvs(v5)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/2. - 
             (hvs(-1 + v5/v3)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),2))/2. + 
             (hvs((v1 - v3)/v5)*rlog(Abs(1 - (-v1 + v3)/v5))*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 2))/2. + (hvs((-v3 + v5)/v1)*
                rlog(Abs(1 - (v3 - v5)/v1))*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),2))/2. - 
             (hvs(-1 + v1/v3)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),
                 2))/2.) + hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (-(Power(-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                   2*M_PI*hvs(-v3)*hvs(-(v1*v5)),2)*
                 rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/2. - 
             (hvs(-1 + v5/v3)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),2))/2. + 
             (hvs((v1 - v3)/v5)*rlog(Abs(1 - (-v1 + v3)/v5))*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 2))/2. + (hvs((-v3 + v5)/v1)*rlog(Abs(1 - (v3 - v5)/v1))*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),2))/2. - 
             (hvs(-1 + v1/v3)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                   hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),2))/2.))))/
   v3 - ((-v1f + v1i)*(3*Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v1)) + 
       6*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v1)) + 
       3*Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v3)) - 
       3*(2*Power(M_PI,2)*hvs(-v1)*hvs(-v3)*rlog(Abs(v1)) + 
          Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v3))) + 
       3*Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v5)) - 
       6*M_PI*hvs(-v1)*(-(M_PI*hvs(-v5)*rlog(Abs(v3))) - 
          M_PI*hvs(-v3)*rlog(Abs(v5))) - 
       3*(2*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v3)) + 
          Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v5))) - 
       3*(Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v1)) + 
          2*Power(M_PI,2)*hvs(-v1)*hvs(-v5)*rlog(Abs(v5))) - 
       3*hvs(-(v3/v1))*(rlog(Abs(v1)) - rlog(Abs(v1 - v3)))*
        Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1),2) - 
       3*hvs(-(v1/v3))*(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)))*
        Power(M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3),2) - 
       3*hvs(-(v5/v3))*(rlog(Abs(v3)) - rlog(Abs(v3 - v5)))*
        Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3),2) - 
       3*hvs(-(v3/v5))*(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)))*
        Power(M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),2) + 
       6*((M_PI*hvs(-v3) - M_PI*hvs(-v5))*hvs((-v3 + v5)/v1)*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*hvs(-v1 + v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3)) - 
          (hvs((-v3 + v5)/v1)*(-rlog(Abs(v3)) + rlog(Abs(v5)))*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3),2))/2. + 
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs((v1 - v3)/v5)*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3)) - 
          (hvs((v1 - v3)/v5)*(rlog(Abs(v1)) - rlog(Abs(v3)))*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3),2))/2. + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (-(hvs(v1)*hvs(-v3)*
                 Power(2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))),2)*
                 hvs(v5)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/2. - 
             (hvs(-1 + v5/v3)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),2))/2. + 
             (hvs((v1 - v3)/v5)*rlog(Abs(1 - (-v1 + v3)/v5))*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 2))/2. + (hvs((-v3 + v5)/v1)*
                rlog(Abs(1 - (v3 - v5)/v1))*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),2))/2. - 
             (hvs(-1 + v1/v3)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),
                 2))/2.) + hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (-(Power(-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                   2*M_PI*hvs(-v3)*hvs(-(v1*v5)),2)*
                 rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/2. - 
             (hvs(-1 + v5/v3)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),2))/2. + 
             (hvs((v1 - v3)/v5)*rlog(Abs(1 - (-v1 + v3)/v5))*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 2))/2. + (hvs((-v3 + v5)/v1)*rlog(Abs(1 - (v3 - v5)/v1))*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),2))/2. - 
             (hvs(-1 + v1/v3)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                   hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),2))/2.))))/
   v1 - ((-v5f + v5i)*(3*Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v1)) + 
       6*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v1)) + 
       3*Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v3)) - 
       3*(Power(M_PI,2)*Power(hvs(-v3),2)*rlog(Abs(v1)) + 
          2*Power(M_PI,2)*hvs(-v1)*hvs(-v3)*rlog(Abs(v3))) + 
       3*Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v5)) - 
       3*(2*Power(M_PI,2)*hvs(-v1)*hvs(-v5)*rlog(Abs(v1)) + 
          Power(M_PI,2)*Power(hvs(-v1),2)*rlog(Abs(v5))) - 
       6*M_PI*hvs(-v1)*(-(M_PI*hvs(-v5)*rlog(Abs(v3))) - 
          M_PI*hvs(-v3)*rlog(Abs(v5))) - 
       3*(Power(M_PI,2)*Power(hvs(-v5),2)*rlog(Abs(v3)) + 
          2*Power(M_PI,2)*hvs(-v3)*hvs(-v5)*rlog(Abs(v5))) - 
       3*hvs(-(v3/v1))*(rlog(Abs(v1)) - rlog(Abs(v1 - v3)))*
        Power(-(M_PI*hvs(-v1)) + M_PI*hvs(-v1 + v3) + M_PI*sign(v1),2) - 
       3*hvs(-(v1/v3))*(rlog(Abs(v3)) - rlog(Abs(-v1 + v3)))*
        Power(M_PI*hvs(v1 - v3) - M_PI*hvs(-v3) + M_PI*sign(v3),2) - 
       3*hvs(-(v5/v3))*(rlog(Abs(v3)) - rlog(Abs(v3 - v5)))*
        Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v3 + v5) + M_PI*sign(v3),2) - 
       3*hvs(-(v3/v5))*(rlog(Abs(v5)) - rlog(Abs(-v3 + v5)))*
        Power(M_PI*hvs(v3 - v5) - M_PI*hvs(-v5) + M_PI*sign(v5),2) + 
       6*((M_PI*hvs(-v3) - M_PI*hvs(-v5))*hvs((-v3 + v5)/v1)*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v1))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*hvs(-v1 + v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3)) - 
          (hvs((-v3 + v5)/v1)*(-rlog(Abs(v3)) + rlog(Abs(v5)))*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v1) - M_PI*sign(v3),2))/2. + 
          (-(M_PI*hvs(-v1)) + M_PI*hvs(-v3))*hvs((v1 - v3)/v5)*
           rlog(Abs((v3*(1 - v1/v3 - v5/v3))/v5))*
           (2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
              hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
             M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3)) - 
          (hvs((v1 - v3)/v5)*(rlog(Abs(v1)) - rlog(Abs(v3)))*
             Power(2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
               M_PI*hvs((v3*(1 - v1/v3 - v5/v3))/v5) - M_PI*sign(v3),2))/2. + 
          hvs(1 + (v3*(-v1 + v3 - v5))/(v1*v5))*
           (-(hvs(v1)*hvs(-v3)*
                 Power(2*M_PI - M_PI*hvs(-((v3*(-v1 + v3 - v5))/(v1*v5))),2)*
                 hvs(v5)*rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/2. - 
             (hvs(-1 + v5/v3)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),2))/2. + 
             (hvs((v1 - v3)/v5)*rlog(Abs(1 - (-v1 + v3)/v5))*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*
                   hvs(v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),
                 2))/2. + (hvs((-v3 + v5)/v1)*rlog(Abs(1 - (v3 - v5)/v1))*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),2))/2. - 
             (hvs(-1 + v1/v3)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                   hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),2))/2.) + 
          hvs(-1 - (v3*(-v1 + v3 - v5))/(v1*v5))*
           (-(Power(-M_PI - M_PI*hvs((v3*(-v1 + v3 - v5))/(v1*v5)) + 
                   2*M_PI*hvs(-v3)*hvs(-(v1*v5)),2)*
                 rlog(Abs((v3*(-v1 + v3 - v5))/(v1*v5))))/2. - 
             (hvs(-1 + v5/v3)*(rlog(Abs(v3)) - rlog(Abs(v5)))*
                Power(-(M_PI*hvs(-v3)) + M_PI*hvs(-v5) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v1),2))/2. + 
             (hvs((v1 - v3)/v5)*rlog(Abs(1 - (-v1 + v3)/v5))*
                Power(-(M_PI*hvs(-1 + (-v1 + v3)/v5)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                   hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v3),2))/2. + 
             (hvs((-v3 + v5)/v1)*rlog(Abs(1 - (v3 - v5)/v1))*
                Power(-(M_PI*hvs(-1 + (v3 - v5)/v1)) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v5)*
                   hvs(-v1 + v5)*hvs(-v3 + v5)*hvs(v1 - v3 + v5) - 
                  M_PI*sign(v3),2))/2. - 
             (hvs(-1 + v1/v3)*(-rlog(Abs(v1)) + rlog(Abs(v3)))*
                Power(M_PI*hvs(-v1) - M_PI*hvs(-v3) + 
                  2*M_PI*hvs(v1)*hvs(v1 - v3)*hvs(v3)*hvs(v1 - v5)*hvs(v5)*
                   hvs(-v3 + v5)*hvs(v1 - v3 + v5) - M_PI*sign(v5),2))/2.))))/v5
;
 return integrandEu20;
}  
