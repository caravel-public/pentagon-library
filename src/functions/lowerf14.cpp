
#include "../header.h"  
double FAw3pt5Re(double v1,double v2,double v3,double v4, double v5);
double FAw3pt5Im(double v1,double v2,double v3,double v4, double v5);

double lowerweight14 (int imag, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;  
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double v1=-v1f; double v2=-v2f; double v3=-v3f; double v4=-v4f; double v5=-v5f;
   double zeta3=1.2020569031595942;

   double result14;
    if(imag==0){
         result14 =  -(hvs(-(v4/v1))*rLi(3,v1/(v1 - v4))) - hvs(v4/v1)*rLi(3,1 - v4/v1) - (Power(M_PI,2)*hvs(-(v4/v1))*rlog(Abs(v1)))/6. + (Power(M_PI,2)*Power(hvs(-v1),2)*hvs(-(v4/v1))*rlog(Abs(v1)))/2. - Power(M_PI,2)*hvs(-v1)*hvs(-(v4/v1))*hvs(-v1 + v4)*rlog(Abs(v1)) + (Power(M_PI,2)*hvs(-(v4/v1))*Power(hvs(-v1 + v4),2)*rlog(Abs(v1)))/2. - (hvs(-(v4/v1))*Power(rlog(Abs(v1)),3))/6. + (Power(M_PI,2)*hvs(-(v4/v1))*rlog(Abs(v1 - v4)))/6. - (Power(M_PI,2)*Power(hvs(-v1),2)*hvs(-(v4/v1))*rlog(Abs(v1 - v4)))/2. + Power(M_PI,2)*hvs(-v1)*hvs(-(v4/v1))*hvs(-v1 + v4)*rlog(Abs(v1 - v4)) - (Power(M_PI,2)*hvs(-(v4/v1))*Power(hvs(-v1 + v4),2)*rlog(Abs(v1 - v4)))/2. + (hvs(-(v4/v1))*Power(rlog(Abs(v1)),2)*rlog(Abs(v1 - v4)))/2. - (hvs(-(v4/v1))*rlog(Abs(v1))*Power(rlog(Abs(v1 - v4)),2))/2. + (hvs(-(v4/v1))*Power(rlog(Abs(v1 - v4)),3))/6. - Power(M_PI,2)*hvs(-v1)*hvs(-(v4/v1))*rlog(Abs(v1))*sign(v1) + Power(M_PI,2)*hvs(-(v4/v1))*hvs(-v1 + v4)*rlog(Abs(v1))*sign(v1) + Power(M_PI,2)*hvs(-v1)*hvs(-(v4/v1))*rlog(Abs(v1 - v4))*sign(v1) - Power(M_PI,2)*hvs(-(v4/v1))*hvs(-v1 + v4)*rlog(Abs(v1 - v4))*sign(v1) + (Power(M_PI,2)*hvs(-(v4/v1))*rlog(Abs(v1))*Power(sign(v1),2))/2. - (Power(M_PI,2)*hvs(-(v4/v1))*rlog(Abs(v1 - v4))*Power(sign(v1),2))/2. ;
    }else{
         result14 =  (Power(M_PI,3)*hvs(-v1)*hvs(-(v4/v1)))/6. - (Power(M_PI,3)*Power(hvs(-v1),3)*hvs(-(v4/v1)))/6. - (Power(M_PI,3)*hvs(-(v4/v1))*hvs(-v1 + v4))/6. + (Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-(v4/v1))*hvs(-v1 + v4))/2. - (Power(M_PI,3)*hvs(-v1)*hvs(-(v4/v1))*Power(hvs(-v1 + v4),2))/2. + (Power(M_PI,3)*hvs(-(v4/v1))*Power(hvs(-v1 + v4),3))/6. + (M_PI*hvs(-v1)*hvs(-(v4/v1))*Power(rlog(Abs(v1)),2))/2. - (M_PI*hvs(-(v4/v1))*hvs(-v1 + v4)*Power(rlog(Abs(v1)),2))/2. - M_PI*hvs(-v1)*hvs(-(v4/v1))*rlog(Abs(v1))*rlog(Abs(v1 - v4)) + M_PI*hvs(-(v4/v1))*hvs(-v1 + v4)*rlog(Abs(v1))*rlog(Abs(v1 - v4)) + (M_PI*hvs(-v1)*hvs(-(v4/v1))*Power(rlog(Abs(v1 - v4)),2))/2. - (M_PI*hvs(-(v4/v1))*hvs(-v1 + v4)*Power(rlog(Abs(v1 - v4)),2))/2. - (Power(M_PI,3)*hvs(-(v4/v1))*sign(v1))/6. + (Power(M_PI,3)*Power(hvs(-v1),2)*hvs(-(v4/v1))*sign(v1))/2. - Power(M_PI,3)*hvs(-v1)*hvs(-(v4/v1))*hvs(-v1 + v4)*sign(v1) + (Power(M_PI,3)*hvs(-(v4/v1))*Power(hvs(-v1 + v4),2)*sign(v1))/2. - (M_PI*hvs(-(v4/v1))*Power(rlog(Abs(v1)),2)*sign(v1))/2. + M_PI*hvs(-(v4/v1))*rlog(Abs(v1))*rlog(Abs(v1 - v4))*sign(v1) - (M_PI*hvs(-(v4/v1))*Power(rlog(Abs(v1 - v4)),2)*sign(v1))/2. - (Power(M_PI,3)*hvs(-v1)*hvs(-(v4/v1))*Power(sign(v1),2))/2. + (Power(M_PI,3)*hvs(-(v4/v1))*hvs(-v1 + v4)*Power(sign(v1),2))/2. + (Power(M_PI,3)*hvs(-(v4/v1))*Power(sign(v1),3))/6. ;
    }
 return result14;
}  
