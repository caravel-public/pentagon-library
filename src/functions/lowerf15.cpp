
#include "../header.h"  
double FAw3pt5Re(double v1,double v2,double v3,double v4, double v5);
double FAw3pt5Im(double v1,double v2,double v3,double v4, double v5);

double lowerweight15 (int imag, void * p) {
      using namespace GiNaC;

   struct my_f_prm * prm=(struct my_f_prm *)p;  
   double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);
   double v1=-v1f; double v2=-v2f; double v3=-v3f; double v4=-v4f; double v5=-v5f;
   double zeta3=1.2020569031595942;

   double result15;
    if(imag==0){
         result15 =  -(hvs(-(v5/v2))*rLi(3,v2/(v2 - v5))) - hvs(v5/v2)*rLi(3,1 - v5/v2) - (Power(M_PI,2)*hvs(-(v5/v2))*rlog(Abs(v2)))/6. + (Power(M_PI,2)*Power(hvs(-v2),2)*hvs(-(v5/v2))*rlog(Abs(v2)))/2. - Power(M_PI,2)*hvs(-v2)*hvs(-(v5/v2))*hvs(-v2 + v5)*rlog(Abs(v2)) + (Power(M_PI,2)*hvs(-(v5/v2))*Power(hvs(-v2 + v5),2)*rlog(Abs(v2)))/2. - (hvs(-(v5/v2))*Power(rlog(Abs(v2)),3))/6. + (Power(M_PI,2)*hvs(-(v5/v2))*rlog(Abs(v2 - v5)))/6. - (Power(M_PI,2)*Power(hvs(-v2),2)*hvs(-(v5/v2))*rlog(Abs(v2 - v5)))/2. + Power(M_PI,2)*hvs(-v2)*hvs(-(v5/v2))*hvs(-v2 + v5)*rlog(Abs(v2 - v5)) - (Power(M_PI,2)*hvs(-(v5/v2))*Power(hvs(-v2 + v5),2)*rlog(Abs(v2 - v5)))/2. + (hvs(-(v5/v2))*Power(rlog(Abs(v2)),2)*rlog(Abs(v2 - v5)))/2. - (hvs(-(v5/v2))*rlog(Abs(v2))*Power(rlog(Abs(v2 - v5)),2))/2. + (hvs(-(v5/v2))*Power(rlog(Abs(v2 - v5)),3))/6. - Power(M_PI,2)*hvs(-v2)*hvs(-(v5/v2))*rlog(Abs(v2))*sign(v2) + Power(M_PI,2)*hvs(-(v5/v2))*hvs(-v2 + v5)*rlog(Abs(v2))*sign(v2) + Power(M_PI,2)*hvs(-v2)*hvs(-(v5/v2))*rlog(Abs(v2 - v5))*sign(v2) - Power(M_PI,2)*hvs(-(v5/v2))*hvs(-v2 + v5)*rlog(Abs(v2 - v5))*sign(v2) + (Power(M_PI,2)*hvs(-(v5/v2))*rlog(Abs(v2))*Power(sign(v2),2))/2. - (Power(M_PI,2)*hvs(-(v5/v2))*rlog(Abs(v2 - v5))*Power(sign(v2),2))/2. ;
    }else{
         result15 =  (Power(M_PI,3)*hvs(-v2)*hvs(-(v5/v2)))/6. - (Power(M_PI,3)*Power(hvs(-v2),3)*hvs(-(v5/v2)))/6. - (Power(M_PI,3)*hvs(-(v5/v2))*hvs(-v2 + v5))/6. + (Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-(v5/v2))*hvs(-v2 + v5))/2. - (Power(M_PI,3)*hvs(-v2)*hvs(-(v5/v2))*Power(hvs(-v2 + v5),2))/2. + (Power(M_PI,3)*hvs(-(v5/v2))*Power(hvs(-v2 + v5),3))/6. + (M_PI*hvs(-v2)*hvs(-(v5/v2))*Power(rlog(Abs(v2)),2))/2. - (M_PI*hvs(-(v5/v2))*hvs(-v2 + v5)*Power(rlog(Abs(v2)),2))/2. - M_PI*hvs(-v2)*hvs(-(v5/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v5)) + M_PI*hvs(-(v5/v2))*hvs(-v2 + v5)*rlog(Abs(v2))*rlog(Abs(v2 - v5)) + (M_PI*hvs(-v2)*hvs(-(v5/v2))*Power(rlog(Abs(v2 - v5)),2))/2. - (M_PI*hvs(-(v5/v2))*hvs(-v2 + v5)*Power(rlog(Abs(v2 - v5)),2))/2. - (Power(M_PI,3)*hvs(-(v5/v2))*sign(v2))/6. + (Power(M_PI,3)*Power(hvs(-v2),2)*hvs(-(v5/v2))*sign(v2))/2. - Power(M_PI,3)*hvs(-v2)*hvs(-(v5/v2))*hvs(-v2 + v5)*sign(v2) + (Power(M_PI,3)*hvs(-(v5/v2))*Power(hvs(-v2 + v5),2)*sign(v2))/2. - (M_PI*hvs(-(v5/v2))*Power(rlog(Abs(v2)),2)*sign(v2))/2. + M_PI*hvs(-(v5/v2))*rlog(Abs(v2))*rlog(Abs(v2 - v5))*sign(v2) - (M_PI*hvs(-(v5/v2))*Power(rlog(Abs(v2 - v5)),2)*sign(v2))/2. - (Power(M_PI,3)*hvs(-v2)*hvs(-(v5/v2))*Power(sign(v2),2))/2. + (Power(M_PI,3)*hvs(-(v5/v2))*hvs(-v2 + v5)*Power(sign(v2),2))/2. + (Power(M_PI,3)*hvs(-(v5/v2))*Power(sign(v2),3))/6. ;
    }
 return result15;
}  
