#include <vector>
#include "header.h"


std::complex<double> integrate (int whichint, double s12, double s23, double s34, double s45, double s15, double err, size_t iwsalloc);
std::complex<double> evaluatelower (int whichint, double s12, double s23, double s34, double s45, double s15);


std::vector<std::complex<double> > evaluate_pentagonfunctions(double v[5], double error) { 

       double iwsall = 1e7;

       std::vector<std::complex<double> > eval_pen(82);

       eval_pen[0] = evaluatelower(1, v[0], v[1], v[2], v[3], v[4]);  // f[1,1,1]
       eval_pen[1] = evaluatelower(2, v[0], v[1], v[2], v[3], v[4]);  // f[1,1,2]
       eval_pen[2] = evaluatelower(3, v[0], v[1], v[2], v[3], v[4]);  // f[1,1,3]
       eval_pen[3] = evaluatelower(4, v[0], v[1], v[2], v[3], v[4]);  // f[1,1,4]
       eval_pen[4] = evaluatelower(5, v[0], v[1], v[2], v[3], v[4]);  // f[1,1,5]
       eval_pen[5] = evaluatelower(6, v[0], v[1], v[2], v[3], v[4]);  // f[2,1,1]
       eval_pen[6] = evaluatelower(7, v[0], v[1], v[2], v[3], v[4]);  // f[2,1,2]
       eval_pen[7] = evaluatelower(8, v[0], v[1], v[2], v[3], v[4]);  // f[2,1,3]
       eval_pen[8] = evaluatelower(9, v[0], v[1], v[2], v[3], v[4]);  // f[2,1,4]
       eval_pen[9] = evaluatelower(10, v[0], v[1], v[2], v[3], v[4]);  // f[2,1,5]
       eval_pen[10] = evaluatelower(11, v[0], v[1], v[2], v[3], v[4]);  // f[3,1,1]
       eval_pen[11] = evaluatelower(12, v[0], v[1], v[2], v[3], v[4]);  // f[3,1,2]
       eval_pen[12] = evaluatelower(13, v[0], v[1], v[2], v[3], v[4]);  // f[3,1,3]
       eval_pen[13] = evaluatelower(14, v[0], v[1], v[2], v[3], v[4]);  // f[3,1,4]
       eval_pen[14] = evaluatelower(15, v[0], v[1], v[2], v[3], v[4]);  // f[3,1,5]
       eval_pen[15] = evaluatelower(16, v[0], v[1], v[2], v[3], v[4]);  // f[3,2,1]
       eval_pen[16] = evaluatelower(17, v[0], v[1], v[2], v[3], v[4]);  // f[3,2,2]
       eval_pen[17] = evaluatelower(18, v[0], v[1], v[2], v[3], v[4]);  // f[3,2,3]
       eval_pen[18] = evaluatelower(19, v[0], v[1], v[2], v[3], v[4]);  // f[3,2,4]
       eval_pen[19] = evaluatelower(20, v[0], v[1], v[2], v[3], v[4]);  // f[3,2,5]
       eval_pen[20] = evaluatelower(21, v[0], v[1], v[2], v[3], v[4]);  // f[3,3,1]
       eval_pen[21] = evaluatelower(22, v[0], v[1], v[2], v[3], v[4]);  // f[3,3,2]
       eval_pen[22] = evaluatelower(23, v[0], v[1], v[2], v[3], v[4]);  // f[3,3,3]
       eval_pen[23] = evaluatelower(24, v[0], v[1], v[2], v[3], v[4]);  // f[3,3,4]
       eval_pen[24] = evaluatelower(25, v[0], v[1], v[2], v[3], v[4]);  // f[3,3,5]
       eval_pen[25] = evaluatelower(26, v[0], v[1], v[2], v[3], v[4]);  // f[3,4]
       eval_pen[26] = integrate(1, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,1,1]
       eval_pen[27] = integrate(2, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,1,2]
       eval_pen[28] = integrate(3, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,1,3]
       eval_pen[29] = integrate(4, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,1,4]
       eval_pen[30] = integrate(5, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,1,5]
       eval_pen[31] = integrate(6, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,2,1]
       eval_pen[32] = integrate(7, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,2,2]
       eval_pen[33] = integrate(8, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,2,3]
       eval_pen[34] = integrate(9, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,2,4]
       eval_pen[35] = integrate(10, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,2,5]
       eval_pen[36] = integrate(11, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,3,1]
       eval_pen[37] = integrate(12, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,3,2]
       eval_pen[38] = integrate(13, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,3,3]
       eval_pen[39] = integrate(14, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,3,4]
       eval_pen[40] = integrate(15, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,3,5]
       eval_pen[41] = integrate(16, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,4,1]
       eval_pen[42] = integrate(17, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,4,2]
       eval_pen[43] = integrate(18, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,4,3]
       eval_pen[44] = integrate(19, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,4,4]
       eval_pen[45] = integrate(20, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,4,5]
       eval_pen[46] = integrate(21, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,5,1]
       eval_pen[47] = integrate(22, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,5,2]
       eval_pen[48] = integrate(23, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,5,3]
       eval_pen[49] = integrate(24, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,5,4]
       eval_pen[50] = integrate(25, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,5,5]
       eval_pen[51] = integrate(26, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,6,1]
       eval_pen[52] = integrate(27, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,6,2]
       eval_pen[53] = integrate(28, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,6,3]
       eval_pen[54] = integrate(29, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,6,4]
       eval_pen[55] = integrate(30, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,6,5]
       eval_pen[56] = integrate(31, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,7,1]
       eval_pen[57] = integrate(32, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,7,2]
       eval_pen[58] = integrate(33, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,7,3]
       eval_pen[59] = integrate(34, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,7,4]
       eval_pen[60] = integrate(35, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,7,5]
       eval_pen[61] = integrate(36, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,8,1]
       eval_pen[62] = integrate(37, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,8,2]
       eval_pen[63] = integrate(38, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,8,3]
       eval_pen[64] = integrate(39, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,8,4]
       eval_pen[65] = integrate(40, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,8,5]
       eval_pen[66] = integrate(41, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,9,1]
       eval_pen[67] = integrate(42, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,9,2]
       eval_pen[68] = integrate(43, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,9,3]
       eval_pen[69] = integrate(44, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,9,4]
       eval_pen[70] = integrate(45, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,9,5]
       eval_pen[71] = integrate(46, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,10,1]
       eval_pen[72] = integrate(47, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,10,2]
       eval_pen[73] = integrate(48, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,10,3]
       eval_pen[74] = integrate(49, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,10,4]
       eval_pen[75] = integrate(50, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,10,5]
       eval_pen[76] = integrate(51, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,11,1]
       eval_pen[77] = integrate(52, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,11,2]
       eval_pen[78] = integrate(53, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,11,3]
       eval_pen[79] = integrate(54, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,11,4]
       eval_pen[80] = integrate(55, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,11,5]
       eval_pen[81] = integrate(56, v[0], v[1], v[2], v[3], v[4], error, iwsall);  // f[4,12]

        

       return eval_pen;
}

