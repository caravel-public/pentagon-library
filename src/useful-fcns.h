
struct my_f_prm { double v1f; double v2f; double v3f; double v4f; double v5f;};
inline double Power(double t, int n) {
   double Power = gsl_sf_pow_int(t,n); 
   return Power;
} 

inline std::complex<double> powCompl(std::complex<double> t, int n) {
   std::complex<double> Power = pow(t,n); 
   return Power;
} 

inline double SSqrt(double t) {
   double SSqrt = sqrt(std::abs(t)); 
   return SSqrt;
} 

inline double ArcTan(double t) {
   double ArcTan(0.);
//   if(t>1e-16){ ArcTan = atan(t);} 
   ArcTan = atan(t); 
   return ArcTan;
} 

inline double Abs(double t) {
  using namespace GiNaC;
   double Abs = ex_to<numeric>(abs(t)).to_double();  
   return Abs;
} 

inline double hvs(double t) { double hh=0;  if(t > 0) hh=1.;  return hh;}

inline double sign(double t) { double sg=-1.;  if(t > 0) sg=1.; if(t==0) sg =0; return sg;}

inline double rlog(double t) { 
  using namespace GiNaC;
   double rlog = 0 ; 
//   if (t>1e-16 && t<1.) rlog = ex_to<numeric>(log(t)).to_double();
//   if (t>1. && t<1e16) rlog = -ex_to<numeric>(log(1./t)).to_double();
   if (t>0. && t<1.) rlog = ex_to<numeric>(log(t)).to_double();
   if (t>1.) rlog = -ex_to<numeric>(log(1./t)).to_double();
//   if (t<0.) rlog = ex_to<numeric>(log(-t)).to_double();
   return rlog;
}

inline double newlog(double t) { 
  using namespace GiNaC;
   double newlog = 0 ; 
//   if (t>1e-5 && t<1.) newlog = ex_to<numeric>(log(t)).to_double();
//   if (t>1. && t<1e5) newlog = -ex_to<numeric>(log(1./t)).to_double();
   return newlog;
}

inline double rLi(int n, double t) { 
  using namespace GiNaC;
   double rLi = 0 ; 
   if(t<1.) rLi = ex_to<numeric>(Li(n,t)).to_double();
//   if (t<1. && (t> -1.||n> 3)) rLi = ex_to<numeric>(Li(n,t)).to_double();
//   if (t<1. && t< -1. && n==2) rLi = - ex_to<numeric>(Li(2,1./t)).to_double() - 1./2.*rlog(-1./t)*rlog(-1./t) - M_PI*M_PI/6.; 
//   if (t<1. && t< -1. && n==3) rLi = ex_to<numeric>(Li(3,1/t)).to_double() + 1./6*rlog(-1./t)*rlog(-1./t)*rlog(-1./t) + rlog(-1./t)*M_PI*M_PI/6.;
   return rLi;
}

