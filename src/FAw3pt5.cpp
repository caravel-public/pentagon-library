#include "header.h"
#include "phi5integrandIm.cpp"
#include "phi5integrandRe.cpp"
#include <vector>

using namespace std;
using namespace GiNaC;

typedef double (*func)(double, void*);
func pt5w4DLogRe2, pt5w4DLogIm2;

vector<double> ptstmp2;

double pt5w4DLogReTMP2(double t, void* p) {
    typedef double (*func)(double, void*);
    double pt5w4DLogReTMP2;
    struct my_f_prm* prm = (struct my_f_prm*)p;
    double v1f = (prm->v1f);
    double v2f = (prm->v2f);
    double v3f = (prm->v3f);
    double v4f = (prm->v4f);
    double v5f = (prm->v5f);

    int npts(ptstmp2.size());
    double pts[npts];
    for (int count = 0; count < npts; count++) { pts[count] = ptstmp2.at(count); }

    double x1(0.);
    double x2(0.);
    if (npts == 3) {
        x1 = pts[1];
        pt5w4DLogReTMP2 = (x1 * (*pt5w4DLogRe2)(x1 * t, p) + (1 - x1) * (*pt5w4DLogRe2)(x1 * t + (1. - t), p));
    };

    if (npts == 4) {
        x1 = pts[1];
        x2 = pts[2];
        pt5w4DLogReTMP2 = (x1 * (*pt5w4DLogRe2)(x1 * t, p) + (x2 - x1) / 2. * (*pt5w4DLogRe2)(x1 * t + (x1 + x2) / 2. * (1. - t), p) +
                           (x2 - x1) / 2. * (*pt5w4DLogRe2)(x2 * t + (x1 + x2) / 2. * (1. - t), p) + (1 - x2) * (*pt5w4DLogRe2)(x2 * t + (1. - t), p));
    };
    return pt5w4DLogReTMP2;
}

double FAw3pt5Re(double v1, double v2, double v3, double v4, double v5) {

    struct my_f_prm kinem = {v1, v2, v3, v4, v5};

    pt5w4DLogRe2 = &phi5integrandRe;
    double resultpt5w4DLogRe, errorpt5w4DLogRe;
    gsl_integration_workspace* wpt5w4DLogRe = gsl_integration_workspace_alloc(1e7);

    gsl_function Fpt5w4DLogRe;
    Fpt5w4DLogRe.function = pt5w4DLogRe2;
    Fpt5w4DLogRe.params = &kinem;

    if (v1 < 0 && v2 < 0 && v3 < 0 && v4 < 0 && v5 < 0) {
        double poleraw1 = 1. / (1. + v1 + v2 - v4);
        double poleraw2 = 1. / (1. + v2 + v3 - v5);
        double poleraw3 = 1. / (1. + v3 + v4 - v1);
        double poleraw4 = 1. / (1. + v4 + v5 - v2);
        double poleraw5 = 1. / (1. + v5 + v1 - v3);
        double polezero(0.);
        double poleone(1.);
        ptstmp2.push_back(polezero);
        ptstmp2.push_back(poleone);
        if ((poleraw1 > polezero) && (poleraw1 < poleone)) { ptstmp2.push_back(poleraw1); }
        if ((poleraw2 > polezero) && (poleraw2 < poleone)) { ptstmp2.push_back(poleraw2); }
        if ((poleraw3 > polezero) && (poleraw3 < poleone)) { ptstmp2.push_back(poleraw3); }
        if ((poleraw4 > polezero) && (poleraw4 < poleone)) { ptstmp2.push_back(poleraw4); }
        if ((poleraw5 > polezero) && (poleraw5 < poleone)) { ptstmp2.push_back(poleraw5); }
        sort(ptstmp2.begin(), ptstmp2.end());

        if (ptstmp2.size() == 3 || ptstmp2.size() == 4) { Fpt5w4DLogRe.function = &pt5w4DLogReTMP2; }

        gsl_integration_qags(&Fpt5w4DLogRe, 0., 1., 1e-9, 1e-9, 100., wpt5w4DLogRe, &resultpt5w4DLogRe, &errorpt5w4DLogRe);
    } else {
        gsl_integration_qags(&Fpt5w4DLogRe, 0., 1., 1e-9, 1e-9, 100., wpt5w4DLogRe, &resultpt5w4DLogRe, &errorpt5w4DLogRe);
    }

    gsl_integration_workspace_free(wpt5w4DLogRe);

    return resultpt5w4DLogRe;
}

double FAw3pt5Im(double v1, double v2, double v3, double v4, double v5) {
    using namespace std;
    using namespace GiNaC;

    if (v1 < 0 && v2 < 0 && v3 < 0 && v4 < 0 && v5 < 0) { return 0.; }

    struct my_f_prm kinem = {v1, v2, v3, v4, v5};
    typedef double (*func)(double, void*);
    pt5w4DLogIm2 = &phi5integrandIm;
    double resultpt5w4DLogIm, errorpt5w4DLogIm;
    gsl_integration_workspace* wpt5w4DLogIm = gsl_integration_workspace_alloc(1e7);

    gsl_function Fpt5w4DLogIm;
    Fpt5w4DLogIm.function = pt5w4DLogIm2;
    Fpt5w4DLogIm.params = &kinem;

    gsl_integration_qags(&Fpt5w4DLogIm, 0., 1., 1e-9, 1e-9, 1e6, wpt5w4DLogIm, &resultpt5w4DLogIm, &errorpt5w4DLogIm);
    gsl_integration_workspace_free(wpt5w4DLogIm);

    return resultpt5w4DLogIm;
}
