
#include <vector>
#include "header.h"
#include "headers-integrands.h" 

using namespace std;
using namespace GiNaC;

typedef double (*func)(double, void*) ; 

func pt5w4DLogRe, pt5w4DLogIm;

vector<double> ptstmp;



double pt5w4DLogReTMP(double t, void * p){
    typedef double (*func)(double, void*) ;
    double pt5w4DLogReTMP;
    struct my_f_prm * prm=(struct my_f_prm *)p;
    double v1f=(prm->v1f);double v2f=(prm->v2f);double v3f=(prm->v3f);double v4f=(prm->v4f);double v5f=(prm->v5f);

    int npts(ptstmp.size());
    double pts[npts];
    for(int count = 0; count<npts ; count++){pts[count]=ptstmp.at(count);}
   
    double x1(0.);  double x2(0.);

    if(npts==3){
        x1 = pts[1];
        if(x1<0.5){
             pt5w4DLogReTMP = x1*(*pt5w4DLogRe)( x1*t, p) + \
                    x1*(*pt5w4DLogRe)( 2*x1 - x1*t, p) + \
         (1. - 2*x1)*(*pt5w4DLogRe)( 2*x1 + (1. - 2*x1)*t, p);
        } else { 
             pt5w4DLogReTMP = (2*x1-1.) * (*pt5w4DLogRe)( (2*x1-1.)*t, p) + \
                    (1.-x1)*(*pt5w4DLogRe)( 2*x1 - 1. + (1.-x1)*t, p) + \
                    (1.-x1)*(*pt5w4DLogRe)( 1. - (1.-x1)*t, p);
        }
    };

    if(npts==4){
        x1 = pts[1]; x2 = pts[2];
        double delta;
        if(x1<=1.-x2 && x1 <= (x2-x1)/2.){
            delta=x1;
            pt5w4DLogReTMP =  delta*((*pt5w4DLogRe)( x1 - delta + delta*t, p) + \
                  (*pt5w4DLogRe)( x1 + delta - delta*t, p)) + \
                  (x2 - x1 - 2* delta) * (*pt5w4DLogRe)( x1 + delta + (x2 - x1 - 2* delta)*t, p)  + \
                  delta*((*pt5w4DLogRe)( x2 - delta + delta*t, p) +  \
                  (*pt5w4DLogRe)( x2 + delta - delta*t, p)) + \
                  (1. - x2 - delta)*(*pt5w4DLogRe)( x2 + delta + (1. - x2 - delta)*t, p);
        } else {
              if((x2-x1)/2. <= 1.-x2 && (x2-x1)/2. <= x1){
                  delta=(x2-x1)/2.;
                  pt5w4DLogReTMP = (x1-delta) * (*pt5w4DLogRe)( (x1-delta)*t, p) + \
                     delta*((*pt5w4DLogRe)( x1 - delta + delta*t, p) + \
                           (*pt5w4DLogRe)( x1 + delta - delta*t, p)) + \
                      delta*((*pt5w4DLogRe)( x2 - delta + delta*t, p) +  \
                            (*pt5w4DLogRe)( x2 + delta - delta*t, p)) + \
                     (1. - x2 - delta)*(*pt5w4DLogRe)( x2 + delta + (1. - x2 - delta)*t, p);
              }
              if(1.-x2 <= x1 && 1-x2 <= (x2-x1)/2.){
                  delta=1-x2;
                  pt5w4DLogReTMP = (x1-delta) * (*pt5w4DLogRe)( (x1-delta)*t, p) + \
                     delta*((*pt5w4DLogRe)( x1 - delta + delta*t, p) + \
                           (*pt5w4DLogRe)( x1 + delta - delta*t, p)) + \
                      (x2 - x1 - 2* delta) * (*pt5w4DLogRe)( x1 + delta + (x2 - x1 - 2* delta)*t, p)  + \
                      delta*((*pt5w4DLogRe)( x2 - delta + delta*t, p) +  \
                            (*pt5w4DLogRe)( x2 + delta - delta*t, p)) ;
              }
        }
    };
     return pt5w4DLogReTMP;
}



 
complex<double> integrate (int whichint, double s12, double s23, double s34, double s45, double s15, double err, size_t iwsalloc)
{

  using namespace std;
  using namespace GiNaC;

    struct my_f_prm kinem = {s12,s23,s34,s45,s15};

    double Re2,Im2;   double resultRe(0),resultIm(0);  double resultRe2(0),resultIm2(0);
 

    double resultpt5w4DLogRe{0.}, errorpt5w4DLogRe(0.);                            
    double resultpt5w4DLogIm{0.}, errorpt5w4DLogIm(0.);

    #include "integr-definition.h" 

    
      gsl_integration_workspace * wpt5w4DLogRe = gsl_integration_workspace_alloc (iwsalloc);
      gsl_integration_workspace * wpt5w4DLogIm = gsl_integration_workspace_alloc (iwsalloc);                                     
      gsl_function Fpt5w4DLogRe; Fpt5w4DLogRe.function = pt5w4DLogRe; Fpt5w4DLogRe.params = &kinem;
      gsl_function Fpt5w4DLogIm; Fpt5w4DLogIm.function = pt5w4DLogIm; Fpt5w4DLogIm.params = &kinem;

      if(kinem.v1f<0 && kinem.v2f<0 && kinem.v3f<0 && kinem.v4f<0 && kinem.v5f<0){ 
       double poleraw1=1./(1.+ (kinem.v1f) + (kinem.v2f) - (kinem.v4f));
       double poleraw2=1./(1.+ (kinem.v2f) + (kinem.v3f) - (kinem.v5f));
       double poleraw3=1./(1.+ (kinem.v3f) + (kinem.v4f) - (kinem.v1f));
       double poleraw4=1./(1.+ (kinem.v4f) + (kinem.v5f) - (kinem.v2f));
       double poleraw5=1./(1.+ (kinem.v5f) + (kinem.v1f) - (kinem.v3f));
       double polezero(0.);
       double poleone(1.);
       ptstmp.clear();
       ptstmp.push_back(polezero);
       ptstmp.push_back(poleone);
       if((poleraw1>polezero)&&(poleraw1<poleone)){ptstmp.push_back(poleraw1);}
       if((poleraw2>polezero)&&(poleraw2<poleone)){ptstmp.push_back(poleraw2);}
       if((poleraw3>polezero)&&(poleraw3<poleone)){ptstmp.push_back(poleraw3);}
       if((poleraw4>polezero)&&(poleraw4<poleone)){ptstmp.push_back(poleraw4);}
       if((poleraw5>polezero)&&(poleraw5<poleone)){ptstmp.push_back(poleraw5);}
       sort(ptstmp.begin(),ptstmp.end());

       if(ptstmp.size()==3||ptstmp.size()==4){Fpt5w4DLogRe.function = &pt5w4DLogReTMP;}

    int npts(ptstmp.size());
    double pts[npts];
    for(int count = 0; count<npts ; count++){pts[count]=ptstmp.at(count);}

        gsl_integration_qag (&Fpt5w4DLogRe, 0., 1., err, err,1e7,4,
                           wpt5w4DLogRe, &resultpt5w4DLogRe, &errorpt5w4DLogRe); 

       } else {
        gsl_integration_qag (&Fpt5w4DLogRe, 0., 1., err, err, 10,4,
                           wpt5w4DLogRe, &resultpt5w4DLogRe, &errorpt5w4DLogRe); 
        gsl_integration_qag (&Fpt5w4DLogIm, 0., 1., err, err, 10,4,        
                           wpt5w4DLogIm, &resultpt5w4DLogIm, &errorpt5w4DLogIm);
      }
        gsl_integration_workspace_free (wpt5w4DLogRe); 
        gsl_integration_workspace_free (wpt5w4DLogIm); 

    resultRe = resultpt5w4DLogRe;
    resultIm = resultpt5w4DLogIm;

  complex<double> integrated = resultRe + resultIm*1i;

  return integrated;
}




