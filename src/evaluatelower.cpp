#include "header.h"
#include "headers-integrands.h"
#include <vector>

using namespace std;
using namespace GiNaC;

complex<double> evaluatelower(int whichint, double s12, double s23, double s34, double s45, double s15) {

    using namespace std;
    using namespace GiNaC;

    struct my_f_prm kinem = {s12, s23, s34, s45, s15};

    typedef double (*funclow)(int, void*);
    funclow lowerweight;

#include "lowerfunc-definition.h"

    double realpart = (*lowerweight)(0, &kinem);
    double imagpart = (*lowerweight)(1, &kinem);
    complex<double> evaluatelower = realpart + imagpart * 1i;

    return evaluatelower;
}

