
#include <iostream>
#include <list>
#include <cmath>
#include <complex>
#include <gsl/gsl_integration.h>
#include <gsl/gsl_complex_math.h>
#include <gsl/gsl_sf_pow_int.h>
#include <ginac/ginac.h>
#include "useful-fcns.h"
using namespace std::complex_literals;

